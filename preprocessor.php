#!/usr/local/bin/php
<?php
/**
 * to run preprocessor use in terminal 
 * 
 * # php preprocessor.php path-to-input-file path-to-output-file [working-direcory]
 * 
 * for example, run in main project folder:
 * 
 * # php preprocessor.php prs/extends.prs www/output/output
 * 
 */
header('Content-type: text/html; charset=utf-8');

require 'vendor/autoload.php';

use Preprasor\Config\Setting\SimpleStandAlone;
use Preprasor\Preprocess\Helper\Factory\SimplePreprocessorFactory;

do {
	if(!isset($argv[1])) {
		echo "param input missing";
		exit;
	}
	if(!isset($argv[2])) {
		echo "param output missing";
		exit;
	}
	
	$inputFile = $argv[1];
	$outputFile = $argv[2];
	
	if(!is_file($inputFile)) {
		echo sprintf("input file %s not found in %s", $inputFile, dirname(realpath($inputFile)));
		exit;
	}
	
	if(is_file($outputFile)) {
		echo sprintf("output file %s already exists, rewrite? type Y for rewrite\n", $outputFile);
		$stdin = fopen('php://stdin', 'r');
		$response = fgetc($stdin);
		if ($response != 'Y') {
		   echo "aborted.\n";
		   exit;
		}
	}
	
	if(is_file($outputFile) && !is_writable($outputFile)) {
		echo sprintf("output file %s is not writable", $outputFile);
		exit;
	}
	
	$input = file_get_contents($inputFile);
	
	if(isset($argv[3])) {
		$workingDirectory = $argv[3];
	} else {
		$workingDirectory = dirname(realpath($inputFile)) . DIRECTORY_SEPARATOR;
	}	

	$setting = new SimpleStandAlone;
	$factory = new SimplePreprocessorFactory;
	$preprasor = $factory->createPreprasor($input, $workingDirectory, $setting);

	$preprasor->init();
	$output = $preprasor->printCSSOutput();
	
	$myfile = fopen($outputFile, "w") or die("unable to open file!");
	fwrite($myfile, $output);
	fclose($myfile);
	
	echo sprintf("output write in file %s\n", $outputFile);
	
} while (false);



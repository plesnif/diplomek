function apply() {
    document.getElementById('iframe').src = "iframe.php?style=" + encodeURIComponent($('#output').val());
}
function preprocessAndApply() {
    $.ajax({
        type: "GET",
        url: "prepras.php",
        data: {input: $('#input').val()},
        success: function(data){
            $('#output').val(data);
            apply();
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert(XMLHttpRequest.responseText);
        }
    });
}

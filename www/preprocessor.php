<?php

header('Content-type: text/html; charset=utf-8');

require __DIR__ . '/../vendor/autoload.php';

use Preprasor\Config\Setting\StandardStandAlone;
use Preprasor\Config\Setting\StandardWebProduction;
use Preprasor\Config\Setting\SimpleStandAlone;
use Preprasor\Config\Setting\SimpleWebProduction;
use Preprasor\Preprocess\Helper\Factory\SimplePreprocessorFactory;
use Preprasor\Preprocess\Helper\Factory\StandardPreprocessorFactory;
use Tracy\Debugger;

Debugger::enable();
Debugger::$maxDepth = 20;
Debugger::$maxLength = 50;

$useSimpleMode = 1;
$runInWebProductionMode = 1;
$printOnlyTokens = 0;
//$file = 'css/simpleBlock.css';
//$file = 'css/test.css';
//$file = 'prs/variable.prs';
//$file = 'prs/require.prs';
//$file = 'prs/mixin.prs';
//$file = 'prs/nesting.prs';
//$file = 'prs/extends.prs';
//$file = 'prs/exclude.prs';
//$file = 'prs/color.prs';
//$file = 'prs/imageFunction.prs';
//$file = 'prs/imageModification.prs';

//$file = 'prs/test.prs';
//$file = 'prs/test4.prs';
$file = 'prs/short.prs';
if(!is_file($file)) {
	throw new Exception("file not found");
}
$input = file_get_contents($file);
$workingDirectory = dirname(realpath($file)) . DIRECTORY_SEPARATOR;

if($useSimpleMode) {
	if($runInWebProductionMode) {
		$setting = new SimpleWebProduction;
	} else {
		$setting = new SimpleStandAlone;
	}	
	$factory = new SimplePreprocessorFactory;
} else {	
	if($runInWebProductionMode) {
		$setting = new StandardWebProduction;
	} else {
		$setting = new StandardStandAlone;
	}
	$factory = new StandardPreprocessorFactory;
}

$factory = new SimplePreprocessorFactory;
$preprasor = $factory->createPreprasor($input, $workingDirectory, $setting);

if($printOnlyTokens) {
	$preprasor->printTokens();
} else {
	$preprasor->init();

	//$preprasor->dumpCSSTree();

	echo "<pre>";
	echo $preprasor->printCSSOutput();
	echo "</pre>";
	$preprasor->printFinalAnalysis();
}

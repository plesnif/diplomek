<?php

require __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

$input = filter_input(INPUT_GET, 'input');

use Preprasor\Preprocess\Helper\Factory\SimplePreprocessorFactory;
use Preprasor\Config\Setting\SimpleWebProduction;
use Tracy\Debugger;

Debugger::enable();
Debugger::$maxDepth = 20;
Debugger::$maxLength = 50;


if ($input != "") {
	$workingDirectory = __DIR__ . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR;
	try {
		$setting = new SimpleWebProduction;
		$factory = new SimplePreprocessorFactory;
		$preprasor = $factory->createPreprasor($input, $workingDirectory, $setting);

		$preprasor->init();
		echo $preprasor->printCssOutput();
	} catch (Exception $ex) {
		echo $ex->getMessage();
	}


} else {
	echo "no input";
}



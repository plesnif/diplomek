<?php
$input = "";
if(isset($_GET["input_file"])) {
	$file = "prs/".$_GET["input_file"].".prs";
	$input = file_get_contents($file);
	$workingDirectory = dirname(realpath($file)) . DIRECTORY_SEPARATOR;
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <script type='text/javascript' src="js/jquery.min.js"></script>
        <script type='text/javascript' src="js/script.js"></script>
		<link rel="stylesheet" type="text/css" href="css/style.css">
        <title>Preprocess Lab</title>
    </head>
    <body>
	<a href="examples.html">Zpátky na stránku s příklady</a>
		<form action="prepras.php" method="post">
			<table>
				<tr>
					<th>
						Vstup v syntaxi jazyka preprocesoru:
					</th>
					<th>
						Výstup v syntaxi jazyka CSS:
					</th>
					<th>
						Aplikování vytvořeného CSS:
					</th>
				</tr>
				<tr>
					<td>
						<textarea id="input" name="input">
<?php
echo $input;
?>
						</textarea>
						
					</td>
					<td>
						<textarea id="output" name="output" readonly>
						</textarea>
						<input type="button" onclick="preprocessAndApply();" value="vytvořit CSS výstup a aplikovat -->"/>
					</td>
					<td>
						<iframe src="iframe.php" id="iframe">
					</td>
				</tr>
			</table>

		</form>
	</body>
</html>
<?php

namespace Preprasor\Preprocess\Image;

/**
 * Generated by PHPUnit_SkeletonGenerator on 2016-10-01 at 07:38:21.
 * @group image
 */
class TransformatorTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @var Transformator
	 */
	protected $object;

	/**
	 * @var string
	 */
	private $path;

	/**
	 * @var string
	 */
	private $fileName;

	/**
	 * @var string
	 */
	private $nonexistingPath;
	
	/**
	 * @var string 
	 */
	private $notSupportedExtension;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		$this->path = "input/image/";
		$this->fileName = [
			"png" => "cat.png", 
			"jpg" => "bee.jpg",
			"gif" => "jesus.gif"
		]; 
		
		$this->notSupportedExtension = "notSupported.extension";
		$this->nonexistingPath = "nowhere";
		$this->diver = $this->getMockBuilder('\Preprasor\Preprocess\Helper\DirectoryDiver')
					->setConstructorArgs(array($this->path))
					->getMock();
		$this->diver->method('getPath')
             ->willReturn($this->path);


	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {
		
	}
	
	public function transformsDataProvider() {
		$this->setUp();
		return array(
			array(array("negate", $this->fileName["png"])),
			array(array("grayscale", $this->fileName["png"])),
			array(array("brightness", $this->fileName["png"], 15)),
			array(array("contrast", $this->fileName["png"], 15)),
			array(array("colorize", $this->fileName["png"], 15, 15, 15, 15)),
			array(array("blur", $this->fileName["png"], 15)),
			array(array("negate", $this->fileName["png"])),
			array(array("negate", $this->fileName["png"])),
			array(array("negate", $this->fileName["png"])),
		);
	}

	/**
	 * @covers Preprasor\Preprocess\Image\Transformator::__construct
	 */
	public function test__Construct() {
		$this->object = new Transformator(array("arg1", $this->fileName["png"]), $this->diver);
	}
	
	/**
	 * @covers Preprasor\Preprocess\Image\Transformator::createImageFromPath
	 */
	public function testCreateImageFromPath_FileIsNotReadable() {
		$this->expectException(\Preprasor\Preprocess\Image\FileIsNotReadable::class);
		$this->object = new Transformator(array("arg1", $this->nonexistingPath), $this->diver);
	}
	/**
	 * @covers Preprasor\Preprocess\Image\Transformator::createImageFromPath
	 */
	public function testCreateImageFromPath_NotSupportedImageExtension() {
		$this->expectException(\Preprasor\Preprocess\Image\NotSupportedImageExtension::class);
		$this->object = new Transformator(array("arg1", $this->notSupportedExtension), $this->diver);
	}
	/**
	 * @covers Preprasor\Preprocess\Image\Transformator::createImageFromPath
	 */
	public function testCreateImageFromPath() {
		
		$this->object = new Transformator(array("arg1", $this->fileName["png"]), $this->diver);
		$this->assertInstanceOf('\Preprasor\Preprocess\Image\Transformator', $this->object);
		
		$this->object = new Transformator(array("arg1", $this->fileName["jpg"]), $this->diver);
		$this->assertInstanceOf('\Preprasor\Preprocess\Image\Transformator', $this->object);
		
		$this->object = new Transformator(array("arg1", $this->fileName["gif"]), $this->diver);
		$this->assertInstanceOf('\Preprasor\Preprocess\Image\Transformator', $this->object);
	}

	/**
	 * @covers Preprasor\Preprocess\Image\Transformator::transformate
	 * @covers Preprasor\Preprocess\Image\Transformator::toInt
	 * @covers Preprasor\Preprocess\Image\Transformator::expectNumberOfArguments
	 * 
	 * @dataProvider transformsDataProvider
	 */
	public function testTransformate($args) {
		$this->object = new Transformator($args, $this->diver);		
		$this->assertInternalType(\PHPUnit_Framework_Constraint_IsType::TYPE_STRING, $this->object->transformate());
		
	}
	/**
	 * @covers Preprasor\Preprocess\Image\Transformator::transformate
	 * @covers Preprasor\Preprocess\Image\Transformator::toInt
	 */
	public function testTransformate_FunctionNameNotSet() {
		$this->expectException(\Preprasor\Preprocess\Image\FunctionNameNotSet::class);
		$this->object = new Transformator(array("nonSupportedFilter", $this->fileName["png"]), $this->diver);
		$this->object->transformate();		
	}

	/**
	 * @covers Preprasor\Preprocess\Image\Transformator::expectNumberOfArguments
	 */
	public function testExpectNumberOfArguments() {
		$this->expectException(\Preprasor\Preprocess\Image\UnexpectedNumberOfArgument::class);
		$this->object = new Transformator(array("brightness", $this->fileName["png"], 15, 15), $this->diver);		
		$this->assertInternalType(\PHPUnit_Framework_Constraint_IsType::TYPE_STRING, $this->object->transformate());
	}	
}
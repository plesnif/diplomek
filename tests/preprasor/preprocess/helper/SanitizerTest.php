<?php

namespace Preprasor\Preprocess\Helper;

/**
 * Generated by PHPUnit_SkeletonGenerator on 2016-11-01 at 14:12:47.
 * @group sanitize
 */
class SanitizerTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @var Sanitizer
	 */
	protected $object;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		$chararcters = new SpecialCharacters;
		$this->object = new Sanitizer($chararcters);
	}
	
    public function fixUTFDataProvider() {
        return array(
            array("FÃÂ©dération Camerounaise de Football"),
            array("FÃ©dÃ©ration Camerounaise de Football"),
            array("FÃÂ©dÃÂ©ration Camerounaise de Football"),
			array("FÃÂÂÂÂ©dÃÂÂÂÂ©ration Camerounaise de Football")
        );
    }
	
	public function replaceDataProvider() {
        return array(
            array("\x000D\x000A", "\x000A"),
            array("\x000D", "\x000A"),
            array("\x000C", "\x000A"),
			array("\x0000", "\xFFFD")
		);
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {
		
	}

	/**
	 * @covers Preprasor\Preprocess\Helper\Sanitizer::sanitizeInput
     * @dataProvider fixUTFDataProvider
	 */
	public function testSanitizeInput($input) {
		$sanitized = $this->object->sanitizeInput($input);
		$this->assertEquals("Fédération Camerounaise de Football", $sanitized);
		
		$this->expectException(\Preprasor\Preprocess\Helper\InvalidInput::class);
		$this->object->sanitizeInput(5);
	}
	
	/**
	 * @covers Preprasor\Preprocess\Helper\Sanitizer::preprocessInputStream
     * @dataProvider replaceDataProvider
	 */
	public function testPreprocessInputStream($input, $expected) {
		$sanitized = $this->object->sanitizeInput($input);
		$this->assertEquals($expected, $sanitized);
	}
}

<?php

namespace Preprasor\Preprocess\Helper;

/**
 * Generated by PHPUnit_SkeletonGenerator on 2016-12-03 at 16:59:25.
 */
class AnalyzerTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @var Analyzer
	 */
	protected $object;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		$this->object = new Analyzer;
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {
		
	}

	/**
	 * @covers Preprasor\Preprocess\Helper\Analyzer::printFinalAnalysis
	 */
	public function testPrintFinalAnalysis() {
		$this->object->printFinalAnalysis();
	}
}

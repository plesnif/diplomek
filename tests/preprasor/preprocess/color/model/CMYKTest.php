<?php

namespace Preprasor\Preprocess\Color\Model;

/**
 * Generated by PHPUnit_SkeletonGenerator on 2016-12-11 at 13:53:12.
 */
class CMYKTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @var CMYK
	 */
	protected $object;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		\Preprasor\Config\Config::setSetting(new \Preprasor\Config\Setting\SimpleWebProduction);
		$this->params1 = array(11, 11, 11, 11);
		$this->params2 = array(5000, 11, 11, 11);
		
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {
		
	}

	/**
	 * @covers Preprasor\Preprocess\Color\Model\CMYK::printOut
	 */
	public function testPrintOut() {
		$this->object = new CMYK($this->params1);
		$this->assertEquals("#cacaca", $this->object->printOut());

	}

	/**
	 * @covers Preprasor\Preprocess\Color\Model\CMYK::__construct
	 * @covers Preprasor\Preprocess\Color\Model\CMYK::setFromArray
	 */
	public function testConstuct() {
		$this->object = new CMYK($this->params1);
		
		$this->expectException(\Preprasor\Preprocess\Color\Model\CMYKWrongFormat::class);
		$this->object = new CMYK($this->params2);
	}

}

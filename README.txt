# Preprasor #

PHP preprocesor extending standard CSS language

## Description ##

This package written in PHP helps working with CSS language. Provides preprocessing options extending standard CSS language and making CSS code more readable and sustainable. Due to written in PHP, preprocessor syntax tries to be as similar as PHP syntax. Adds features such as Variables, Mixins, Nesting, Inheritance, Excluding, Color and Image modifications and functions.

## Usage ##

	require __DIR__ . '/vendor/autoload.php';
	
	use Preprasor\Preprasor;
	use Preprasor\Preprocess\StandardPreprocessor;

	//prepare input
	$file = 'www/css/input.css';	
	$input = file_get_contents($file);
	$workingDirectory = dirname(realpath($file)) . DIRECTORY_SEPARATOR;	
	
	//hands over input and working directory
	$preprocessor = new StandardPreprocessor($input, $workingDirectory);
	//for "basic" simple CSS code might be used Preprasor\Preprocess\SimplePreprocessor
	
	//inject $preprocessor and execute desired actions 
	$preprasor = new Preprasor($preprocessor);
	
	//prints stream of tokens
	$preprasor->printTokens();
	
	//initializes necessary components 
	$preprasor->init();
	
	//dumps structure representing CSS Tree
	$preprasor->dumpCssTree();
	
	//prints prepocessed output
	echo "<pre>";
	echo $preprasor->printCssOutput();
	echo "</pre>";
	
	//prints final analysis
	$preprasor->printFinalAnalysis();

## Examples ##


### Require ###

includes external file in output

#### input ####

	body {
		color: blue;
	}
	@require '../css/simpleDiv.css';
	
#### output ####

	body {
		color: blue;
	}
	div {
		color: red;
	}

### Variable ###

defines variable

input

	$fontFace Arial, sans-serif;
	p1 {
		font: 15px $fontFace;
	}
	p2 {
		font: 20px $fontFace;
	}

output:

	p1 {
		font: 15px Arial, sans-serif;
	}
	p2 {
		font: 20px Arial, sans-serif;
	}
	
### Mixin ###

defines reusable functions

input

        mixin paragraph($fontface,$fontSize) {
                font: $fontface $fontSize;
                text-align: right;
        }
        p1 {
                @mixin paragraph(15, Arial);
        }
        p2 {
                @mixin paragraph(20, Helvetica);
        }	

output:

        p1 {
                font: 15 Arial;
                text-align: right;
        }
        p2 {
                font: 20 Helvetica;
                text-align: right;
        }

### Nesting ###

maintains better visual hierarchy

input

	div {
		span {
			margin: 0;
			padding: 0;
			list-style: none;
		}
		a {
			color: red;
			text-decoration: none;
		}
	}

output:

	div span {
		margin: 0;
		padding: 0;
		list-style: none;
	}
	div a {
		color: red;
		text-decoration: none;
	}
	
### Extending classes ###

introduces basic OOP principle

input

	.navigation {
		font-family: "Times New Roman";
		font-size: 30px;
	}
	
	.detail extends .navigation {
		color: red;
	}

output:

	.navigation, .detail {
		font-family: "Times New Roman";
		font-size: 30px;
	}
	
	.detail  {
		color: red;
	}
	
### Excluding attributes ###

excludes desired attributes from declarations

input

	.paragraph {
		font-family: "Times New Roman";
		font-size: 30px;
		color: red;
		child: -4n+10;
	}
	.colorlessParagraph excludeFrom .paragraph {
		color;
	}

output:

	.paragraph {
		font-family: "Times New Roman";
		font-size: 30px;
		color: red;
		child: -4n+10;
	}
	.colorlessParagraph  {
		font-family: "Times New Roman";
		font-size: 30px;
		child: -4n+10;
	}
	
### Color operations ###

establishes color modifications

input

	body {
		color: hsl(12, 54, 87);
	}
	div {
		color: hsv(12, 54, 87);
	}
	span {
		color: rgb(12, 54, 87);
	}
	p {
		color: darken(hsv(12, 54, 87), 20);
	}
	ul {
		color: lighten(rgb(85, 200, 13), 20);
	}
	p {
		color: saturate(hsv(12, 54, 87), 12);
	}
	ul {
		color: desaturate(hsv(85, 84, 13), 12);
	}

output:

	body {
		color: #efd3cb;
	}
	div {
		color: #dd7e66;
	}
	span {
		color: #0c3657;
	}
	p {
		color: #b24327;
	}
	ul {
		color: #8af245;
	}
	p {
		color: #e8765a;
	}
	ul {
		color: #131c07;
	}
	
### Image functions ###

provides helpful image functions

input

	$height image-height('../images/bolog.jpg');
	$width image-width('../images/bolog.jpg');
	
	.imageContainer {
		height: $height;
		width: $width;
	}

output:

	.imageContainer {
		height: 447px;
		width: 600px;
	}
	
### Image modification ###

creates modified images and link them in CSS

input

	.div1 {
		image: negate_image('../images/bolog.jpg');
	}
	.div2 {
		image: grayscale_image('../images/bolog.jpg');
	}
	.div3 {
		image: brightness_image('../images/bolog.jpg' 50);
	}
	.div4 {
		image: contrast_image('../images/bolog.jpg' 50);
	}
	.div5 {
		image: colorize_image('../images/bolog.jpg' 15 15 15 15);
	}
	.div6 {
		image: blur_image('../images/bolog.jpg' 50);
	}

output:

	/* also creates image at relative url */
	.div1 {
		image: url('../bolog-negate.jpg');
	}
	/* also creates image at relative url */
	.div2 {
		image: url('../bolog-grayscale.jpg');
	}
	/* also creates image at relative url */
	.div3 {
		image: url('../bolog-brightness-50.jpg');
	}
	/* also creates image at relative url */
	.div4 {
		image: url('../bolog-contrast-50.jpg');
	}
	/* also creates image at relative url */
	.div5 {
		image: url('../bolog-colorize-15-15-15-15.jpg');
	}
	/* also creates image at relative url */
	.div6 {
		image: url('../bolog-blur-50.jpg');
	}
	

<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */
namespace Preprasor;

/**
 * Class storing application constants
 *
 * @package Preprasor
 */
class Constant {
	
	const NUMBER_TYPE_DEFAULT = 1;
	const NUMBER_TYPE_INTEGER = 2;
	const NUMBER_TYPE_NUMBER = 3;
	
	const HASH_FLAG_DEFAULT = 10;
	const HASH_FLAG_UNRESTRICTED = 11;
	const HASH_FLAG_ID = 12;
	
	const KEYWORD_REQUIRE = 'require';
	const KEYWORD_CHARSET = 'charset';
	const KEYWORD_IMAGE_HEIGHT = 'image-height';
	const KEYWORD_IMAGE_WIDTH = 'image-width';
	const KEYWORD_PIXELS = 'px';
	const KEYWORD_RGB = 'rgb';
	const KEYWORD_RGBA = 'rgba';
	const KEYWORD_HSV = 'hsv';
	const KEYWORD_HSVA = 'hsva';
	const KEYWORD_HSL = 'hsl';
	const KEYWORD_HSLA = 'hsla';
	const KEYWORD_CMYK = 'cmyk';
	const KEYWORD_CMYKA = 'cmyka';
	const KEYWORD_SATURATE = 'saturate';
	const KEYWORD_DESATURATE = 'desaturate';
	const KEYWORD_LIGHTEN = 'lighten';
	const KEYWORD_DARKEN = 'darken';
	const KEYWORD_CHANGE_HUE = 'changehue';
	const KEYWORD_MIXIN = 'mixin';
	const KEYWORD_URL = 'url';
	const KEYWORD_IMPORTANT = 'important';
	const KEYWORD_EXTENDS = 'extends';
	const KEYWORD_EXCLUDE_FROM = 'excludeFrom';
	
	const SUFFIX_IMAGE = "image";
	
	const QUOTATION_MARK = "\"";
	const SLASH = "/";
	const NUMBER = "#";
	const EQUALS = "=";
	const DOLAR = "$";
	const APOSTROPHE = "'";
	const LEFT_PARENTHESIS = "(";
	const RIGHT_PARENTHESIS = ")";
	const ASTERISK = "*";
	const PLUS = "+";
	const COMMA = ",";
	const HYPHEN_MINUS = "-";
	const FULL_STOP = ".";
	const COLON = ":";
	const SEMICOLON = ";";
	const LESS_THAN = "<";
	const GREATER_THAN = ">";
	const COMMERCIAL_AT = "@";
	const LEFT_SQUARE_BRACKET = "[";
	const RIGHT_SQUARE_BRACKET = "]";
	const CIRCUMFLEX_ACCENT = "^";
	const REVERSE_SOLIDUS = "\\";
	const LEFT_CURLY_BRACKET = "{";
	const RIGHT_CURLY_BRACKET = "}";
	const VERTICAL_LINE = "|";
	const TILDE = "~";
	const PERCENTAGE_SIGN = "%";
	const EXCLAMATION_MARK = "!";
	const UNDERSCORE = "_";
	
	const FULL_STOP_REGEXP = "\.";
	
	const NEW_LINE = "\n";
	const TAB = "\t";

}

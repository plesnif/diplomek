<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor;

use Preprasor\Preprocess\Tokenize\Streamer;

/**
 * Error handling
 *
 * @package Preprasor
 */
class PreprasorErrorException extends \Exception {
	
	/**
	 * @var Streamer
	 */
	private static $streamer;
	
	/**
	 * sets streamer to track caret position in case of error
	 * 
	 * @param Streamer $streamer
	 */
	public static function setStreamer(Streamer $streamer) {
		self::$streamer = $streamer;
	}
	
	/**
	 * gets caret position when error occurs
	 * 
	 * @return string
	 */
	public static function getErrorAsIndexedCell() {
		return self::$streamer->getPositionAsIndexedCell();
	}

	/**
	 * gets hint for error
	 *
	 * @return string
	 */
	public function getHintMessage() {
		$errorPosition = self::getErrorAsIndexedCell();
		if(is_null($errorPosition)) {
			return $this->getMessage(). "(Nepodařilo se zjistit řádek a sloupec chyby.)";
		} else {
			return $this->getMessage(). " Chyba na řádku " . $errorPosition["row"] . ", a sloupci " . $errorPosition["column"];
		}
	}
}
<?php

/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor;

use Preprasor\Preprocess\Helper\Analyzer;
use Preprasor\Preprocess\Preprocessor;
use Preprasor\Preprocess\StandardPreprocessor;
use Preprasor\Preprocess\SimplePreprocessor;
use Preprasor\Preprocess\I\Initiable;
use Preprasor\Preprocess\I\Preprocessible;

/**
 * Main class
 *
 * @package Preprasor
 */
class Preprasor implements Initiable {
	/**
	 * @var Preprocessor
	 */
	private $preprocessor;
	
	/**
	 * @var Analyzer
	 */
	private $analyzer;

	/**
	 * Preprasor constructor.
	 *
	 * @param Preprocessor|null $preprocessor
	 */
	public function __construct(Preprocessible $preprocessor = null) {
		$this->analyzer = new Analyzer();
		if($preprocessor) {
			$this->preprocessor = $preprocessor;
		}
	}
	
	/**
	 * let Preprasor use Simple preprocessing
	 * 
	 * @param string $input
	 * @param string $workingDirectory
	 */
	public function useSimplePreprocessor($input, $workingDirectory) {
		$this->preprocessor = new SimplePreprocessor($input, $workingDirectory);
	}
	
	/**
	 * let Preprasor use Standart preprocessing
	 * 
	 * @param string $input
	 * @param string $workingDirectory
	 */	
	public function useStandardPreprocessor($input, $workingDirectory) {
		$this->preprocessor = new StandardPreprocessor($input, $workingDirectory);
	}
	
	
	/**
	 * runs preprocessor
	 * 
	 * @throws PreprocessorNotSet	
	 */
	public function init() {
		$this->checkIfPreprocessorExists();
		$this->preprocessor->init();
	}

	/**
	 * {@inheritDoc}
	 */
	public function dumpCSSTree() {
		$this->checkIfPreprocessorExists();
		$this->preprocessor->dumpCSSTree();
	}

	/**
	 * {@inheritDoc}
	 */
	public function printCSSOutput() {
		$this->checkIfPreprocessorExists();
		return $this->preprocessor->printCSSOutput();
	}

	/**
	 * {@inheritDoc}
	 */
	public function printTokens() {
		$this->checkIfPreprocessorExists();
		$this->preprocessor->printTokens();
	}
	
	/**
	 * preints final analysis
	 */
	public function printFinalAnalysis() {
		$this->checkIfPreprocessorExists();
		$this->analyzer->printFinalAnalysis();
	}
	
	/**
	 * throws Exception if preprocessor is not set
	 * 
	 * @throws PreprocessorNotSet
	 */
	private function checkIfPreprocessorExists() {
		if(is_null($this->preprocessor)) {
			throw new PreprocessorNotSet("Preprocessor není definován.");
		}
	}
}

class PreprocessorNotSet extends PreprasorErrorException {
	
}

class InputsNotSet extends PreprasorErrorException {
	
}

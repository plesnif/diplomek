<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */
namespace Preprasor\Preprocess\I;

/**
 * Interface defining preprocessor interface
 *
 * @package Preprasor\Preprocess\I
 */

interface Compositable {
	/**
	 * Prints item in certain depth
	 * 
	 * @param int $depth
	 * @return string
	 */
	public function printOut($depth);
	
	/**
	 * Runs preprocessing
	 */
    public function prepras();
	
	/**
	 * returns name of Compositable object if defined
	 * 
	 * @return string|false
	 */
	//public function name();
	
	/**
	 * checks whenever Compositable object is new line
	 */
	public function isNewline();
	
	/**
	 * checks whenever token is supportive token - i.e. whitespace, comma
	 * 
	 * @return boolean
	 */
	public function isSupportive();	

}

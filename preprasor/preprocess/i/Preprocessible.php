<?php

/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\I;

/**
 * Preprocessible inteface
 */
interface Preprocessible extends Initiable {

}

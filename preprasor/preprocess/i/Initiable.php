<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\I;

/**
 * Interface defining preprocessor interface
 *
 * @package Preprasor\Preprocess\I
 */
interface Initiable {
	
	/**
	 * Runs preprocessing
	 */
    public function init();
	
	/**
	 * dumps CSS tree
	 */
	public function dumpCSSTree();
	
	/**
	 * prints output in CSS format
	 */
	 public function printCSSOutput();
	 
	/**
	 * prints token stream from input
	 */
	 public function printTokens();

}
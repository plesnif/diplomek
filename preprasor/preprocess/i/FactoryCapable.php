<?php

/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\I;

use Preprasor\Preprocess\Tokenize\Streamer;
use Preprasor\Preprocess\Helper\DirectoryDiver;
use Preprasor\Preprocess\Tokenize\Tokenizer\I\Tokenizerable;
use Preprasor\Config\Setting\Setting;
use Preprasor\Preprocess\Parse\Parser;
use Preprasor\Preprasor;
use Preprasor\Preprocess\I\Preprocessible;

/**
 * classe capable of creating factory
 */
interface FactoryCapable {
	/**
	 * creates streamer
	 * 
	 * @param string $input
	 * @return Streamer
	 */
	public function createStreamer($input);
	/**
	 * creates direcory diver
	 * 
	 * @param string $pathToWorkingDirectory
	 * @return DirectoryDiver
	 */
	public function createDirectoryDiver($pathToWorkingDirectory);
	/**
	 * creates tokenizer
	 * 
	 * @param Streamer $streamer
	 * @return Tokenizerable
	 */
	public function createTokenizer(Streamer $streamer, DirectoryDiver $directoryDiver);
	/**
	 * creates parser
	 * 
	 * @param Tokenizerable $tokenizer
	 * @param DirectoryDiver $directoryDiver
	 * @return Parser
	 */
	public function createParser(Tokenizerable $tokenizer, DirectoryDiver $directoryDiver);	
	/**
	 * creates preprocessor
	 * 
	 * @param string $input
	 * @param string $pathToWorkingDirectory
	 * @return Preprocessible
	 */
	public function createPreprocessor($input, $pathToWorkingDirectory);
	/**
	 * creates preprasor
	 * 
	 * @param string $input
	 * @param string $pathToWorkingDirectory
	 * @param Setting $setting
	 * @return Preprasor
	 */
	public function createPreprasor($input, $pathToWorkingDirectory, Setting $setting = null);
}

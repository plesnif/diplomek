<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */
namespace Preprasor\Preprocess\Image\Format;

use Preprasor\Preprocess\Image\Format\I\ImageCreatable;

/**
 * Class representing GIF image control
 *
 * @package Preprasor\Preprocess\Image\Format
 */
class GIF implements ImageCreatable{
	/**
	 * {@inheritDoc}
	 */
    public function createFromFile($pathToFile) {
		return imagecreatefromgif($pathToFile);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function saveToFile($image, $pathToFile) {
		return imagegif($image, $pathToFile);
	}
}

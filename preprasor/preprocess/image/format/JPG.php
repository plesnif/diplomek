<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Image\Format;

use Preprasor\Preprocess\Image\Format\I\ImageCreatable;

/**
 * Class representing JPG image control
 *
 * @package Preprasor\Preprocess\Image\Format
 */
class JPG implements ImageCreatable {
	/**
	 * {@inheritDoc}
	 */
    public function createFromFile($pathToFile) {
		return imagecreatefromjpeg($pathToFile);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function saveToFile($image, $pathToFile) {
		return imagejpeg($image, $pathToFile, 100);
	}
	
}

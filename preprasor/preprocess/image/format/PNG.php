<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */
namespace Preprasor\Preprocess\Image\Format;

use Preprasor\Preprocess\Image\Format\I\ImageCreatable;

/**
 * Class representing PNG image control
 *
 * @package Preprasor\Preprocess\Image\Format
 */
class PNG implements ImageCreatable{
	/**
	 * {@inheritDoc}
	 */
    public function createFromFile($pathToFile) {
		return imagecreatefrompng($pathToFile);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function saveToFile($image, $pathToFile) {
		return imagepng($image, $pathToFile, 0);
	}
}

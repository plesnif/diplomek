<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */
/**
 * @codeCoverageIgnore
 */
namespace Preprasor\Preprocess\Image\Format\I;

/**
 * Interface denoting class able to create images according to its format
 *
 * @package Preprasor\Preprocess\Image\Format\I
 */
interface ImageCreatable {
	
	/**
	 * returns an image identifier representing the image obtained from the given filename
	 * 
	 * @param string $pathToFile
	 * @return resource
	 */
    public function createFromFile($pathToFile);
	
	/**
	 * outputs $image to file $pathToFile, returns bool created/not created
	 * 
	 * @param resource $image
	 * @param string $pathToFile
	 * @return boolean
	 */
	public function saveToFile($image, $pathToFile);
	
}
<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Image;

use Preprasor\PreprasorErrorException;
use Preprasor\Preprocess\Image\Format\I\ImageCreatable;
use Preprasor\Preprocess\Image\Format\JPG;
use Preprasor\Preprocess\Image\Format\PNG;
use Preprasor\Preprocess\Image\Format\GIF;
use Preprasor\Preprocess\Helper\DirectoryDiver;

/**
 * Class responsible for Image transformation
 */
class Transformator {
	/**
	 * @var array 
	 */
	private $supportedExtensions = ["png", "jpg", "jpeg", "gif"];
	
	/**
	 * @var resource 
	 */
	private $image;
	
	/**
	 * @var array 
	 */
	private $args;
	
	/**
	 * @var array 
	 */
	private $pathInfo;
	
	/**
	 * @var ImageCreatable
	 */
	public $imageFormat;
	
	/**
	 * @var DirectoryDiver 
	 */
	private $directoryDiver;

	/**
	 * Transformator constructor.
	 *
	 * @param array  $args
	 * @param DirectoryDiver $directoryDiver
	 */
	public function __construct(array $args, DirectoryDiver $directoryDiver) {
		$this->directoryDiver = $directoryDiver;
		$this->args = $args;
		$relativePathToFile = $args[1];
		$this->image = $this->createImageFromPath($relativePathToFile);
	}
	
	/**
	 * creates image from path according to its extension
	 * 
	 * @param string $relativePathToFile
	 * @return ImageCreatable
	 * @throws FileIsNotReadable
	 * @throws NotSupportedImageExtension
	 */
	private function createImageFromPath($relativePathToFile) {
		$absolutePathToFile = $this->directoryDiver->getPath() . $relativePathToFile;
		if(!is_readable($absolutePathToFile)) {
			throw new FileIsNotReadable("Není možné načíst soubor  " . $absolutePathToFile);
		}
		$this->pathInfo = pathinfo($relativePathToFile);
		switch ($this->pathInfo["extension"]) {
			case $this->supportedExtensions[0]:
				$this->imageFormat = new PNG;
				break;
			case $this->supportedExtensions[1]:
			case $this->supportedExtensions[2]:
				$this->imageFormat = new JPG;
				break;
			case $this->supportedExtensions[3]:
				$this->imageFormat = new GIF;
				break;
			default:
				throw new NotSupportedImageExtension("Přípona " . $this->pathInfo["extension"] . " není pro obrázky podporována.");
		}
		$image = $this->imageFormat->createFromFile($absolutePathToFile);
		return $image;
	}

	/**
	 * applies transfromation to image
	 *
	 * @return string
	 * @throws EditedImageNotSaved
	 * @throws FunctionNameNotSet
	 */
	public function transformate() {
		$functionName = $this->args[0];
		$newImageName = $this->pathInfo["filename"] . "-" . $functionName;
		switch ($functionName) {
			case "negate":
					$this->expectNumberOfArguments(0);
					imagefilter($this->image, IMG_FILTER_NEGATE);
				break;
			case "grayscale":
					$this->expectNumberOfArguments(0);
					imagefilter($this->image, IMG_FILTER_GRAYSCALE);
				break;
			case "brightness":
				$this->expectNumberOfArguments(1);
				$arg = $this->toInt($this->args[2]);
				$newImageName .= "-$arg";
				imagefilter($this->image, IMG_FILTER_BRIGHTNESS, $arg);
				break;
			case "contrast":
				$this->expectNumberOfArguments(1);
				$arg = $this->toInt($this->args[2]);
				$newImageName .= "-$arg";
				imagefilter($this->image, IMG_FILTER_CONTRAST, $arg);
				break;
			case "colorize":
				$this->expectNumberOfArguments(4);
				$args = $this->toInt(array($this->args[2], $this->args[3], $this->args[4], $this->args[5]));
				$newImageName .= "-" . $args[0] . "-" . $args[1] . "-" . $args[2]  . "-" . $args[3];
				imagefilter($this->image, IMG_FILTER_COLORIZE, $args[0], $args[1], $args[2], $args[3]);
				break;
			case "blur":
				$this->expectNumberOfArguments(1);
				$arg = $this->toInt($this->args[2]);
				$newImageName .= "-$arg";
				imagefilter($this->image, IMG_FILTER_SMOOTH, $arg);
				break;
			default:
				throw new FunctionNameNotSet("Jméno funkce pro zpracování obrázku není nadefinováno, nebo nerozpoznáno. " . ($functionName ? "\"" . $functionName . "\"" : ""));
				break;
		}
		$newImagePath = $this->pathInfo["dirname"] . DIRECTORY_SEPARATOR . $newImageName . "." . $this->pathInfo["extension"];
		
		$newAbsolutePath = $this->directoryDiver->getPath() . $newImagePath;
		$saved = $this->imageFormat->saveToFile($this->image, $newAbsolutePath);
		// @codeCoverageIgnoreStart
		if(!$saved) {
			throw new EditedImageNotSaved("Upravený obrázek formátu " . get_class($this->imageFormat) . ", ukládaný do souboru $newAbsolutePath  se nepodařilo uložit.");
		}
		// @codeCoverageIgnoreEnd
		return $newImagePath;
	}
	
	/**
	 * converts given arguments to integer and returns
	 * 
	 * @param array|string $args
	 * @return array|string
	 * @throws InvalidArgument
	 */
	private function toInt($args) {
		if(is_array($args)) {
			$return = [];
			$count = count($args);
			for ($i = 0; $i < $count; $i++) {
				//get string representation of object than convert to int
				$return[] = (int) (string) $args[$i];
			}
		} else {
			$return = (int) (string) $args;
		}
		return $return;
	}
	
	/**
	 * checks whenever number of arguments gotten into class is expected, first two are name of function and path to the file, therefore are not take into account
	 * 
	 * @param int $expectedNumber
	 * @throws UnexpectedNumberOfArgument
	 */
	private function expectNumberOfArguments($expectedNumber) {
		$count = count($this->args) - 2;
		$unexpected = !(is_numeric($expectedNumber) && $expectedNumber === $count);
		if($unexpected) {
			throw new UnexpectedNumberOfArgument("Neočekávaný počet argumentů pro změnu obrázku. Poskytnuto ".$count.". Očekáváno " . $expectedNumber . ".");
		}
	}
}

class ImageErrorException extends PreprasorErrorException {

}

class FileIsNotReadable extends ImageErrorException {

}

class NotSupportedImageExtension extends ImageErrorException {

}

class InvalidArgument extends ImageErrorException {

}

class EditedImageNotSaved extends ImageErrorException {

}

class UnexpectedNumberOfArgument extends ImageErrorException {

}

class FunctionNameNotSet extends ImageErrorException {

}
<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Helper;

use Preprasor\PreprasorErrorException;

/**
 * Class for checking directory's availability
 *
 * @package Preprasor\Preprocess\Helper
 */
class DirectoryDiver {
	/**
	 * @var string 
	 */
	public $pathToDirectory;

	/**
	 * DirectoryDiver constructor.
	 *
	 * @param $pathToDirectory
	 * @throws DirectoryNotFound
	 */
	public function __construct($pathToDirectory) {
		if(!file_exists($pathToDirectory)) {
			throw new DirectoryNotFound("Adresář $pathToDirectory se nepodařilo nalézt.");	
		}
		$this->pathToDirectory = $pathToDirectory;
	}
	/**
	 * returns its path to directory
	 * 
	 * @return string
	 */
	public function getPath() {
		return $this->pathToDirectory;
	}
}

class DirectoryDiverErrorException extends PreprasorErrorException {

}

class DirectoryNotFound extends DirectoryDiverErrorException {

}

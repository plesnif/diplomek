<?php

/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Helper\Factory;

use Preprasor\Preprocess\Tokenize\Streamer;
use Preprasor\PreprasorErrorException;
use Preprasor\Preprocess\Helper\DirectoryDiver;
use Preprasor\Preprocess\Tokenize\Token\TokenTypes;
use Preprasor\Preprocess\Tokenize\RegExp\StandardRegExps;
use Preprasor\Preprocess\Tokenize\Match\MatchTypes;
use Preprasor\Preprocess\Tokenize\Tokenizer\StandardTokenizer;
use Preprasor\Preprocess\Parse\Parser;
use Preprasor\Preprocess\Tokenize\Tokenizer\I\Tokenizerable;
use Preprasor\Preprasor;
use Preprasor\Config\Config;
use Preprasor\Config\Setting\Setting;
use Preprasor\Config\Setting\StandardWebProduction;
use Preprasor\Preprocess\I\FactoryCapable;
use Preprasor\Preprocess\Parse\Parser;
use Preprasor\Preprasor;
use Preprasor\Preprocess\I\Preprocessible;

/**
 * SimplePreprocessorFactory
 */
class StandardPreprocessorFactory implements FactoryCapable {
	/**
	 * creates streamer
	 * 
	 * @param string $input
	 * @return Streamer
	 */
	public function createStreamer($input) {
		$sanitizer = new Sanitizer;			
		$sanitizedInput = $sanitizer->sanitizeInput($input);
		$streamer = new Streamer($sanitizedInput);
		PreprasorErrorException::setStreamer($streamer);
		return $streamer;
	}
	/**
	 * creates direcory diver
	 * 
	 * @param string $pathToWorkingDirectory
	 * @return DirectoryDiver
	 */
	public function createDirectoryDiver($pathToWorkingDirectory) {
		return new DirectoryDiver($pathToWorkingDirectory);
	}
	/**
	 * creates tokenizer
	 * 
	 * @param Streamer $streamer
	 * @return Tokenizerable
	 */
	public function createTokenizer(Streamer $streamer, DirectoryDiver $directoryDiver) {
		$regExps = new StandardRegExps;
		$matchTypes = new MatchTypes($regExps);		
		$tokenTypes = new TokenTypes;
		$specialChars = new SpecialCharacters;
		return new StandardTokenizer($streamer, $matchTypes, $tokenTypes, $specialChars, $directoryDiver);
	}
	/**
	 * creates parser
	 * 
	 * @param Tokenizerable $tokenizer
	 * @param DirectoryDiver $directoryDiver
	 * @return Parser
	 */
	public function createParser(Tokenizerable $tokenizer, DirectoryDiver $directoryDiver) {
		return new Parser($tokenizer, $directoryDiver);
	}
	/**
	 * creates preprocessor
	 * 
	 * @param string $input
	 * @param string $pathToWorkingDirectory
	 * @return Preprocessible
	 */
	public function createPreprocessor($input, $pathToWorkingDirectory) {
		$streamer = $this->createStreamer($input);
		$directoryDiver = $this->createDirectoryDiver($pathToWorkingDirectory);
		$tokenizer = $this->createTokenizer($streamer, $directoryDiver);
		$parser = $this->createParser($tokenizer, $directoryDiver);
		return new Preprocessor($parser);
	}
	/**
	 * creates preprasor
	 * 
	 * @param string $input
	 * @param string $pathToWorkingDirectory
	 * @param Setting $setting
	 * @return Preprasor
	 */
	public function createPreprasor($input, $pathToWorkingDirectory, Setting $setting = null) {
		if(is_null($setting)) {
			$setting = new StandardWebProduction;
		}
		Config::setSetting($setting);
		$preprocessor = $this->createPreprocessor($input, $pathToWorkingDirectory);
		return new Preprasor($preprocessor);
	}
}

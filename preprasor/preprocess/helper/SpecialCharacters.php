<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Helper;

/**
 * Contains all present token types for comparation purpouses
 *
 * @package Preprasor\Preprocess\Tokenize\Sign
 */
class SpecialCharacters {
	/**
	 * @var string
	 */
	public $replacementCharacter = 0xFFFD;
	
	/**
	 * @var array
	 */
	public $surrogateCodePoints = [0xD800, 0xDFFF];
	
	/**
	 * @var string
	 */
	public $maximumAllowedCodePoint = 0x10FFFF;
	
}
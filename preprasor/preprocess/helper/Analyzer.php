<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Helper;

/**
 * Class responsible for runtime analitycs
 *
 * @package Preprasor\Preprocess\Helper
 */
class Analyzer {	
	/**
	 * prints memory and time statistics
	 */
	public function printFinalAnalysis() {
		$memoryUsage = memory_get_usage();			
		echo "<br/>\n";
		echo "Použitá paměť: " . $memoryUsage . " bytů ~ " . round($memoryUsage/1024,2) . "Ki bytů<br/>\n";
		echo "Čas běhu programu: " . (\microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]) . " ms<br/>\n";
	}
}

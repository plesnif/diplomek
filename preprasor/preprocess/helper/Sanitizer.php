<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Helper;

use Preprasor\PreprasorErrorException;
use ForceUTF8\Encoding;

/**
 * Class responsible for converting encoding and preprocessing input stream
 *
 * @package Preprasor\Preprocess\Helper
 */
class Sanitizer {
	/**
	 * sanitizes string $input
	 * 
	 * @param string $input
	 * @return string
	 * @throws InvalidInput
	 */
	public function sanitizeInput($input) {
        if(!is_string($input)) {
            throw new InvalidInput("Vstup není typu string, ale " . gettype($input) . ".");
        }
		$input = Encoding::fixUTF8($input);

		$input = $this->preprocessInputStream($input);
		return $input;
	}
	/**
	 * Replace any U+000D CARRIAGE RETURN (CR) code points, U+000C FORM FEED (FF) code points, or pairs of U+000D CARRIAGE RETURN (CR) followed by U+000A LINE FEED (LF), by a single U+000A LINE FEED (LF) code point. 
	 * Replace any U+0000 NULL code point with U+FFFD REPLACEMENT CHARACTER (�). 
		* \0x000D = [UNICODE] CR: Carriage Return, U+000D
		* \0x000C = [UNICODE] FF: Form Feed, U+000C
		* \0x000D000A = [UNICODE] CR+LF: CR (U+000D) followed by LF (U+000A)
		* \0x000A = [UNICODE] LF: Line Feed, U+000A
		* \0x0000 = [UNICODE] NULL: U+0000
		* \0xFFFD = [UNICODE] REPLACEMENT CHARACTER: �, U+FFFD
	 * 
	 * @param string $input
	 * @return string
	 */
	private function preprocessInputStream($input) {
		$search = array("\x000D\x000A", "\x000D", "\x000C",  "\x0000");
		$replace = array("\x000A", "\x000A", "\x000A", "\xFFFD");
		return str_replace($search, $replace, $input);
	}
}

class SanitizerErrorException extends PreprasorErrorException {
	
}

class InvalidInput extends SanitizerErrorException {

}


<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess;

use Preprasor\Config\Config;
use Preprasor\PreprasorErrorException;
use Preprasor\Preprocess\I\Initiable;
use Preprasor\Preprocess\Parse\Parser;
use Preprasor\Preprocess\I\Preprocessible;

/**
 * Class responsible for preprocessing
 *
 * @package Preprasor\Preprocess
 */
class Preprocessor implements Preprocessible {
	/**
	 * @var Parser
	 */
    protected $parser;

	/**
	 * @var Config
	 */
	protected $config;
	
	public function __construct(Parser $parser) {
		$this->parser = $parser;
	}

	/**
	 * {@inheritDoc}
	 */
    public function init() {
        $this->parser->init();
    }
	
	/**
	 * {@inheritDoc}
	 */
    public function dumpCSSTree() {
        $this->parser->dumpCSSTree();
    }
	
	/**
	 * {@inheritDoc}
	 */
    public function printCSSOutput() {
        return $this->parser->printCSSOutput();
    }
	
	/**
	 * {@inheritDoc}
	 */
    public function printTokens() {
        $this->parser->printTokens();
    }

	/**
	 * sets configuration
	 *
	 * @param Config $config
	 */
	public function setConfig(Config $config) {
		$this->config = $config;
	}
}

class PreprocessorErrorException extends PreprasorErrorException {

}


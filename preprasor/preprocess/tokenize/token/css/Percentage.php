<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Token\Css;

use Preprasor\Preprocess\Tokenize\Token\Base\Token;
use Preprasor\Constant;

/**
 * Class representing Percentage token
 *
 * @package Preprasor\Preprocess\Tokenize\Token\Css
 */
class Percentage extends Token {
	/**
	 * @var bool 
	 */
	protected $hasNumericValue = true;
	
	/**
	 * {@inheritDoc}
	 */
    public function printOut($depth = 0) {
        return $this->value() . Constant::PERCENTAGE_SIGN;
    }
	/**
	 * {@inheritDoc}
	 */
	public function getOccupiedLength() {
		return parent::getOccupiedLength() + 1;
	}
	
} 
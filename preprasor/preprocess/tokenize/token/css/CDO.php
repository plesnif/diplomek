<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Token\Css;

use Preprasor\Preprocess\Tokenize\Token\Base\Token;
use Preprasor\Constant;

/**
 * Class representing CDO token "<!--"
 *
 * @package Preprasor\Preprocess\Tokenize\Token\Css
 */
class CDO extends Token {
	/**
	 * @var string
	 */
	protected $value = Constant::LESS_THAN . Constant::EXCLAMATION_MARK . Constant::HYPHEN_MINUS . Constant::HYPHEN_MINUS;
} 
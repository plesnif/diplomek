<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Token\Css;

use Preprasor\Preprocess\Tokenize\Token\Base\Token;
use Preprasor\Constant;

/**
 * Class representing Semicolon token
 *
 * @package Preprasor\Preprocess\Tokenize\Token\Css
 */
class Semicolon extends Token {
	/**
	 * @var string
	 */
	protected $value = Constant::SEMICOLON;
} 
<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Token\Css;

use Preprasor\Preprocess\Tokenize\Token\Base\Token;
use Preprasor\Constant;

/**
 * Class representing SuffixMatch token
 *
 * @package Preprasor\Preprocess\Tokenize\Token\Css
 */
class SuffixMatch extends Token {
	/**
	 * @var string
	 */
	protected $value = Constant::DOLAR . Constant::EQUALS;
} 
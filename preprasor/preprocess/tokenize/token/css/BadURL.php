<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Token\Css;

/**
 * Class representing BadURL token
 *
 * @package Preprasor\Preprocess\Tokenize\Token\Css
 */
class BadURL extends URL {
	/**
	 * {@inheritDoc}
	 */
	public function getOccupiedLength() {
		return parent::getOccupiedLength() + 7;
	}
} 
<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Token\Css;

use Preprasor\Preprocess\Tokenize\Token\Base\Token;
use Preprasor\Constant;
use Preprasor\PreprasorErrorException;

/**
 * Class representing Hash token
 *
 * @package Preprasor\Preprocess\Tokenize\Token\Css
 */
class Hash extends Token {
	/**
	 * @var int 
	 */
	private $flag;

	/**
	 * Hash constructor.
	 *
	 * @param null $value
	 * @param null $flag
	 */
	public function __construct($value = null, $flag = null) {
		parent::__construct($value);
		if(is_null($flag)) {
			$flag = Constant::HASH_FLAG_UNRESTRICTED;
		}
		$this->flag = $flag;
	}

	/**
	 * sets token flag
	 * 
	 * @param string $flag
	 * @throws UnknownFlag
	 */
	public function setFlag($flag) {
		if($flag != (Constant::HASH_FLAG_UNRESTRICTED || Constant::HASH_FLAG_ID) ) {
			throw new UnknownFlag("Nelze nastavit vlajku hash tokenu. Konstanta vlajky nerozpoznána - $flag");
		}
		$this->flag = $flag;
	}
	
	/**
	 * {@inheritDoc}
	 */
    public function printOut($depth = 0) {
        return Constant::NUMBER . $this->value();
    }

	/**
	 * {@inheritDoc}
	 */
	public function getOccupiedLength() {
		return parent::getOccupiedLength() + 1;
	}
	
}

class UnknownFlag extends PreprasorErrorException {
	
}

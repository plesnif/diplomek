<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Token\Css;

use Preprasor\Preprocess\Tokenize\Token\Base\Token;
use Preprasor\Constant;

/**
 * Class representing Comma token
 *
 * @package Preprasor\Preprocess\Tokenize\Token\Css
 */
class Comma extends Token {
	/**
	 * @var string
	 */
	protected $value = Constant::COMMA;
	
	/**
	 * {@inheritDoc}
	 */
	public function isSupportive() {
		return true;
	}
} 
<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Token\Css;

use Preprasor\Constant;

/**
 * Class representing URL token
 *
 * @package Preprasor\Preprocess\Tokenize\Token\Css
 */
class URL extends StringToken {
	/**
	 * {@inheritDoc}
	 */
    public function printOut($depth = 0) {
        return Constant::KEYWORD_URL . Constant::LEFT_PARENTHESIS . $this->quote . $this->value(). $this->quote . Constant::RIGHT_PARENTHESIS;
    }
	/**
	 * {@inheritDoc}
	 */
	public function getOccupiedLength() {
		return parent::getOccupiedLength() + 7;
	}
} 
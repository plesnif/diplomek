<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Token\Css;

use Preprasor\Preprocess\Tokenize\Token\Base\BlockStarting;
use Preprasor\Constant;

/**
 * Class representing Left Square Bracket token
 *
 * @package Preprasor\Preprocess\Tokenize\Token\Css
 */
class LeftSquareBracket extends BlockStarting {
	/**
	 * @var string
	 */
	protected $value = Constant::LEFT_SQUARE_BRACKET;
} 
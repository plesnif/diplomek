<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Token\Css;

use Preprasor\Preprocess\Tokenize\Token\Base\Token;
use Preprasor\Constant;

/**
 * Class representing DashMatch token
 *
 * @package Preprasor\Preprocess\Tokenize\Token\Css
 */
class DashMatch extends Token {
	/**
	 * @var string
	 */
	protected $value = Constant::VERTICAL_LINE . Constant::EQUALS;
}
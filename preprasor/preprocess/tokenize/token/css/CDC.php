<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Token\Css;

use Preprasor\Preprocess\Tokenize\Token\Base\Token;
use Preprasor\Constant;

/**
 * Class representing CDC token "-->"
 *
 * @package Preprasor\Preprocess\Tokenize\Token\Css
 */
class CDC extends Token {
	/**
	 * @var string
	 */
	protected $value =  Constant::HYPHEN_MINUS . Constant::HYPHEN_MINUS . Constant::GREATER_THAN;
} 
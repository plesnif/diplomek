<?php

/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Token\Css;

use Preprasor\Preprocess\Tokenize\Token\Base\Token;
use Preprasor\Constant;

/**
 * Class representing AtKeyword token
 *
 * @package Preprasor\Preprocess\Tokenize\Token\Css
 */
class AtKeyword extends Token {
	/**
	 * {@inheritDoc}
	 */
    public function printOut($depth = 0) {
        return Constant::COMMERCIAL_AT . $this->value();
    }
	
	/**
	 * {@inheritDoc}
	 */
	public function getOccupiedLength() {
		return parent::getOccupiedLength() + 1;
	}
	
	/**
	 * checks whenever at rule is mixin
	 * 
	 * @return bool
	 */
	public function isMixin() {
		return strtolower($this->value()) === Constant::KEYWORD_MIXIN;
	}
} 
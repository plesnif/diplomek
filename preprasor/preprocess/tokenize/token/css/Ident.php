<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Token\Css;

use Preprasor\Preprocess\Tokenize\Token\Base\Token;

/**
 * Class representing Ident token
 *
 * @package Preprasor\Preprocess\Tokenize\Token\Css
 */
class Ident extends Token {

} 
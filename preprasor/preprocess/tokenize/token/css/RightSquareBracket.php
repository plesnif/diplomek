<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Token\Css;

use Preprasor\Preprocess\Tokenize\Token\Base\Token;
use Preprasor\Constant;

/**
 * Class representing Right Square Bracket token
 *
 * @package Preprasor\Preprocess\Tokenize\Token\Css
 */
class RightSquareBracket extends Token {
	/**
	 * @var string
	 */
	protected $value = Constant::RIGHT_SQUARE_BRACKET;
} 
<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Token\Css;

use Preprasor\Preprocess\Tokenize\Token\Base\Token;
use Preprasor\PreprasorErrorException;
use Preprasor\Constant;
use Preprasor\Config\Config;

/**
 * Class StringToken
 *
 * @package Preprasor\Preprocess\Tokenize\Token\Css
 */
class StringToken extends Token {
	/**
	 * @var string
	 */
	protected $quote;

	/**
	 * StringToken constructor.
	 *
	 * @param string $quote
	 * @param null|Token $value
	 * @throws QuoteUndefined
	 */
	public function __construct($quote, $value = null) {
		parent::__construct($value);
		if(!($quote == Constant::APOSTROPHE || $quote == Constant::QUOTATION_MARK)) {
			throw new QuoteUndefined("Pro token typu " . get_class() . " řetězec musí byt definována jednoduchá nebo dvojitá uvozovka.");
		}
		if(Config::useDefaultQuotes()) {
			$this->quote = Config::defaultQuote();
		} else {
			$this->quote = $quote;
		}
		
	}
	
	/**
	 * {@inheritDoc}
	 */
    public function printOut($depth = 0) {
        return $this->quote .$this->value(). $this->quote;
	}
	/**
	 * {@inheritDoc}
	 */
	public function getOccupiedLength() {
		return parent::getOccupiedLength() + 2;
	}
} 

class QuoteUndefined extends PreprasorErrorException {
	
}
<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Token\Css;

use Preprasor\Preprocess\Tokenize\Token\Base\UnitSet;

/**
 * Class representing Dimension token
 *
 * @package Preprasor\Preprocess\Tokenize\Token\Css
 */
class Dimension extends UnitSet {
	/**
	 * @var bool 
	 */
	protected $hasNumericValue = true;
	
	/**
	 * {@inheritDoc}
	 */
	public function getOccupiedLength() {
		return parent::getOccupiedLength() + mb_strlen($this->unitSet);
	}
	/**
	 * {@inheritDoc}
	 */
	public function value() {
		return parent::value() . $this->unitSet;
	}
} 
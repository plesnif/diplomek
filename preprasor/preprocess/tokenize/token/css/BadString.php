<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Token\Css;

/**
 * Class representing BadString token
 *
 * @package Preprasor\Preprocess\Tokenize\Token\Css
 */
class BadString extends StringToken {
	/**
	 * {@inheritDoc}
	 */
	public function getOccupiedLength() {
		return parent::getOccupiedLength() + 1;
	}
} 
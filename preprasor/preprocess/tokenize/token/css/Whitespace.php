<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Token\Css;

use Preprasor\Preprocess\Tokenize\Token\Base\Token;
use Preprasor\Constant;

/**
 * Class representing Whitespace token
 *
 * @package Preprasor\Preprocess\Tokenize\Token\Css
 */
class Whitespace extends Token {
	/**
	 * @var string
	 */
	protected $value = " ";
	
	/**
	 * {@inheritDoc}
	 */
    public function printOut($depth = 0) {
        return $this->value;
    }
	
	/**
	 * {@inheritDoc}
	 */
	public function isSupportive() {
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function isNewline() {
		return $this->value == Constant::NEW_LINE;
	}
} 
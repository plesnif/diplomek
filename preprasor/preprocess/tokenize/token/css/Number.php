<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Token\Css;

use Preprasor\Preprocess\Tokenize\Token\Base\FlagToken;

/**
 * Class representing Number token
 *
 * @package Preprasor\Preprocess\Tokenize\Token\Css
 */
class Number extends FlagToken {
	/**
	 * @var bool 
	 */
	protected $hasNumericValue = true;

} 
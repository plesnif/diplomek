<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Token\Css;

use Preprasor\Preprocess\Tokenize\Token\Base\Token;
use Preprasor\Constant;

/**
 * Class representing Comment token
 *
 * @package Preprasor\Preprocess\Tokenize\Token\Css
 */
class Comment extends Token {
	/**
	 * {@inheritDoc}
	 */
	public function printOut($depth) {
		return $this->getTabs($depth) .	Constant::SLASH . Constant::ASTERISK . $this->value() . Constant::ASTERISK . Constant::SLASH;
	}
	/**
	 * {@inheritDoc}
	 */
	public function getOccupiedLength() {
		return parent::getOccupiedLength() + 4;
	}

} 
<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Token\Css;

use Preprasor\Preprocess\Tokenize\Token\Base\Token;
use Preprasor\Constant;

/**
 * Class representing Function token
 *
 * @package Preprasor\Preprocess\Tokenize\Token\Css
 */
class FunctionToken extends Token {
	/**
	 * {@inheritDoc}
	 */
	public function printOut($depth) {
		return $this->value() . Constant::LEFT_PARENTHESIS;
	}
} 
<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Token\Css;

use Preprasor\Preprocess\Tokenize\Token\Base\Token;

/**
 * Class representing Delim token
 *
 * @package Preprasor\Preprocess\Tokenize\Token\Css
 */
class Delim extends Token {

} 
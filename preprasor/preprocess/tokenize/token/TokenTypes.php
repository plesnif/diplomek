<?php

/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Token;

use Preprasor\Preprocess\Tokenize\Token\Css\Comment;
use Preprasor\Preprocess\Tokenize\Token\Css\Whitespace;
use Preprasor\Preprocess\Tokenize\Token\Css\Ident;
use Preprasor\Preprocess\Tokenize\Token\Css\Number;
use Preprasor\Preprocess\Tokenize\Token\Css\StringToken;
use Preprasor\Preprocess\Tokenize\Token\Css\BadString;
use Preprasor\Preprocess\Tokenize\Token\Css\RightParenthesis;
use Preprasor\Preprocess\Tokenize\Token\Css\LeftParenthesis;
use Preprasor\Preprocess\Tokenize\Token\Css\Hash;
use Preprasor\Preprocess\Tokenize\Token\Css\Comma;
use Preprasor\Preprocess\Tokenize\Token\Css\CDC;
use Preprasor\Preprocess\Tokenize\Token\Css\CDO;
use Preprasor\Preprocess\Tokenize\Token\Css\URL;
use Preprasor\Preprocess\Tokenize\Token\Css\BadURL;
use Preprasor\Preprocess\Tokenize\Token\Css\AtKeyword;
use Preprasor\Preprocess\Tokenize\Token\Css\Percentage;
use Preprasor\Preprocess\Tokenize\Token\Css\Dimension;
use Preprasor\Preprocess\Tokenize\Token\Css\Delim;
use Preprasor\Preprocess\Tokenize\Token\Css\IncludeMatch;
use Preprasor\Preprocess\Tokenize\Token\Css\DashMatch;
use Preprasor\Preprocess\Tokenize\Token\Css\SuffixMatch;
use Preprasor\Preprocess\Tokenize\Token\Css\PrefixMatch;
use Preprasor\Preprocess\Tokenize\Token\Css\SubstringMatch;
use Preprasor\Preprocess\Tokenize\Token\Css\Colon;
use Preprasor\Preprocess\Tokenize\Token\Css\Semicolon;
use Preprasor\Preprocess\Tokenize\Token\Css\RightSquareBracket;
use Preprasor\Preprocess\Tokenize\Token\Css\LeftSquareBracket;
use Preprasor\Preprocess\Tokenize\Token\Css\RightCurlyBracket;
use Preprasor\Preprocess\Tokenize\Token\Css\LeftCurlyBracket;
use Preprasor\Preprocess\Tokenize\Token\Css\FunctionToken;
use Preprasor\Preprocess\Tokenize\Token\Css\Important;
use Preprasor\Preprocess\Tokenize\Token\Css\EOF;

use Preprasor\Preprocess\Tokenize\Token\Preprocessor\Variable;
use Preprasor\Preprocess\Tokenize\Token\Preprocessor\Mixin;
use Preprasor\Preprocess\Tokenize\Token\Preprocessor\ExtendsToken;
use Preprasor\Preprocess\Tokenize\Token\Preprocessor\ExcludeToken;
use Preprasor\Preprocess\Tokenize\Token\Preprocessor\ClassToken;

use Preprasor\Constant;

use Preprasor\Preprocess\Tokenize\I\Settable;

/**
 * Contains all present token types for comparation purpouses
 *
 * @package Preprasor\Preprocess\Tokenize\Token
 */
class TokenTypes implements Settable {
	//preprocessor defined types
	public $whitespace;
	public $comment;
	public $lineComment;
	public $ident;
	public $number;
	public $badString;
	public $string;
	public $leftParenthesis;
	public $rightParenthesis;
	public $hash;
	public $comma;
	public $CDC;
	public $CDO;
	public $URL;
	public $badURL;
	public $atKeyword;
	public $percentage;
	public $delim;
	public $dimension;
	public $includeMatch;
	public $dashMatch;
	public $suffixMatch;
	public $prefixMatch;
	public $substringMatch;
	public $semicolon;
	public $colon;
	public $leftSquareBracket;
	public $rightSquareBracket;
	public $leftCurlyBracket;
	public $rightCurlyBracket;
	public $function;
    public $important;
	public $EOF;
	//preprocessor defined types
    public $variable;
	public $mixin;
    public $extends;
    public $excludeFrom;
    public $class;	
	
	/**
	 * defines tokens types
	 */
	public function setContent() {		
        //standard css tokens
        $this->whitespace = new Whitespace;
        $this->comment = new Comment;
        $this->ident = new Ident;
        $this->number = new Number(null, "");
        $this->badString = new BadString(Constant::APOSTROPHE);
        $this->string = new StringToken(Constant::APOSTROPHE);
        $this->rightParenthesis = new RightParenthesis();
        $this->leftParenthesis = new LeftParenthesis(Constant::LEFT_PARENTHESIS, $this->rightParenthesis);
        $this->hash = new Hash;
        $this->comma = new Comma;
        $this->CDC = new CDC;
        $this->CDO = new CDO;
        $this->URL = new URL(Constant::APOSTROPHE);
        $this->badURL = new BadURL(Constant::APOSTROPHE);
        $this->atKeyword = new AtKeyword;
        $this->percentage = new Percentage;
        $this->dimension = new Dimension(null, null, NULL);
        $this->delim = new Delim;
        $this->includeMatch = new IncludeMatch;
        $this->dashMatch = new DashMatch;
        $this->suffixMatch = new SuffixMatch;
        $this->prefixMatch = new PrefixMatch;
        $this->substringMatch = new SubstringMatch;
        $this->colon = new Colon;
        $this->semicolon = new Semicolon;
        $this->rightSquareBracket = new RightSquareBracket;
        $this->leftSquareBracket = new LeftSquareBracket(Constant::LEFT_SQUARE_BRACKET, $this->rightSquareBracket);
        $this->rightCurlyBracket = new RightCurlyBracket;
        $this->leftCurlyBracket = new LeftCurlyBracket(Constant::LEFT_CURLY_BRACKET, $this->rightCurlyBracket);
        $this->function = new FunctionToken;
        $this->important = new Important;
        $this->EOF = new EOF;		
        //preprocessor defined css tokens
        $this->variable = new Variable;
		$this->mixin = new Mixin;
        $this->extends = new ExtendsToken;
        $this->excludeFrom = new ExcludeToken;
        $this->class = new ClassToken;
	}
}
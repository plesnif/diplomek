<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Token\Base;

/**
 * Class representing Token that is starting Block ( {}, [], () )
 *
 * @package Preprasor\Preprocess\Tokenize\Token\Base
 */
abstract class BlockStarting extends Token {

	/**
	 * @var Token reflection Token (if actual token represents "{", mirror token is "}")
	 */
    private $mirrorToken;

	/**
	 * BlockStarting constructor.
	 *
	 * @param null  $value
	 * @param Token $mirrorToken
	 */
    public function __construct($value, Token $mirrorToken) {
        parent::__construct($value);
        $this->mirrorToken = $mirrorToken;
    }

	/**
	 * returns reflection Token
	 * 
	 * @return Token
	 */
    public function mirrorToken() {
        return $this->mirrorToken;
    }

	/**
	 * decides how to print block
	 * 
	 * @return boolean
	 */
	public function printOutAsBlock() {
		return false;
	}
}
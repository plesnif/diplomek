<?php

/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Token\Base;

/**
 * Abstract class representing token with unit set
 *
 * @package Preprasor\Preprocess\Tokenize\Token\Base
 */
abstract class UnitSet extends FlagToken {
	/**
	 *
	 * @var string 
	 */
	protected $unitSet;

	/**
	 * UnitSet constructor.
	 *
	 * @param null $value
	 * @param null $flag
	 * @param      $unitSet
	 */
	public function __construct($value, $flag, $unitSet) {
		parent::__construct($value, $flag);
		$this->unitSet = $unitSet;
	}
}
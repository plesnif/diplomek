<?php

/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Token\Base;

use Preprasor\Constant;

/**
 * Abstract class representing token with unit set
 *
 * @package Preprasor\Preprocess\Tokenize\Token\Base
 */
abstract class FlagToken extends Token {
	/**
	 * @var int 
	 */
	private $flag;

	/**
	 * FlagToken constructor.
	 *
	 * @param null $value
	 * @param null $flag
	 */
	public function __construct($value=null, $flag=null) {
		parent::__construct($value);
		if($flag === null) {
			$flag = Constant::NUMBER_TYPE_NUMBER;
		}
		$this->flag = $flag;
	}
}
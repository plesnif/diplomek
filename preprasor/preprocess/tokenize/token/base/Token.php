<?php

/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Token\Base;

use Preprasor\PreprasorErrorException;
use Preprasor\Preprocess\I\Compositable;
use Preprasor\Constant;

/**
 * Basic abstract class representing Preprasor Token
 */
abstract class Token implements Compositable {
	/**
	 * @var string|Compositable
	 */
	protected $value;
	
	/**
	 * @var bool 
	 */
	protected $hasNumericValue = false;

	/**
	 * Token constructor.
	 *
	 * @param null $value
	 */
	public function __construct($value = null) {
		if(!is_null($value)) {
			$this->setValue($value);
		}
	}

	/**
	 * returns inner value
	 * 
	 * @return string|Compositable
	 */
	public function value() {
		if(is_object($this->value)) {
			return $this->value->value();
		}
		return $this->value;
	}

	/**
	 * sets token value
	 *
	 * @param $value
	 * @throws WrongInputValue
	 */
	public function setValue($value) {
	  	if($this->hasNumericValue) {
			if(!is_numeric($value)) {
				throw new WrongInputValue("Vstupní hodnota není numerická - $value");
			}
		}
		$this->value = $value;
	}

	/**
	 * returns class name
	 * 
	 * @return string
	 */
	public function type() {
		return get_class();
	}

	/**
	 * tests whenever actual Token is same type as $token
	 * 
	 * @param Token $token
	 * @return boolean
	 */
	public function isType(Token $token) {
		return $this instanceof $token;
	}
	
	/**
	 * tests whenever actual Token starts block
	 * 
	 * @return boolean
	 */
	public function startsBlock() {
		return $this instanceof BlockStarting;
	}

	/**
	 * tests whenever actual Token is one of Token in $types
	 * 
	 * @param array $types
	 * @return boolean
	 */
	public function belongsInTypes(array $types) {
		foreach($types as $type) {
			if($this->isType($type)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * gets matching sequence - regular expression
	 * 
	 * @return string
	 * @throws MatchTokenNotSet
	 */
	public function getMatchSequence() {
		if(is_object($this->value)) {
			return $this->value->getMatchSequence();
		} else {
			throw new MatchTokenNotSet("Není žádná matchovací sekvence.");
		}
	}

	/**
	 * prints Token value, defined in its successors, $depth is sometimes used for tab offset
	 * 
	 * @param int $depth
	 * @return string
	 */
	public function printOut($depth = 0) {
		return $this->value();
	}

	/**
	 * function called in stage of preprocessing, defined in successors or stays empty
	 */
	public function prepras() {

	}
	
	/**
	 * returns string representing object
	 * 
	 * @return string
	 */
	public function __toString() {
		return (string) $this->value();
	}
	
	/**
	 * gets number of characters occupied in input stream by this token
	 * 
	 * @return int
	 */
	public function getOccupiedLength() {
		return mb_strlen($this->value);
	}
	
	/**
	 * returns indentation according to $depth
	 * 
	 * @param int $depth
	 * @return string
	 */
	protected function getTabs($depth) {
		return str_repeat(Constant::TAB, $depth);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function isSupportive() {
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function name() {
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function isNewline() {
		return false;
	}
}


class TokenErrorException extends PreprasorErrorException {

}

class MatchTokenNotSet extends TokenErrorException {

}

class ValueAlreadySet extends TokenErrorException {

}

class WrongInputValue extends TokenErrorException {

}
<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Token\Preprocessor;

use Preprasor\Preprocess\Tokenize\Token\Base\Token;
use Preprasor\Constant;

/**
 * Class representing ClassToken token
 *
 * @package Preprasor\Preprocess\Tokenize\Token\Preprocessor
 */
class ClassToken extends Token {
	/**
	 * {@inheritDoc}
	 */
    public function printOut($depth = 0) {
        return Constant::FULL_STOP . $this->value();
    }
}


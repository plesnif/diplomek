<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Token\Preprocessor;

use Preprasor\Preprocess\Tokenize\Token\Base\Token;

/**
 * Class representing Extends token
 *
 * @package Preprasor\Preprocess\Tokenize\Token\Preprocessor
 */
class ExtendsToken extends Token {

}


<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Helper;

/**
 * Helper class for tokenization stage
 *
 * @package Preprasor\Preprocess\Tokenize\Helper
 */
class ThreeTuple {
	/**
	 * @var string
	 */
	private $repr;

	/**
	 * @var string
	 */
	private $value;

	/**
	 * @var string
	 */
	private $type;

	/**
	 * ThreeTuple constructor.
	 *
	 * @param $repr
	 * @param $value
	 * @param $type
	 */
	public function __construct($repr, $value, $type) {
		$this->repr = $repr;
		$this->value = $value;
		$this->type = $type;
	}

	/**
	 * gets value
	 *
	 * @return string
	 */
	public function getValue() {
		return $this->value;
	}

	/**
	 * gets type
	 * @return string
	 */
	public function getType() {
		return $this->type;			
	}
}

<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Helper;

use Preprasor\Preprocess\Tokenize\Token\Base\Token;
use Preprasor\Preprocess\Tokenize\Tokenizer\Tokenizer;

/**
 * Class representing two point option, returns one of given Tokens based on character matching
 *
 * @package Preprasor\Preprocess\Tokenize\Helper
 */
class Splitter {
	/**
	 * @var string
	 */
	private $testedChar;
	
	/**
	 * @var string
	 */
	private $whishedChar;
	
	/**
	 * @var Token
	 */
	private $ifMatched;
	
	/**
	 * @var Token
	 */
	private $otherwise;

	/**
	 * @var Tokenizer
	 */
	private $tokenizer;

	/**
	 * Splitter constructor.
	 *
	 * @param string    $testedChar
	 * @param string    $whishedChar
	 * @param Token     $ifMatched
	 * @param Token     $otherwise
	 * @param Tokenizer $tokenizer
	 */
	public function __construct($testedChar, $whishedChar, Token $ifMatched, Token $otherwise, Tokenizer $tokenizer) {
		$this->testedChar = $testedChar;
		$this->whishedChar = $whishedChar;
		$this->ifMatched = $ifMatched;
		$this->otherwise = $otherwise;
		$this->tokenizer = $tokenizer;
	}

	/**
	 * return one of Tokens given in constructor based on match of $testedChar and $whishedChar
	 * 
	 * @return Token
	 */
	public function getOption() {
		$this->tokenizer->consumeCodePoint();
		if($this->testedChar == (string) $this->whishedChar) {
			/* two code points are consumed */
			$this->tokenizer->consumeCodePoint();
			return $this->ifMatched;
		} else {
			return $this->otherwise;
		}
	}
}

<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */
namespace Preprasor\Preprocess\Tokenize;

use Preprasor\PreprasorErrorException;
use Preprasor\Preprocess\Tokenize\Token\Base\Token;
use Preprasor\Preprocess\Tokenize\Match\Match;
use Preprasor\Config\Config;
use Preprasor\Constant;

/**
 * Class for controlling input stream of code point, moves caret through input and test input for token matching
 *
 * @package Preprasor\Preprocess\Tokenize
 */
class Streamer {	
	/** 
	 * @var string 
	 */
	public $input;
	
	/** 
	 * @var integer 
	 */
	public $position = 0;
	
	/** 
	 * @var integer 
	 */
	private $length;
	
	/** 
	 * @var integer 
	 */
	private $lastTokenPosition = 0;
	
	/** 
	 * @var integer 
	 */
	private $column = 0;
	
	/** 
	 * @var integer 
	 */
	private $row = 0;

	/**
	 * Streamer constructor.
	 *
	 * @param $input
	 */
	public function __construct($input) {
		$this->input = $input;
		$this->length = mb_strlen($input);
	}
	/**
	 * resets streamer
	 */
	public function reset() {
		$this->position = 0;
		$this->lastTokenPosition = 0;
		$this->column = 0;
		$this->row = 0;
	}

	/**
	 * moves caret position by $shift 
	 * 
	 * @param integer $shift
	 */
	public function movePosition($shift) {
		$this->position = $this->position + $shift;
	}

	/**
	 * checks whenever position exists in input
	 * 
	 * @param integer $position
	 * @return boolean
	 */
	public function positionExists($position) {
		if (!isset($this->input[$position])) {
			return false;
		}
		return true;
	}
	
	/**
	 * gets actual caret position
	 * 
	 * @return integer
	 */
	public function getPosition() {
		return $this->position;
	}

	/**
	 * sets caret position on $position
	 *
	 * @param int $position
	 * @return bool
	 */
	public function setPosition($position) {
        if($this->positionExists($position)) {
			$this->position = $position;
			return true;
        } else {
			return false;
		}
	}

	/**
	 * sets caret position one position back
	 */
	public function stepBack() {
		$this->setPosition($this->position-1);
	}
	
	/**
	 * checks whenever actual position exists in input
	 * 
	 * @return boolean
	 */
	public function actualPositionExists() {
		return $this->positionExists($this->position);
	}

	/**
	 * checks whenever position exists, throw exception otherwise
	 * 
	 * @param int $position
	 * @return boolean
	 * @throws PositionDoesNotExists
	 */
	public function checkPosition($position) {
		if($this->positionExists($position)) {
			return true;
		} else {
			throw new PositionDoesNotExists("Pozice $position(index od nuly) ve vstupu neexistuje. Délka řetězce je $this->length.");
		}
	}
	
	/**
	 * gets code point at specified $position if set, otherwise at actual position
	 * 
	 * @param integer $position
	 * @return string|false
	 */
	public function getCodePoint($position = 0) {
		if($position === 0) {
			$position = $this->position;
		}
		if($this->positionExists($position)) {
			return $this->input[$position];
		} else {
			return false;
		}
	}

	/**
	 * gets next code point
	 * 
	 * @return string
	 */
	public function getNextCodePoint() {
		return $this->lookAheadCodePoint(1);
	}
	
	/**
	 * looks ahead $lookAheadIndex chars from actaul position and return char if exists
	 * 
	 * @param int $lookAheadIndex
	 * @return string|false
	 */
	public function lookAheadCodePoint($lookAheadIndex = 1) {
		if($this->positionExists($this->position + $lookAheadIndex) === true) {
			return $this->getCodePoint($this->position + $lookAheadIndex);
		} else{
			return false;
		}
	}
	
	/**
	 * gets code point at actual position and moves caret
	 * 
	 * @return string|false
	 */
	public function consumeCodePoint() {
		$nextCodePointExists = $this->setPosition($this->position + 1);
		if($nextCodePointExists === true) {
			$codePoint = $this->getCodePoint();
			return $codePoint;
		} else {
			return false;
		}
	}
	
	/**
	 * consumes code point(s) of number $numberToConsume and moves caret
	 * 
	 * @param int $numberToConsume
	 * @return string|false
	 */
	public function consumeCodePoints($numberToConsume = 1) {
		$subStr = mb_substr($this->input, $this->position, $numberToConsume);
		$lookAhedCodePointOnPositionExists = $this->setPosition($this->position + $numberToConsume);
		if($lookAhedCodePointOnPositionExists === true) {
			return $subStr;
		} else {
			return false;
		}
	}
	
	/**
	 * prepares actual code point for reconsumation
	 */
	public function reconsumeCodePoint() {
		$newPosition = $this->position - 1;
		if($newPosition < 0) {
			$newPosition = 0;
		}
		$this->setPosition($newPosition);
	}
	
	/**
	 * tests whenever substring starting at actual position matches matching sequence in $expectedMatch(regular expression for token type) and returns found match in form of $token; 
	 * if returned array is contains more than one element, there is difference between string length of matching sequence of regular expression and string length of actual match ("[.*]" is lenght of 4, but matches "whateverString" of different length)
	 * 
	 * @param Match $expectedMatch
	 * @param Token $token
	 * @return false|Token
	 */
	
	public function tryConsumeMatchIntoToken(Match $expectedMatch, Token $token) {
		$string = mb_substr($this->input, $this->position);
		$match = array();
		if (preg_match($expectedMatch->getMatchSequence(), $string, $match, PREG_OFFSET_CAPTURE)) {
			if(sizeof($match)>1) {
				$matchingString = $match[count($match)-1][0];
				$consumeLength = mb_strlen($match[0][0]);
			} else {
				$matchingString = $match[0][0];
				$consumeLength = mb_strlen($matchingString);
			}
			$this->lastTokenPosition = $this->position;
			$this->movePosition($consumeLength);
			$token->setValue($matchingString);
			return $token;
		} else {
			return false;
		}
	}

	/**
	 * gets position of last Token in stream
	 *
	 * @return int
	 */
	public function getLastTokenPosition() {
		return $this->lastTokenPosition;
	}
	
	/**
	 * inserts string in $data to input at actual position
	 * 
	 * @param string $input
	 */
	public function injectInput($input) {
		$string = mb_substr($this->input, 0, $this->position) . $input . mb_substr($this->input, $this->position);
		$this->input = $string;
		$this->length = mb_strlen($string);
	}
	
	/**
	 * returns actual carret position representing as row and column
	 * 
	 * @return array
	 */
	public function getPositionAsIndexedCell() {
		$search = substr($this->input, 0, $this->position);
		$rowsToArray = preg_split("/\n|\r\n|\r/", $search, 0, PREG_SPLIT_OFFSET_CAPTURE);
		$sum = 0;
		$last = end($rowsToArray);
		foreach ($rowsToArray as $row) {
			if($row != $last) {
				$sum += (strlen($row[0]) + 1);
			}
		}

		$column = $this->position - $sum;
		$row = count($rowsToArray);
		$return = array(
			"row" => $row, 
			"column" => $column
				);
		return $return;
	}
	/**
	 * injects external input from file
	 *
	 * @param $directory
	 * @param $relativePathToFile
	 * @throws FileManipulationProhibited
	 * @throws FileNotFound
	 */
	public function injectExternalInputFromFile($directory, $relativePathToFile) {
		if(Config::prohibitFileManipulation()) {
			throw new FileManipulationProhibited("Manipulování se soubory je v této konfiguraci zakázáno. Příkaz " . Constant::COMMERCIAL_AT . Constant::KEYWORD_REQUIRE . " není dostupný.");
		}
		$file = $directory . DIRECTORY_SEPARATOR . $relativePathToFile;
		if(!is_readable($file)) {
			throw new FileNotFound("Soubor '$file' nenalezen.");
		}
		$input = file_get_contents($file);
		$this->injectInput($input);
	}
}

class StreamerErrorException extends PreprasorErrorException {
	
}

class PositionDoesNotExists extends StreamerErrorException {

}

class FileNotFound extends StreamerErrorException {

}

class InputEmpty extends StreamerErrorException {

}
class FileManipulationProhibited extends StreamerErrorException {

}

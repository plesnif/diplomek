<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Tokenizer;

use Preprasor\Preprocess\Tokenize\Streamer;
use Preprasor\Preprocess\Tokenize\Token\TokenTypes;
use Preprasor\Preprocess\Tokenize\Token\Base\Token;
use Preprasor\Preprocess\Tokenize\Match\Match;
use Preprasor\Preprocess\Tokenize\Match\MatchTypes;
use Preprasor\Preprocess\Tokenize\Tokenizer\I\Tokenizerable;
use Preprasor\Preprocess\Tokenize\Token\Css\EOF;
use Preprasor\PreprasorErrorException;
use Preprasor\Preprocess\Helper\DirectoryDiver;

/**
 * foundation class for Tokenizer
 *
 * @package Preprasor\Preprocess\Tokenize\Tokenizer
 */
abstract class Tokenizer implements Tokenizerable {
	protected $streamer;
	protected $actualToken;
	protected $savedPosition;
	protected $regExps;
	protected $matchesTypes;
	protected $tokenTypes;
	protected $tokenIndex = 0;
	protected $throwEOFInNextCycle = false;
	protected $tokenStartPositionsInInputStream;
	protected $lastPositionInStreamer;
	protected $tokens = array();
	protected $reconsumeTokenFlag = false;
	protected $workingDirectory;
	protected $requiredFiles = array();

	/**
	 * Tokenizer constructor.
	 *
	 * @param Streamer       $streamer
	 * @param MatchTypes     $matchesTypes
	 * @param TokenTypes     $tokenTypes
	 * @param DirectoryDiver $workingDirectory
	 */
	public function __construct(Streamer $streamer, MatchTypes $matchesTypes, TokenTypes $tokenTypes, DirectoryDiver $workingDirectory) {
		$this->streamer = $streamer;
		$this->workingDirectory = $workingDirectory;
		$this->tokenTypes = $tokenTypes;
		$this->tokenTypes->setContent();		
		$this->matchesTypes = $matchesTypes;
		$this->matchesTypes->setContent();
	}

	/**
	 * consumes a returns next token from input stream
	 */
	protected abstract function consumeToken();

	/**
	 * {@inheritDoc}
	 */
	public function init() {

	}
	
	protected function getCodePoint() {
		return $this->streamer->getCodePoint();
	}

	/**
	 * returns next code point
	 *
	 * @return string
	 */
	protected function getNextCodePoint() {
		return $this->streamer->getNextCodePoint();
	}

	/**
	 * consumes code point
	 *
	 * @return false|string
	 */
	public function consumeCodePoint() {
		return $this->streamer->consumeCodePoint();
	}

	/**
	 * consumes code points
	 *
	 * @param int $numberToConsume
	 * @return false|string
	 */
	protected function consumeCodePoints($numberToConsume = 1) {
		return $this->streamer->consumeCodePoints($numberToConsume);
	}

	/**
	 * looks ahead for code point
	 *
	 * @param int $lookAhead
	 * @return false|string
	 */
	protected function lookAheadCodePoint($lookAhead = 1) {
		return $this->streamer->lookAheadCodePoint($lookAhead);
	}

	/**
	 * reconsumes code point
	 */
	protected function reconsumeCodePoint() {
		$this->streamer->reconsumeCodePoint();
	}

	/**
	 * test whenever $needle a $haystack matches
	 *
	 * @param $needle
	 * @param $haystack
	 * @return bool
	 */
	protected function isMatch($needle, $haystack) {
		$match = new Match($haystack);
		return $this->matches($needle, $match);
	}

	/**
	 * @param string $needle
	 * @param Match $haystack
	 * @return bool
	 */
	protected function matches($needle, Match $haystack) {
		if ($haystack->testAgainst($needle)===true) {
			return true;
		}
		return false;
	}

	/**
	 * adds token to list
	 *
	 * @param Token $token
	 */
	protected function addToken(Token $token) {
		$this->tokens[$this->tokenIndex] = $token;
		$this->tokenStartPositionsInInputStream[$this->tokenIndex] = $this->streamer->getLastTokenPosition();
		$this->tokenIndex++;
	}

	/**
	 * try to match token and consumes if found
	 *
	 * @param Match $match
	 * @param Token $token
	 * @return false|Token
	 */
	protected function tryTransformMatchIntoToken(Match $match, Token $token) {
		$consumed = $this->tryConsumeToken($match, $token);
		if($consumed) {
			$this->actualToken = $consumed;
		}
		return $consumed;
	}

	/**
	 * consumes token
	 *
	 * @param Match $match
	 * @param Token $token
	 * @return false|Token
	 */
	protected function tryConsumeToken(Match $match, Token $token) {
		$return  = $this->streamer->tryConsumeMatchIntoToken($match, $token);
		return $return;
	}

	/**
	 * {@inheritDoc}
	 */
	public function getTokenType() {
		return $this->tokenTypes;
	}

	/**
	 * {@inheritDoc}
	 */
	public function getActualToken() {
		return $this->actualToken;
	}

	/**
	 * {@inheritDoc}
	 */
	public function stepBack($stepsBack = 1) {
		$this->tokenIndex -= $stepsBack;
		if($this->tokenIndex < 0) {
			$this->tokenIndex = 0;
		}
		$this->actualToken = $this->tokens[$this->tokenIndex];
		return $this->actualToken;
	}

	/**
	 * return object containing all token matches
	 *
	 * @return MatchTypes
	 */
	protected function getMatch() {
		return $this->matchesTypes;
	}

	/**
	 * is tokenizer at the end of input?
	 *
	 * @param $char
	 * @return bool
	 */
	protected function isEOF($char) {
		return $char === false;		
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function printTokens() {
		if(!empty($this->tokens)) {
			foreach ($this->tokens as $token) {
				\Tracy\Debugger::dump($token);
			}
		} else {
			do {
				$token = $this->consumeToken();
				\Tracy\Debugger::dump($token);
			} while($this->tokenStreamContinues($token));
		}	
		$this->streamer->reset();
		$this->actualToken = $this->tokens[0];
		$this->tokenIndex =  0;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function tokenize() {
		if(empty($this->tokens)) {
			do {
				$token = $this->consumeToken();
				$this->addToken($token);
			} while($this->tokenStreamContinues($token));
		}
		$this->streamer->reset();
		$this->actualToken = $this->tokens[0];
		$this->tokenIndex =  0;
		return $this->tokens;
	}

	/**
	 * test whenever stream of tokens continues
	 *
	 * @param Token $token
	 * @return bool
	 */
	protected function tokenStreamContinues(Token $token) {
		return !($token instanceof EOF);
	}

	/**
	 * injects external input
	 *
	 * @param $directory
	 * @param $relativePathToFile
	 */
	protected function injectExternalInputFromFile($directory, $relativePathToFile) {
		$this->streamer->injectExternalInputFromFile($directory, $relativePathToFile);
	}

	/**
	 * prepares for injecting external input
	 *
	 * @param Token $token
	 */
	protected function noticeRequireFile(Token $token) {
		$this->requiredFiles[$this->tokenIndex] = $token;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getPreviousToken($considerWhitespace = false) {
		$lookBackIndex = 1;
		do {
			$lookBackIndex++;
			$newTokenIndex = $this->tokenIndex - $lookBackIndex;
			if($newTokenIndex < 0) {
				$newTokenIndex = 0;
			}
			$previousToken = $this->tokens[$newTokenIndex];
		} while($considerWhitespace === false && $previousToken->isType($this->getTokenType()->whitespace));

		return $previousToken;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getPreviousNonWhitespaceToken() {
		$lookBackIndex = 1;
		do {
			$lookBackIndex++;
			$previousToken = $this->tokens[$this->tokenIndex - $lookBackIndex];
		} while($previousToken->isType($this->getTokenType()->whitespace));
		return $previousToken;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getNextNonWhitespaceToken() {
		do {
			$nextToken = $this->nextToken();
		} while($nextToken->isType($this->getTokenType()->whitespace));
		return $nextToken;
	}
	
	/**
	 * {@inheritDoc}
	 */
	protected function stepBackToPreviousNonWhitespaceToken() {
		do {
			$previousToken = $this->stepBack();
		} while($previousToken->isType($this->getTokenType()->whitespace));
		return $previousToken;
	}
	
}

class TokenizerErrorException extends PreprasorErrorException {
	
}
<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Tokenizer\I;
use Preprasor\Preprocess\Tokenize\Token\Base\Token;

/**
 * Interface denoting Tokenizer
 *
 * @package Preprasor\Preprocess\Tokenize\Tokenizer\I
 */
interface Tokenizerable {
	
	/**
	 * returns next token
	 */
	public function nextToken();

	/**
	 * initializes tokenizer
	 */
	public function init();
	
	/**
	 * convert input stream to stream of tokens
	 */
	public function tokenize();
	
	/**
	 * prints tokens
	 */
	public function printTokens();
	
	/**
	 * returns previous token
	 */
	public function getPreviousToken();

	/**
	 * returns specific type of token for comparation
	 */
	public function getTokenType();
	
	/**
	 * sets actual token back by $stepsBack
	 *
	 * @param $stepsBack
	 * @return Token
	 */
	public function stepBack($stepsBack);
	
	/**
	 * returns actual token
	 */
	public function getActualToken();
	
	/**
	 * reconsumes actual token
	 */
	public function reconsumeToken();
	
	/**
	 * returns previous non whitespace token starting at actual position
	 */
	public function  getPreviousNonWhitespaceToken();
	
	/**
	 * returns next non whitespace token starting at actual position
	 */
	public function getNextNonWhitespaceToken();

	/**
	 * reverts look ahead during searching for nested blocks
	 */
	public function revertLookAhead();
}

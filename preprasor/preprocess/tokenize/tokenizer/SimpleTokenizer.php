<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Tokenizer;

use Preprasor\Preprocess\Tokenize\Streamer;
use Preprasor\Preprocess\Color\Model\RGB;
use Preprasor\Preprocess\Color\Model\RGBWrongFormat;
use Preprasor\Preprocess\Tokenize\Token\Base\BlockStarting;
use Preprasor\Preprocess\Tokenize\Token\Base\Token;
use Preprasor\Preprocess\Tokenize\Match\Match;
use Preprasor\Preprocess\Tokenize\Token\Css\Comment;
use Preprasor\Preprocess\Tokenize\Token\Css\Whitespace;
use Preprasor\Preprocess\Tokenize\Token\Css\Ident;
use Preprasor\Preprocess\Tokenize\Token\Css\Number;
use Preprasor\Preprocess\Tokenize\Token\Css\StringToken;
use Preprasor\Preprocess\Tokenize\Token\Css\BadString;
use Preprasor\Preprocess\Tokenize\Token\Css\RightParenthesis;
use Preprasor\Preprocess\Tokenize\Token\Css\LeftParenthesis;
use Preprasor\Preprocess\Tokenize\Token\Css\Hash;
use Preprasor\Preprocess\Tokenize\Token\Css\Comma;
use Preprasor\Preprocess\Tokenize\Token\Css\CDC;
use Preprasor\Preprocess\Tokenize\Token\Css\CDO;
use Preprasor\Preprocess\Tokenize\Token\Css\URL;
use Preprasor\Preprocess\Tokenize\Token\Css\BadURL;
use Preprasor\Preprocess\Tokenize\Token\Css\AtKeyword;
use Preprasor\Preprocess\Tokenize\Token\Css\Percentage;
use Preprasor\Preprocess\Tokenize\Token\Css\Dimension;
use Preprasor\Preprocess\Tokenize\Token\Css\Delim;
use Preprasor\Preprocess\Tokenize\Token\Css\IncludeMatch;
use Preprasor\Preprocess\Tokenize\Token\Css\DashMatch;
use Preprasor\Preprocess\Tokenize\Token\Css\SuffixMatch;
use Preprasor\Preprocess\Tokenize\Token\Css\PrefixMatch;
use Preprasor\Preprocess\Tokenize\Token\Css\SubstringMatch;
use Preprasor\Preprocess\Tokenize\Token\Css\Colon;
use Preprasor\Preprocess\Tokenize\Token\Css\Semicolon;
use Preprasor\Preprocess\Tokenize\Token\Css\RightSquareBracket;
use Preprasor\Preprocess\Tokenize\Token\Css\LeftSquareBracket;
use Preprasor\Preprocess\Tokenize\Token\Css\RightCurlyBracket;
use Preprasor\Preprocess\Tokenize\Token\Css\LeftCurlyBracket;
use Preprasor\Preprocess\Tokenize\Token\Css\FunctionToken;
use Preprasor\Preprocess\Tokenize\Token\Css\Important;
use Preprasor\Preprocess\Tokenize\Token\Css\EOF;
use Preprasor\Preprocess\Tokenize\Token\Preprocessor\Variable;
use Preprasor\Preprocess\Tokenize\Token\Preprocessor\Mixin;
use Preprasor\Preprocess\Tokenize\Token\Preprocessor\ExtendsToken;
use Preprasor\Preprocess\Tokenize\Token\Preprocessor\ExcludeToken;
use Preprasor\Preprocess\Tokenize\Token\Preprocessor\ClassToken;
use Preprasor\Preprocess\Tokenize\Token\Preprocessor\RequireToken;
use Preprasor\Preprocess\Tokenize\Token\TokenTypes;
use Preprasor\Preprocess\Tokenize\Match\MatchTypes;
use Preprasor\Constant;
use Preprasor\Preprocess\Helper\DirectoryDiver;
/**
 * Class for simplified tokenization
 *
 * @package Preprasor\Preprocess\Tokenize\Tokenizer
 */
class SimpleTokenizer extends Tokenizer {
	protected $savedPosition;
	private $containers;
	private $onePointTokens;
	private $twoPointTokens;

	/**
	 * SimpleTokenizer constructor.
	 *
	 * @param Streamer   $streamer
	 * @param MatchTypes $matchesTypes
	 * @param TokenTypes $tokenTypes
	 * @param DirectoryDiver     $directoryDiver
	 */
	public function __construct(Streamer $streamer, MatchTypes $matchesTypes, TokenTypes $tokenTypes, DirectoryDiver $directoryDiver) {
		parent::__construct($streamer, $matchesTypes, $tokenTypes, $directoryDiver);
		$this->onePointTokens = [
			Constant::LEFT_PARENTHESIS => $this->getTokenType()->leftParenthesis,
			Constant::RIGHT_PARENTHESIS => $this->getTokenType()->rightParenthesis,
			Constant::COMMA => $this->getTokenType()->comma,
			Constant::COLON => $this->getTokenType()->colon,
			Constant::SEMICOLON => $this->getTokenType()->semicolon,
			Constant::LEFT_SQUARE_BRACKET => $this->getTokenType()->leftSquareBracket,
			Constant::RIGHT_SQUARE_BRACKET => $this->getTokenType()->rightSquareBracket,
			Constant::LEFT_CURLY_BRACKET => $this->getTokenType()->leftCurlyBracket,
			Constant::RIGHT_CURLY_BRACKET => $this->getTokenType()->rightCurlyBracket,
		];
		// all two point tokens are followed by char '=', i.e. $=
		$this->twoPointTokens = [
			Constant::ASTERISK => $this->getTokenType()->substringMatch,
			Constant::TILDE => $this->getTokenType()->includeMatch,
			Constant::CIRCUMFLEX_ACCENT => $this->getTokenType()->prefixMatch,
		];
		$this->containers = [
			Constant::NUMBER => $this->getTokenType()->hash,
			Constant::PLUS => $this->getTokenType()->number,
			Constant::LESS_THAN => $this->getTokenType()->CDO,
			Constant::COMMERCIAL_AT => $this->getTokenType()->atKeyword,
			Constant::EXCLAMATION_MARK => $this->getTokenType()->important,
		];
	}

	/**
	 * {@inheritDoc}
	 */
	public function nextToken() {
		return $this->consumeToken();
	}

	/**
	 * {@inheritDoc}
	 */
	protected function consumeToken() {		
		$actualCodePoint = $this->getCodePoint();
		
		// was End Of File registered in previous run?
		if($this->throwEOFInNextCycle === true || $this->isEOF($actualCodePoint)) {
			$token = new EOF();
			$this->appendToken($token);
			return $token;
		}

		if ($this->reconsumeTokenFlag) {
			$this->reconsumeTokenFlag = false;
			return $this->actualToken;
		}
		
		// register End Of File for next run?
		if($this->isEOF($this->getNextCodePoint())) {
			$this->throwEOFInNextCycle = true;
		}

		if ($this->tryTransformMatchIntoToken($this->getMatch()->whitespace, new Whitespace)) {
			return $this->actualToken;
		}
		
		if ($this->tryConsumeMatchIntoToken($this->getMatch()->comment, new Comment)) {
			return $this->actualToken;
		}
		if ($this->tryConsumeMatchIntoToken($this->getMatch()->mixin, new Mixin)) {
			return $this->actualToken;
		}
		
		if ($this->tryConsumeMatchIntoToken($this->getMatch()->extends, new ExtendsToken)) {
			return $this->actualToken;
		}
	
		if ($this->tryConsumeMatchIntoToken($this->getMatch()->excludefrom, new ExcludeToken)) {
			return $this->actualToken;
		}
		
		if ($this->tryConsumeMatchIntoToken($this->getMatch()->class, new ClassToken)) {
			return $this->actualToken;
		}
				
		$token = $this->tryConsumeToken($this->getMatch()->require, new RequireToken);
		if($token) {
			$this->injectExternalInputFromFile($this->workingDirectory->getPath(), $token->value());
			return $this->nextToken();
		}
		if ($actualCodePoint == Constant::DOLAR) {
			if ($this->streamer->getNextCodePoint() == Constant::EQUALS) {
				$this->streamer->consumeCodePoint();
				$token = new SuffixMatch();
				$this->appendToken($token);
				return $token;
			}
			if ($this->tryTransformMatchIntoToken($this->getMatch()->variable, new Variable)) {
				return $this->actualToken;
			}
			$this->streamer->consumeCodePoint();
			$token = new Delim($actualCodePoint);
			$this->appendToken($token);
			return $token;
		}


		if ($actualCodePoint == Constant::QUOTATION_MARK || $actualCodePoint == Constant::APOSTROPHE) {
			if ($this->tryConsumeMatchIntoToken($this->getMatch()->string, new StringToken($actualCodePoint))) {
				return $this->actualToken;
			}
			if ($this->tryConsumeMatchIntoToken($this->getMatch()->badstring, new BadString($actualCodePoint))) {
				return $this->actualToken;
			}
		}
		foreach ($this->containers as $codePoint => $token) {
            if ($actualCodePoint == $codePoint) {
                if ($token->isType($this->getTokenType()->atKeyword)) {
					if ($this->tryConsumeMatchIntoToken($this->getMatch()->atkeyword, new AtKeyword)) {
                        return $this->actualToken;
                    }
                }
				if ($token->isType($this->getTokenType()->hash)) {
					if ($this->tryConsumeMatchIntoToken($this->getMatch()->hash, new Hash)) {
						try {
							$color = new RGB($this->actualToken);
							$this->actualToken = $color;
							return $color;
						} catch(RGBWrongFormat $e) {
        					return $this->actualToken;
    					}
					}
				}
                if ($token->isType($this->getTokenType()->number)) {
                    if ($this->consumeNumericToken()) {
                        return $this->actualToken;
                    }
                } else {
					$reflect = new \ReflectionClass($token);
					$matchName = strtolower($reflect->getShortName());
                    if ($this->tryConsumeMatchIntoToken($this->getMatch()->$matchName, new $token)) {
                        return $this->actualToken;
                    }
                }
				$this->streamer->consumeCodePoint();
				$token = new Delim($codePoint);
				$this->appendToken($token);
				return $token;
            }
        }

		foreach ($this->onePointTokens as $codePoint => $token) {			
			if ($actualCodePoint == $codePoint) {
				if($token->startsBlock()) {
					$token = new $token($codePoint, $token->mirrorToken());
				} else {
					$token = new $token($codePoint);
				}
				$this->streamer->consumeCodePoint();
				$this->appendToken($token);				
				return $token;
			}
		}

		foreach ($this->twoPointTokens as $firstCodePoint => $token) {
			if ($actualCodePoint == $firstCodePoint) {
				if ($this->streamer->getNextCodePoint() == Constant::EQUALS) {
					$this->streamer->consumeCodePoint();
					$this->streamer->consumeCodePoint();
					$token = new $token($firstCodePoint . Constant::EQUALS);
					$this->appendToken($token);
					return $token;
				}

				$this->streamer->consumeCodePoint();

				$token = new Delim($firstCodePoint);

				$this->appendToken($token);
				return $token;
			}
		}

		if ($actualCodePoint == Constant::HYPHEN_MINUS) {
			if ($this->consumeNumericToken()) {
				return $this->actualToken;
			}
			if ($this->tryTransformMatchIntoToken($this->getMatch()->cdc, new CDC)) {
				return $this->actualToken;
			}
			if ($this->tryTransformMatchIntoToken($this->getMatch()->ident, new Ident)) {
				return $this->actualToken;
			}
			$this->consumeCodePoint();
			$token = new Delim($actualCodePoint);
			$this->appendToken($token);
			return $token;
		}
		if ($this->consumeNumericToken()) {
			return $this->actualToken;
		}
		if ($this->consumeIdentLikeToken()) {
			return $this->actualToken;
		}
		if ($actualCodePoint == Constant::VERTICAL_LINE) {
			if ($this->streamer->getNextCodePoint() == Constant::EQUALS) {
				$this->streamer->consumeCodePoint();
				$token = new DashMatch;
				$this->appendToken($token);
				return $token;
			}
			$this->streamer->consumeCodePoint();
			$token = new Delim($actualCodePoint);
			$this->appendToken($token);
			return $token;
		}
		$this->streamer->consumeCodePoint();
		$token = new Delim($actualCodePoint);
		$this->appendToken($token);
		return $token;
	}

	/**
	 * consumes token if matches
	 *
	 * @param Match $match
	 * @param Token $token
	 * @return bool|false|Token
	 */
	private function tryConsumeMatchIntoToken(Match $match, Token $token) {
		$consumedToken = $this->streamer->tryConsumeMatchIntoToken($match, $token);
		if ($consumedToken) {
			$this->appendToken($consumedToken);
			return $consumedToken;
		} else {
			return false;
		}
	}

	/**
	 * consumes numeric token if found
	 *
	 * @return bool
	 */
	private function consumeNumericToken() {
		$token = $this->tryTransformMatchIntoToken($this->getMatch()->number, new Number);
		if ($token) {
			if ($this->streamer->getCodePoint() == Constant::PERCENTAGE_SIGN) {
				$this->streamer->consumeCodePoint();
				$percentageToken = new Percentage($token->value());
				$this->appendToken($percentageToken);
				return true;
			}
			$nextToken = $this->tryTransformMatchIntoToken($this->getMatch()->ident, new Ident);

			if ($nextToken) {
				$additionalToken = new Dimension($token->value(), Constant::NUMBER_TYPE_DEFAULT, $nextToken->value());
				$this->appendToken($additionalToken);
			}			
			return true;
		} else {
			return false;
		}
	}

	/**
	 * consumes ident-like token if found
	 *
	 * @return bool
	 */
	private function consumeIdentLikeToken() {
		$token = $this->tryTransformMatchIntoToken($this->getMatch()->url, new URL(Constant::APOSTROPHE));
		if ($token) {
			$this->appendToken($token);
			return true;
		}
		$token = $this->tryTransformMatchIntoToken($this->getMatch()->ident, new Ident);
		if ($token) {
			if ($this->streamer->getCodePoint() == Constant::LEFT_PARENTHESIS) {
				$this->streamer->consumeCodePoint();
				$token = new FunctionToken($token->value());
				$this->appendToken($token);
				return true;
			}
			$this->appendToken($token);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * appends token
	 *
	 * @param Token $token
	 */
	private function appendToken(Token $token) {
		$this->actualToken = $token;
		$this->addToken($token);
	}

	/**
	 * {@inheritDoc}
	 */
	public function stepBack($stepsBack = 1) {
		$stepBackToken = parent::stepBack($stepsBack);
		$this->streamer->setPosition($this->tokenStartPositionsInInputStream[$this->tokenIndex]);
		return $stepBackToken;
	}

	/**
	 * {@inheritDoc}
	 */
	public function reconsumeToken() {
		$this->reconsumeTokenFlag = true;
	}

	/**
	 * {@inheritDoc}
	 */
	public function revertLookAhead() {
		$this->stepBackToPreviousNonWhitespaceToken();
		$this->nextToken();
	}

}

class SimpleTokenizerErrorException extends TokenizerErrorException {
	
}


<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Tokenizer;

use Preprasor\Preprocess\Tokenize\Token\TokenTypes;
use Preprasor\Preprocess\Tokenize\Token\Base\BlockStarting;
use Preprasor\Preprocess\Tokenize\Token\Base\Token;
use Preprasor\Preprocess\Tokenize\Token\Css\Comment;
use Preprasor\Preprocess\Tokenize\Token\Css\Whitespace;
use Preprasor\Preprocess\Tokenize\Token\Css\Ident;
use Preprasor\Preprocess\Tokenize\Token\Css\Number;
use Preprasor\Preprocess\Tokenize\Token\Css\StringToken;
use Preprasor\Preprocess\Tokenize\Token\Css\BadString;
use Preprasor\Preprocess\Tokenize\Token\Css\RightParenthesis;
use Preprasor\Preprocess\Tokenize\Token\Css\LeftParenthesis;
use Preprasor\Preprocess\Tokenize\Token\Css\Hash;
use Preprasor\Preprocess\Tokenize\Token\Css\Comma;
use Preprasor\Preprocess\Tokenize\Token\Css\CDC;
use Preprasor\Preprocess\Tokenize\Token\Css\CDO;
use Preprasor\Preprocess\Tokenize\Token\Css\URL;
use Preprasor\Preprocess\Tokenize\Token\Css\BadURL;
use Preprasor\Preprocess\Tokenize\Token\Css\AtKeyword;
use Preprasor\Preprocess\Tokenize\Token\Css\Percentage;
use Preprasor\Preprocess\Tokenize\Token\Css\Dimension;
use Preprasor\Preprocess\Tokenize\Token\Css\Delim;
use Preprasor\Preprocess\Tokenize\Token\Css\IncludeMatch;
use Preprasor\Preprocess\Tokenize\Token\Css\DashMatch;
use Preprasor\Preprocess\Tokenize\Token\Css\SuffixMatch;
use Preprasor\Preprocess\Tokenize\Token\Css\PrefixMatch;
use Preprasor\Preprocess\Tokenize\Token\Css\SubstringMatch;
use Preprasor\Preprocess\Tokenize\Token\Css\Colon;
use Preprasor\Preprocess\Tokenize\Token\Css\Semicolon;
use Preprasor\Preprocess\Tokenize\Token\Css\RightSquareBracket;
use Preprasor\Preprocess\Tokenize\Token\Css\LeftSquareBracket;
use Preprasor\Preprocess\Tokenize\Token\Css\RightCurlyBracket;
use Preprasor\Preprocess\Tokenize\Token\Css\LeftCurlyBracket;
use Preprasor\Preprocess\Tokenize\Token\Css\FunctionToken;
use Preprasor\Preprocess\Tokenize\Token\Css\Important;
use Preprasor\Preprocess\Tokenize\Token\Css\EOF;
use Preprasor\Preprocess\Tokenize\Token\Preprocessor\Variable;
use Preprasor\Preprocess\Tokenize\Token\Preprocessor\Mixin;
use Preprasor\Preprocess\Tokenize\Token\Preprocessor\ExtendsToken;
use Preprasor\Preprocess\Tokenize\Token\Preprocessor\ExcludeToken;
use Preprasor\Preprocess\Tokenize\Token\Preprocessor\ClassToken;
use Preprasor\Preprocess\Tokenize\Token\Preprocessor\RequireToken;
use Preprasor\Preprocess\Tokenize\Helper\Splitter;
use Preprasor\Preprocess\Tokenize\Helper\ThreeTuple;
use Preprasor\Preprocess\Helper\SpecialCharacters;
use Preprasor\Preprocess\Tokenize\Match\MatchTypes;
use Preprasor\Preprocess\Tokenize\Streamer;
use Preprasor\Constant;
use Preprasor\Preprocess\Helper\DirectoryDiver;

/**
 * Class for standard tokenization
 *
 * @package Preprasor\Preprocess\Tokenize\Tokenizer
 */
class StandardTokenizer extends Tokenizer {
	/**
	 * @var SpecialCharacters
	 */
	private $specialChars;

	/**
	 * StandardTokenizer constructor.
	 *
	 * @param Streamer          $streamer
	 * @param MatchTypes        $matchTypes
	 * @param TokenTypes        $tokenTypes
	 * @param SpecialCharacters $specialChars
	 * @param DirectoryDiver    $directoryDiver
	 */
	public function __construct(Streamer $streamer, MatchTypes $matchTypes, TokenTypes $tokenTypes, SpecialCharacters $specialChars, DirectoryDiver $directoryDiver) {
		parent::__construct($streamer, $matchTypes, $tokenTypes, $directoryDiver);		
		$this->specialChars = $specialChars;
	}

	/**
	 * {@inheritDoc}
	 */
	public function nextToken() {
		$token = $this->tokens[$this->tokenIndex];
		$this->actualToken = $token;
		$this->tokenIndex++;
		return $token;
	}

	/**
	 * {@inheritDoc}
	 */
	public function init() {
		$this->tokenize();
	}

	/**
	 * {@inheritDoc}
	 */
	protected function consumeToken() {
		if ($this->tryTransformMatchIntoToken($this->getMatch()->whitespace, new Whitespace)) {
			return $this->actualToken;
		}
		
		$actualCodePoint = $this->getCodePoint();
		// was End Of File registered in previous run?
		if($this->throwEOFInNextCycle === true || $this->isEOF($actualCodePoint)) {
			$token = new EOF();
			return $token;
		}
		
		if ($this->tryTransformMatchIntoToken($this->getMatch()->whitespace, new Whitespace)) {
			return $this->actualToken;
		}
		
		// register End Of File for next run
		if($this->isEOF($this->getNextCodePoint())) {
			$this->throwEOFInNextCycle = true;
		}
		
		if ($this->tryTransformMatchIntoToken($this->getMatch()->comment, new Comment)) {
			return $this->actualToken;
		}
		
		if ($this->tryTransformMatchIntoToken($this->getMatch()->mixin, new Mixin)) {
			return $this->actualToken;
		}

		if ($this->tryTransformMatchIntoToken($this->getMatch()->extends, new ExtendsToken)) {
			return $this->actualToken;
		}
	
		if ($this->tryTransformMatchIntoToken($this->getMatch()->excludefrom, new ExcludeToken)) {
			return $this->actualToken;
		}
		
		if ($this->tryTransformMatchIntoToken($this->getMatch()->class, new ClassToken)) {
			return $this->actualToken;
		}
		
		
		if ($actualCodePoint ==  Constant::QUOTATION_MARK || $actualCodePoint == Constant::APOSTROPHE) {
			return $this->consumeStringToken($actualCodePoint);
		}
		
		if($actualCodePoint ==  Constant::DOLAR) {
			$nextCodePoint = $this->getNextCodePoint();
			$afterNextCodePoint = $this->lookAheadCodePoint(2);
			$afterAfterNextCodePoint = $this->lookAheadCodePoint(3);
			if ($this->threeCodePointsStartsIdentifier($nextCodePoint, $afterNextCodePoint, $afterAfterNextCodePoint)) {
				$name = $this->consumeName();
				return new Variable($name);
			} else {
				$token = $this->splitByNextChar(Constant::EQUALS, new SuffixMatch, new Delim($actualCodePoint));
				return $token;
			}
		}
		
		if($actualCodePoint == Constant::EXCLAMATION_MARK) {
			$importantToken = $this->tryTransformMatchIntoToken($this->getMatch()->important, new Important);
			if ($importantToken) {
				return $importantToken;
			} else {
				$this->consumeCodePoint();
				return new Delim($actualCodePoint);
			}
		}	
		
		if($actualCodePoint == Constant::NUMBER) {
			$nextCodePoint = $this->getNextCodePoint();
			$afterNextCodePoint = $this->lookAheadCodePoint(2);
			$afterAfterNextCodePoint = $this->lookAheadCodePoint(3);
			$isHash = $this->matches($nextCodePoint, $this->getMatch()->nameCodePoint) || $this->startValidEscape($nextCodePoint, $afterNextCodePoint);
			if($isHash) {
				$hash = new Hash();
				if($this->threeCodePointsStartsIdentifier($nextCodePoint, $afterNextCodePoint, $afterAfterNextCodePoint)) {
					$hash->setFlag(Constant::HASH_FLAG_ID);
				}
				$value = $this->consumeName();
				$hash->setValue($value);
				return $hash;
			} else {
				$this->consumeCodePoint();
				return new Delim(Constant::NUMBER);
			}
		}
		
		if($actualCodePoint == Constant::LEFT_PARENTHESIS) {
			$this->consumeCodePoint();
			return new LeftParenthesis(Constant::LEFT_PARENTHESIS, new RightParenthesis);
		}
		
		if($actualCodePoint == Constant::RIGHT_PARENTHESIS) {
			$this->consumeCodePoint();
			return new RightParenthesis;
		}
		
		if($actualCodePoint == Constant::ASTERISK) {
			$token = $this->splitByNextChar(Constant::EQUALS, new SubstringMatch, new Delim($actualCodePoint));
			return $token;
		}
		
		if($actualCodePoint == Constant::PLUS) {
			$nextCodePoint = $this->getNextCodePoint();
			$afterNextCodePoint = $this->lookAheadCodePoint(2);				
			if($this->threeCodePointsStartsNumber($actualCodePoint, $nextCodePoint, $afterNextCodePoint)) {
				$this->reconsumeCodePoint();
				$number = $this->consumeNumericToken();
				return $number;
			} else {
				$this->consumeCodePoint();
				return new Delim($actualCodePoint);
			}
		}

		if($actualCodePoint == Constant::COMMA) {
			$this->consumeCodePoint();
			return new Comma;
		}
		
		if($actualCodePoint == Constant::HYPHEN_MINUS) {			
			$nextCodePoint = $this->getNextCodePoint();
			$afterNextCodePoint = $this->lookAheadCodePoint(2);
			if($this->threeCodePointsStartsNumber($actualCodePoint, $nextCodePoint, $afterNextCodePoint)) {
				$this->reconsumeCodePoint();
				return $this->consumeNumericToken();
			} elseif ($nextCodePoint== Constant::HYPHEN_MINUS && $afterNextCodePoint == Constant::GREATER_THAN) {
				$this->consumeCodePoints(3);
				return new CDC;
			} elseif ($this->threeCodePointsStartsIdentifier($actualCodePoint, $nextCodePoint, $afterNextCodePoint)) {
				$this->reconsumeCodePoint();
				return $this->consumeIndentLikeToken();
			} else {
				$this->consumeCodePoint();
				return new Delim($actualCodePoint);
			}
		}
		
		if($actualCodePoint == Constant::FULL_STOP) {
			$nextCodePoint = $this->getNextCodePoint();
			$afterNextCodePoint = $this->lookAheadCodePoint(2);
			if ($this->threeCodePointsStartsNumber($actualCodePoint, $nextCodePoint, $afterNextCodePoint)) {
				$this->reconsumeCodePoint();
				return $this->consumeNumericToken();
			} else {
				$this->consumeCodePoint();
				return new Delim($actualCodePoint);
			}
		}
		

		if($actualCodePoint == Constant::COLON) {
			$this->consumeCodePoint();
			return new Colon;
		}
		
		if($actualCodePoint == Constant::SEMICOLON) {
			$this->consumeCodePoint();
			return new Semicolon;
		}
		
		if($actualCodePoint == Constant::LESS_THAN) {
			$nextCodePoint = $this->getNextCodePoint();
			$afterNextCodePoint = $this->lookAheadCodePoint(2);
			$afterAfterNextCodePoint = $this->lookAheadCodePoint(3);
			$isCDOToken = ($nextCodePoint == Constant::EXCLAMATION_MARK) && ($afterNextCodePoint == Constant::HYPHEN_MINUS) && ($afterAfterNextCodePoint == Constant::HYPHEN_MINUS);
			if($isCDOToken) {
				$this->consumeCodePoints(strlen($this->getTokenType()->CDO->value()));
				return new CDO;
			} else {
				$this->consumeCodePoint();
				return new Delim($actualCodePoint);
			}
		}
		
		if($actualCodePoint == Constant::COMMERCIAL_AT) {
			$nextCodePoint = $this->getNextCodePoint();
			$afterNextCodePoint = $this->lookAheadCodePoint(2);
			$afterAfterNextCodePoint = $this->lookAheadCodePoint(3);

			$token = $this->tryConsumeToken($this->getMatch()->require, new RequireToken);
			if($token) {
				$this->injectExternalInputFromFile($this->workingDirectory->getPath(), $token->value());
				return $this->consumeToken();
			} elseif ($this->threeCodePointsStartsIdentifier($nextCodePoint, $afterNextCodePoint, $afterAfterNextCodePoint)) {
				$name = $this->consumeName();
				return new AtKeyword($name);
			} else {
				$this->consumeCodePoint();
				return new Delim($actualCodePoint);
			}
		}
		
		if($actualCodePoint == Constant::LEFT_SQUARE_BRACKET) {
			$this->consumeCodePoint();
			return new LeftSquareBracket(Constant::LEFT_SQUARE_BRACKET, new RightSquareBracket);
		}
		
		if($actualCodePoint == Constant::REVERSE_SOLIDUS) {
			$nextCodePoint = $this->getNextCodePoint();
			if($this->startValidEscape($actualCodePoint, $nextCodePoint)) {
				// difference from documentation draft, it should be reconsumed, but getNextCodePoint dont consume code point therefore no need to reconsume
				return $this->consumeIndentLikeToken();
			} else {
				//parse error
				$this->consumeCodePoint();
				return new Delim($actualCodePoint);
			}
		}
		
		if($actualCodePoint == Constant::RIGHT_SQUARE_BRACKET) {
			$this->consumeCodePoint();
			return new RightSquareBracket;
		}
		
		if($actualCodePoint == Constant::CIRCUMFLEX_ACCENT) {
			$token = $this->splitByNextChar(Constant::EQUALS, new PrefixMatch, new Delim($actualCodePoint));
			return $token;
		}
		
		if($actualCodePoint == Constant::LEFT_CURLY_BRACKET) {
			$this->consumeCodePoint();
			return new LeftCurlyBracket(Constant::LEFT_CURLY_BRACKET, new RightCurlyBracket);
		}
		
		if($actualCodePoint == Constant::RIGHT_CURLY_BRACKET) {
			$this->consumeCodePoint();
			return new RightCurlyBracket;
		}
		
		if($this->matches($actualCodePoint, $this->getMatch()->digit)) {
			$this->reconsumeCodePoint();
			$number = $this->consumeNumericToken();
			return $number;
		}
		
		if($this->matches($actualCodePoint, $this->getMatch()->nameStartCodePoint)) {
			$this->reconsumeCodePoint();
			$identLike = $this->consumeIndentLikeToken();
			return $identLike;
		}
		
		if($actualCodePoint == Constant::VERTICAL_LINE) {
			$nextCodePoint = $this->getNextCodePoint();
			if($nextCodePoint == Constant::EQUALS) {
				$this->consumeCodePoints(2);
				return new DashMatch;
			} else {
				$this->consumeCodePoint();
				return new Delim($actualCodePoint);
			}
		}
		
		if($actualCodePoint == Constant::TILDE) {
			$token = $this->splitByNextChar(Constant::EQUALS, new IncludeMatch, new Delim($actualCodePoint));
			return $token;
		}
		$this->consumeCodePoint();
		return new Delim($actualCodePoint);
	}

	/**
	 * returns token according to match with $wishedChar
	 *
	 * @param string $wishedChar
	 * @param Token $firstOption
	 * @param Token $secondOption
	 * @return Token
	 */
	private function splitByNextChar($wishedChar, Token $firstOption, Token $secondOption) {
		$nextCodePoint = $this->getNextCodePoint();
		$splitter = new Splitter($nextCodePoint, $wishedChar, $firstOption, $secondOption, $this);
		return $splitter->getOption();
	}

	/**
	 * consumes numeric token
	 *
	 * @return Dimension|Percentage|Number
	 */
	private function consumeNumericToken() {
		$threeTuple = $this->consumeNumber();
		$nextCodePoint = $this->getNextCodePoint();
		$afterNextCodePoint = $this->lookAheadCodePoint(2);
		$afterAfterNextCodePoint = $this->lookAheadCodePoint(3);
		if($this->threeCodePointsStartsIdentifier($nextCodePoint, $afterNextCodePoint, $afterAfterNextCodePoint)) {
			$name = $this->consumeName();
			$dimentsionToken = new Dimension($threeTuple->getValue(), $threeTuple->getType(), $name);
			return $dimentsionToken;
		} elseif($nextCodePoint == Constant::PERCENTAGE_SIGN) {
			/* difference from documentation draft, actualy two code point must be consumed */
			$this->consumeCodePoints(2);
			return new Percentage($threeTuple->getValue());
		} else {
			$this->consumeCodePoint();
			return new Number($threeTuple->getValue(), $threeTuple->getType());
		}
	}

	/**
	 * consumes ident-like token
	 *
	 * @return BadURL|FunctionToken|Ident|URL
	 */
	private function consumeIndentLikeToken() {
		$name = $this->consumeName();
		$nextCodePoint = $this->getCodePoint();
		if($this->matches($name, $this->getMatch()->url) && $nextCodePoint == Constant::LEFT_PARENTHESIS) {
			$this->consumeCodePoint();
			$this->tryConsumeToken($this->getMatch()->whitespace, new Whitespace());
			return $this->cunsumeUrlToken();
		} elseif ($this->getCodePoint() == Constant::LEFT_PARENTHESIS) {
			$this->consumeCodePoint();
			return new FunctionToken($name);
		} else {
			return new Ident($name);
		}
	}

	/**
	 * consumes string
	 *
	 * @param $endingCodePoint
	 * @return BadString|StringToken
	 */
	private function consumeStringToken($endingCodePoint) {
		$stringValue = "";
		while(true) {
			$currentCodePoint = $this->consumeCodePoint();
			if($currentCodePoint == $endingCodePoint) {
				$this->consumeCodePoint();
				return new StringToken($endingCodePoint, $stringValue);
			} elseif ($this->isEOF($currentCodePoint)) {
				//if EOF - parse error
				return  new StringToken($endingCodePoint, $stringValue);
			} elseif($this->isNewLine($currentCodePoint)) {
				//parse error
				return new BadString($endingCodePoint, $stringValue);
			} elseif ($currentCodePoint == Constant::REVERSE_SOLIDUS) {
				$nextCodePoint = $this->getNextCodePoint();
				if($this->isEOF($nextCodePoint)) {
					//do nothing
				} elseif ($this->isNewLine($nextCodePoint)) {
					$this->consumeCodePoint();
				} else {
					if($this->startValidEscape($currentCodePoint, $nextCodePoint)) {
						$stringValue .= $this->consumeEscapedCodePoint();
					}
				}
			} else {
				$stringValue .= $currentCodePoint;
			}
		}
	}

	/**
	 * consumes url token
	 *
	 * @return BadURL|URL
	 */
	private function cunsumeUrlToken() {
		$value = "";
		$quote = "'";
		$this->tryConsumeToken($this->getMatch()->whitespace, new Whitespace());
		$nextCodePoint = $this->getNextCodePoint();
		if($this->isEOF($nextCodePoint)) {
			//parse error
			return new URL($quote);
		}
		$codePoint = $this->getCodePoint();
		$expectedEndingCodePoint = false;
		if($codePoint == Constant::QUOTATION_MARK || $codePoint == Constant::APOSTROPHE) {
			$expectedEndingCodePoint = $codePoint;
			$quote = $codePoint;
			$this->consumeCodePoint();
			$codePoint = $this->getCodePoint();
			$nextCodePoint = $this->getNextCodePoint();
		}

		while(true) {
			if($expectedEndingCodePoint) {
				$endingCondition = $codePoint == $expectedEndingCodePoint && $nextCodePoint == Constant::RIGHT_PARENTHESIS;
			} else {
				$endingCondition = $codePoint == Constant::RIGHT_PARENTHESIS;
			}

			if($endingCondition) {
				$this->consumeCodePoint();
				if ($expectedEndingCodePoint) {
					$this->consumeCodePoint();
				}				
				return new URL($quote, $value);	
			} elseif ($this->isEOF($nextCodePoint)) {
				//parse error
				return new URL($quote, $value);
			} elseif ($this->isNewLine($nextCodePoint)) {
				return new BadURL($quote, $value);
			} else {		
				$value .= $codePoint;
				$this->consumeCodePoint();
				$codePoint = $this->getCodePoint();
				$nextCodePoint = $this->getNextCodePoint();
			}
		}
	}

	/**
	 * consumes escaped code point
	 *
	 * @return string
	 */
	private function consumeEscapedCodePoint() {
		$codepoint = $this->consumeCodePoint();
		if($this->matches($codepoint, $this->getMatch()->hexDigit)) {
			$hexString = $codepoint;
			$hexCount = 1;
			while($this->matches($this->getNextCodePoint(), $this->getMatch()->hexDigit) && $hexCount<6) {
				$hexString .= $this->consumeCodePoint();
				$hexCount++;
			}
			if($this->matches($this->getNextCodePoint(), $this->getMatch()->whitespace)) {
				$this->consumeCodePoint();
			}
			$hexNumber = hexdec($hexString);
			$replaceEscaped = ($hexNumber == 0) || 
					($hexNumber >= $this->getSpecialChar()->surrogateCodePoints[0] && $hexNumber <= $this->getSpecialChar()->surrogateCodePoints[0]) || 
					($hexNumber >= $this->getSpecialChar()->maximumAllowedCodePoint);
			if($replaceEscaped) {
				return $this->getSpecialChar()->replacementCharacter;
			}
		} elseif ($this->isEOF($codepoint)) {
			//parse error
			return $this->getSpecialChar()->replacementCharacter;
		}
		return Constant::REVERSE_SOLIDUS . $hexString;
	}

	/**
	 * test whenever sequence starts valid escape string
	 * @param string $codePoint
	 * @param string $nextCodePoint
	 * @return bool
	 */
	private function startValidEscape($codePoint, $nextCodePoint) {
		if($codePoint != Constant::REVERSE_SOLIDUS) {
			return false;
		} elseif($this->isNewLine($nextCodePoint)) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * test whenever three code points starts identifier
	 *
	 * @param string $firstCodePoint
	 * @param string $secondCodePoint
	 * @param string $thirdCodePoint
	 * @return bool
	 */
	private function threeCodePointsStartsIdentifier($firstCodePoint, $secondCodePoint, $thirdCodePoint) {
		if($firstCodePoint == Constant::HYPHEN_MINUS) {
			$isValid = $this->matches($secondCodePoint, $this->getMatch()->nameStartCodePoint) || $secondCodePoint == Constant::HYPHEN_MINUS || $this->startValidEscape($secondCodePoint, $thirdCodePoint);
			return $isValid;
		} elseif ($this->matches($firstCodePoint, $this->getMatch()->nameStartCodePoint)) {
			return true;
		} elseif ($firstCodePoint == Constant::REVERSE_SOLIDUS) {
			return $this->startValidEscape($firstCodePoint, $secondCodePoint);
		} else {
			return false;
		}
	}

	/**
	 * test whenever three code points starts number
	 *
	 * @param $firstCodePoint
	 * @param $secondCodePoint
	 * @param $thirdCodePoint
	 * @return bool
	 */
	private function threeCodePointsStartsNumber($firstCodePoint, $secondCodePoint, $thirdCodePoint) {
		if($firstCodePoint == Constant::PLUS || $firstCodePoint == Constant::HYPHEN_MINUS) {
			if($this->matches($secondCodePoint, $this->getMatch()->digit)) {
				return true;
			} elseif ($secondCodePoint == Constant::FULL_STOP && $this->matches($thirdCodePoint, $this->getMatch()->digit)) {
				return true;
			} else {
				return false;
			}			
		} elseif ($firstCodePoint == Constant::FULL_STOP) {
			if($this->matches($secondCodePoint, $this->getMatch()->digit)) {
				return true;
			}
			return false;
		// @codeCoverageIgnoreStart - unreachable state by standart tokenization, but is valid part of Check if three code points would start a number algorithm
		} elseif($this->matches($firstCodePoint, $this->getMatch()->digit)) {
			return true;
		} else {
			return false;	
		}
		// @codeCoverageIgnoreEnd
	}

	/**
	 * consumes a returns name
	 *
	 * @return string
	 */
	private function consumeName() {
		$result = "";
		do {
			$codePoint = $this->getCodePoint();
			$nextCodePoint = $this->getNextCodePoint();
			if($this->matches($nextCodePoint, $this->getMatch()->nameCodePoint)) {
				//Consume name alogithm from documentation doesn't always work on beggining of input stream, thus this condition
				if($this->streamer->position == 0 && $this->matches($codePoint, $this->getMatch()->nameCodePoint)) {
					$result .= $codePoint;
				}
				$result .= $nextCodePoint;
			} elseif($this->startValidEscape($codePoint, $nextCodePoint)) {
				$result .= $this->consumeEscapedCodePoint();
				//added extra in contrary of standart definition, cousing problems in escaped names separated by \
				return $result;
			} else {
				//there should be reconsume code point algorithm according to documentation, but consume code point must take place and then move caret to next position - therefore "twice" consume code point algorithm
				$this->consumeCodePoint();
				
				return $result;
			}
			$this->consumeCodePoint();
		} while(true);
	}

	/**
	 * consumes a returns number in form of ThreeTuple
	 *
	 * @return ThreeTuple
	 */
	private function consumeNumber() {		
		$repr = "";
		$type = Constant::NUMBER_TYPE_INTEGER;	
		$nextCodePoint = $this->getNextCodePoint();
		if($nextCodePoint == Constant::PLUS || $nextCodePoint == Constant::HYPHEN_MINUS) {
			$repr .= $nextCodePoint;
			$this->consumeCodePoint();
		}
		$nextCodePoint = $this->getNextCodePoint();
		while ($this->matches($nextCodePoint, $this->getMatch()->digit)) {
			$this->consumeCodePoint();
			$nextCodePoint = $this->getNextCodePoint();
			$repr .= $this->getCodePoint();
		}
		$nextCodePoint = $this->getNextCodePoint();
		$afterNextCodePoint = $this->lookAheadCodePoint(2);
		if($this->isMatch($nextCodePoint, Constant::FULL_STOP_REGEXP) && $this->matches($afterNextCodePoint, $this->getMatch()->digit)) {
			$repr .= $this->consumeCodePoint();
			$repr .= $this->consumeCodePoint();	
			$type = Constant::NUMBER_TYPE_NUMBER;			
			$nextCodePoint = $this->getNextCodePoint();
			while ($this->matches($nextCodePoint, $this->getMatch()->digit)) {
				$this->consumeCodePoint();
				$nextCodePoint = $this->getNextCodePoint();
				$repr .= $this->getCodePoint();
			}
		}
		$nextCodePoint = $this->getNextCodePoint();
		$afterNextCodePoint = $this->lookAheadCodePoint(2);
		$afterAfterNextCodePoint = $this->lookAheadCodePoint(3);
		$optionalPlusOrMinusSign = $afterNextCodePoint == Constant::PLUS || $afterNextCodePoint == Constant::HYPHEN_MINUS;
		$takeInAccountOptionalPlusMinusSign = $optionalPlusOrMinusSign ? $afterAfterNextCodePoint : $afterNextCodePoint;	
		if($this->matches($nextCodePoint, $this->getMatch()->e) && $this->matches($takeInAccountOptionalPlusMinusSign, $this->getMatch()->digit)) {	
			$this->consumeCodePoint();
			$type = Constant::NUMBER_TYPE_NUMBER;
			if($optionalPlusOrMinusSign) {
				$repr .= $this->consumeCodePoints(2);
			} else {
				$repr .= $this->consumeCodePoints(1);
			}
			$nextCodePoint = $this->getCodePoint();
			while ($this->matches($nextCodePoint, $this->getMatch()->digit)) {	
				$repr .= $nextCodePoint;
				$this->consumeCodePoint();
				$nextCodePoint = $this->getCodePoint();
			}	
		}
		return new ThreeTuple($repr, $repr, $type);		
	}

	/**
	 * test whenever $char is new line
	 *
	 * @param string $char
	 * @return bool
	 */
	private function isNewLine($char) {
		return $this->matches($char, $this->getMatch()->newLine);
	}

	/**
	 * returns special characters
	 *
	 * @return SpecialCharacters
	 */
	private function getSpecialChar() {
		return $this->specialChars;
	}

	/**
	 * {@inheritDoc}
	 */
	public function reconsumeToken() {
		$this->stepBack(1);
	}

	/**
	 * {@inheritDoc}
	 */
	public function revertLookAhead() {
		return $this->stepBackToPreviousNonWhitespaceToken();
	}
	
}

class StandartTokenizerErrorException extends TokenizerErrorException {
	
}



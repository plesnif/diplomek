<?php

/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\RegExp;

use Preprasor\Preprocess\Tokenize\I\Settable;

/**
 * Class generating regular expressions and responding Matches for tokenization phase
 *
 * @package Preprasor\Preprocess\Tokenize\RegExp
 */
abstract class RegExps implements Settable {
	//terms for tokenization phaze
	/**
	 * @var string
	 */
	public $newLine;
	
	/**
	 * @var string
	 */
	public $newLines;
	/**
	 * @var string
	 */
	public $whitespace;
	
	/**
	 * @var string
	 */
	public $digit;
	
	/**
	 * @var string
	 */
	public $e;
	
	/**
	 * @var string
	 */
	public $hexDigit;
	
	/**
	 * @var string
	 */
	public $nameStartCodePoint;
	
	/**
	 * @var string
	 */
	public $nameCodePoint;	
	
	/**
	 * @var string
	 */
	//regular expressions
    public $commentToken;
	
	/**
	 * @var string
	 */
    public $whitespaceToken;
	
	/**
	 * @var string
	 */
    public $identToken;
	
	/**
	 * @var string
	 */
    public $badstringToken;
	
	/**
	 * @var string
	 */
    public $stringToken;
	
	/**
	 * @var string
	 */
    public $urlToken;
	
	/**
	 * @var string
	 */
    public $numberToken;
	
	/**
	 * @var string
	 */
    public $atkeywordToken;
	
	/**
	 * @var string
	 */
    public $hashToken;
	
	/**
	 * @var string
	 */
    public $importantToken;
	
	/**
	 * @var string
	 */
	public $cdo;
	
	/**
	 * @var string
	 */
	public $cdc;
	
	/**
	 * @var string
	 */
    public $variableToken;
	
	/**
	 * @var string
	 */
	public $mixinToken;
	
	/**
	 * @var string
	 */
    public $extendsToken;
	
	/**
	 * @var string
	 */
    public $excludefromToken;
	
	/**
	 * @var string
	 */
    public $classToken;
	
	/**
	 * @var string
	 */
	public $requireToken;
}
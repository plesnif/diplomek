<?php

/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\RegExp;

use Preprasor\Constant;

/**
 * Class generating regular expressions for tokenization phase - simplified version
 *
 * @package Preprasor\Preprocess\Tokenize\RegExp
 */
class SimpleRegExps extends RegExps {
	/**
	 * {@inheritDoc}
	 */
	public function setContent() {
		//standart css tokens
		$this->commentToken = "/(?:\/\*(.*?)\*\/)/s";
		$this->newLines = ["\n", "\r\n", "\r", "\f"];
		$this->newLine = implode("|", $this->newLines);
		$this->whitespace = " |\t|" . $this->newLine;
		$this->digit = "[0-9]";
		$this->e = "[eE]";
		$this->hexDigit = "[0-9a-fA-F]";
		$this->whitespaceToken = "(?:(?:$this->whitespace)+)";
		$letter = "[a-zA-Z]";
		$this->nameStartCodePoint = "$letter|_";
		$this->nameCodePoint = "$this->nameStartCodePoint|$this->digit|-";
		$this->identToken = "(?:-?[a-zA-Z_]+[0-9a-zA-Z_-]*)" ;
		$badString1 = "\"((?:(?:(?:(?!(:?(:?\")|(?:\\\)|(?:$this->newLine))).)+)|((\\$this->newLine)*))?)";
		$badString2 = "\'((?:(?:(?:(?!(:?(:?\")|(?:\\\)|(?:$this->newLine))).)+)|((\\$this->newLine)*))?)";
		$this->badstringToken = "$badString1|$badString2";
		$string1 = "$badString1\"";
		$string2 = "$badString2\'";
		$this->cdo = "<!--";
		$this->cdc = "-->";
		$this->stringToken = "$string1|$string2";
		$this->urlToken = "/(?:(?:url\((?:(?:\'(.+)\')|\"(.+)\"|(.+))\)))/iU";
		$this->numberToken = "(?:[+-]?(?:[0-9]*\.)?[0-9]+)";
		$this->atkeywordToken = "\@(" . $this->identToken . ")";
		$this->hashToken =  "(?:#((?:[a-zA-Z0-9_-])*))";
		$this->importantToken = "/(\!" . Constant::KEYWORD_IMPORTANT . ")/i";
		//end of preproccessor defined css tokens
		$this->variableToken = "\\$(" . $this->identToken . ")";
		$this->mixinToken = "(?:" . Constant::KEYWORD_MIXIN . " )(" . $this->identToken . ")\(";
		$this->extendsToken = Constant::KEYWORD_EXTENDS;
		$this->excludefromToken = Constant::KEYWORD_EXCLUDE_FROM;
		$this->classToken = "\.(" . $this->identToken . ")";
		$this->requireToken = "/(?:(?:\@" . Constant::KEYWORD_REQUIRE . " (?:(?:\'(.+)\')|\"(.+)\"|(.+))));/iU";
		
	}
}
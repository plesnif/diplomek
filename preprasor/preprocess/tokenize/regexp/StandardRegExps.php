<?php

/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\RegExp;

use Preprasor\Constant;

/**
 * Class generating regular expressions for tokenization phase - standart version
 *
 * @package Preprasor\Preprocess\Tokenize\RegExp
 */
class StandardRegExps extends RegExps {
	/**
	 * {@inheritDoc}
	 */
	public function setContent() {
		//standart css tokens
		$this->commentToken = "/(?:\/\*(.*?)\*\/)/s";
		$this->newLines = ["\n", "\r\n", "\r", "\f"];
		$this->newLine = implode("|", ($this->newLines));
		$this->whitespace = " |\t|" . $this->newLine;
		$this->digit = "[0-9]";
		$this->e = "[eE]";
		$this->hexDigit = "[0-9a-fA-F]";
		$escape = "(?:\\\(?:(?!(?:(?:$this->newLine)|(?:$this->hexDigit)))|(?:$this->hexDigit{1,6}(?:$this->whitespace)?)))";
		$this->whitespaceToken = "(?:(?:$this->whitespace)+)";
		$ws = "(?:(?:" . $this->whitespaceToken . ")*)";
		$nonASCII = "[^\\0-\\200]"; //octanove zapsane U+0080(je ve formatu HEX)
		$letter = "[a-zA-Z]";
		$this->nameStartCodePoint = "$letter|_";
		$this->nameCodePoint = "$this->nameStartCodePoint|$this->digit|-";
		$this->identToken = "(?:(?:--|(?:-?(?:(?:[a-zA-Z_])*|$nonASCII)|(?:$escape)))(?:(?:(?:[a-zA-Z0-9_-])*|$nonASCII)|(?:(?:$escape)*))?)";
		$badString1 = "\"((?:(?:(?:(?!(?:(?:\")|(?:\\\)|(?:$this->newLine))).)+)|(($escape)*)|((\\$this->newLine)*))?)";
		$badString2 = "\'((?:(?:(?:(?!(?:(?:\")|(?:\\\)|(?:$this->newLine))).)+)|(($escape)*)|((\\$this->newLine)*))?)";
		$this->badstringToken = "$badString1|$badString2";
		$string1 = "$badString1\"";
		$string2 = "$badString2\'";
		$this->cdo = "<!--";
		$this->cdc = "-->";
		$this->stringToken = "$string1|$string2";
		$this->urlToken = "/(?:" . Constant::KEYWORD_URL . ")/i";
		$this->numberToken = "(?:[+-]?[0-9]*\.?[0-9]+(?:[eE][+-]?[0-9]+)?)";
		$this->atkeywordToken = "\@(" . $this->identToken . ")";
		$this->hashToken =  "(?:#((?:(?:$escape)|(?:[a-zA-Z0-9_-]))*))";
		$this->importantToken = "/(\!" . Constant::KEYWORD_IMPORTANT . ")/i";
		//end of preproccessor defined css tokens
		$this->variableToken = "\\$(" . $this->identToken . ")";
		$this->mixinToken = "(?:" . Constant::KEYWORD_MIXIN . " )(" . $this->identToken . ")\(";//"(?:mixin$ws)((?:$ws\($ws\\$" . $this->identToken . "$ws\)$ws))\(";
		$this->extendsToken = Constant::KEYWORD_EXTENDS;
		$this->excludefromToken = Constant::KEYWORD_EXCLUDE_FROM;
		$this->classToken = "\.(" . $this->identToken . ")";
		$this->requireToken = "/(?:(?:\@" . Constant::KEYWORD_REQUIRE . " (?:(?:\'(.+)\')|\"(.+)\"|(.+))));/iU";
	}
}
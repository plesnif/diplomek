<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */
namespace Preprasor\Preprocess\Tokenize\I;

/**
 * Interface denoting class able to preprocess itself
 *
 * @package Preprasor\Preprocess\Tokenize\I
 */
interface Settable {
	
	/**
	 * Sets content
	 */
    public function setContent();
}
<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Match;

use Preprasor\Constant;

/**
 * This class represents Match for given regular expresion and defines basic task over matching sequence i.e. match testing for input stream
 *
 * @package Preprasor\Preprocess\Tokenize\Match
 */
class Match {
	/**
	 * @var string 
	 */
	private $regExpDelimiter = Constant::SLASH;

	/**
	 * Match constructor.
	 *
	 * @param      $matchSequence
	 * @param bool $omitDelimiters
	 */
	public function __construct($matchSequence, $omitDelimiters = false) {
		if(!$omitDelimiters) {
			$matchSequence = $this->addDelimiters($matchSequence);
		}

		$matchSequence = $this->addAnchor($matchSequence);
		$this->matchSequence = $matchSequence;
	}
	
	/**
	 * return matching sequence
	 * 
	 * @return string
	 */
	public function getMatchSequence() {
		return $this->matchSequence;
	}

	/**
	 * wraps $string in delimiter to be use as regular expression 
	 * 
	 * @param string $string
	 * @return string
	 */
	private function addDelimiters($string) {
		return $this->regExpDelimiter . $string . $this->regExpDelimiter;
	}

	/**
	 * adds circumflex inbetween delimiters or if not set at the beginning
	 * 
	 * @param string $regExp
	 * @return string
	 */
	private function addAnchor($regExp) {
		$endOfRegExp = strrpos($regExp, $this->regExpDelimiter);
		$delimetersAreSet = $endOfRegExp !== false;
		if($delimetersAreSet) {
			$newRegExp = substr_replace($regExp, ")", $endOfRegExp, 0);
			$newRegExp = substr_replace($newRegExp, "^(?:", 1, 0); 
		} else {
			$newRegExp = "^(?:" . $regExp . ")";
		}
		return $newRegExp;
	}
	
	/**
	 * tests its matching sequence against $string
	 * 
	 * @param string $string
	 * @return boolean
	 */
	public function testAgainst($string) {
		if (preg_match($this->matchSequence, $string, $match)) {
			return true;
		}
		return false;
	}
}

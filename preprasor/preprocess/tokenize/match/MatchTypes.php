<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Tokenize\Match;

use Preprasor\Preprocess\Tokenize\I\Settable;
use Preprasor\Preprocess\Tokenize\RegExp\RegExps;

/**
 * Class generating regular expressions and responding Matches for tokenization phase
 *
 * @package Preprasor\Preprocess\Tokenize\Match
 */
class MatchTypes implements Settable {
	/**
	 * @var RegExps
	 */
	private $regExps;
	
	/**
	 * @var Match 
	 */
	public $newLine;
	
	/**
	 * @var Match 
	 */
	public $whitespace;
	
	/**
	 * @var Match 
	 */
	public $digit;
	
	/**
	 * @var Match 
	 */
	public $e;
	
	/**
	 * @var Match 
	 */
	public $hexDigit;
	
	/**
	 * @var Match 
	 */
	public $nameStartCodePoint;
	
	/**
	 * @var Match 
	 */
	public $nameCodePoint;
	
	/**
	 * @var Match 
	 */
	public $comment;
	
	/**
	 * @var Match 
	 */
	public $ident;
	
	/**
	 * @var Match 
	 */
	public $badstring;
	
	/**
	 * @var Match 
	 */
	public $string;
	
	/**
	 * @var Match 
	 */
	public $url;
	
	/**
	 * @var Match 
	 */
	public $number;
	
	/**
	 * @var Match 
	 */
	public $atkeyword;
	
	/**
	 * @var Match 
	 */
	public $hash;
	
	/**
	 * @var Match 
	 */
	public $important;
	
	/**
	 * @var Match 
	 */
	public $cdo;
	
	/**
	 * @var Match 
	 */
	public $cdc;
	
	/**
	 * @var Match 
	 */
	public $variable;
	
	/**
	 * @var Match 
	 */
	public $mixin;
	
	/**
	 * @var Match 
	 */
	public $extends;
	
	/**
	 * @var Match 
	 */
	public $excludefrom;
	
	/**
	 * @var Match 
	 */
	public $class;
	
	/**
	 * @var Match 
	 */
	public $require;

	/**
	 * MatchTypes constructor.
	 *
	 * @param RegExps $regExps
	 */
	public function __construct(RegExps $regExps) {
		$this->regExps = $regExps;
	}

	/**
	 * defines classes from regular expressions for match testing during tokenization
	 */
	public function setContent() {
		$this->regExps->setContent();

		$this->newLine = new Match($this->regExps->newLine);
		$this->digit = new Match($this->regExps->digit);
		$this->e = new Match($this->regExps->e);
		$this->hexDigit = new Match($this->regExps->hexDigit);
		$this->nameStartCodePoint = new Match($this->regExps->nameStartCodePoint);
		$this->nameCodePoint = new Match($this->regExps->nameCodePoint);
		$this->comment = new Match($this->regExps->commentToken, true);
		$this->whitespace = new Match($this->regExps->whitespaceToken);
		$this->ident = new Match($this->regExps->identToken);
		$this->badstring = new Match($this->regExps->badstringToken);
		$this->string = new Match($this->regExps->stringToken);
		$this->url = new Match($this->regExps->urlToken, true);
		$this->number = new Match($this->regExps->numberToken);
		$this->atkeyword =  new Match($this->regExps->atkeywordToken);
		$this->hash = new Match($this->regExps->hashToken);
		$this->important = new Match($this->regExps->importantToken, true);
		$this->cdo = new Match($this->regExps->cdo);
		$this->cdc = new Match($this->regExps->cdc);
		$this->variable = new Match($this->regExps->variableToken);
		$this->mixin = new Match($this->regExps->mixinToken);
		$this->extends = new Match($this->regExps->extendsToken);
		$this->excludefrom = new Match($this->regExps->excludefromToken);
		$this->class = new Match($this->regExps->classToken);
		$this->require = new Match($this->regExps->requireToken, true);
	}
	
	/**
	 * return object containing regular expressions
	 * 
	 * @return RegExps
	 */
	public function getRegExp() {
		return $this->regExps;
	}
}
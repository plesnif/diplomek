<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Parse\Rule\Preprocessor;

use Preprasor\Preprocess\Parse\Rule\Base\Rule;

/**
 * Class representing Variable rule
 *
 * @package Preprasor\Preprocess\Parse\Rule\Preprocessor
 */
class Variable extends Magic {
	
	public $realParent;
	
	public function setRealParent(Rule $realParent) {
		$this->realParent = $realParent;
	}
	/**
	 * {@inheritDoc}
	 */
	public function prepras() {
		$variable = $this->parser->tryGetVariableByName($this->name());
		if($variable) {
			if($this->realParent) {
				$this->realParent->alterListItemForArrayByName($this, $variable->getChildren());
			} else {
				$this->getParent()->alterListItemForArrayByName($this, $variable->getChildren());
			}
		} else {
			//for extending in future - color functions on variables
		}
	}
}


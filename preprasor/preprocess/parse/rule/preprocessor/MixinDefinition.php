<?php

/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Parse\Rule\Preprocessor;

use Preprasor\Preprocess\Parse\Rule\Base\NamedRule;
use Preprasor\Preprocess\Parse\Rule\Base\Rule;
use Preprasor\Preprocess\Parse\Rule\Css\Declaration;
use Preprasor\Preprocess\Parse\Rule\Css\DeclarationList;
use Preprasor\Preprocess\Tokenize\Token\Base\Token;
use Preprasor\PreprasorErrorException;
use Preprasor\Preprocess\Tokenize\Token\Preprocessor\Variable as VariableToken;
use Preprasor\Preprocess\Parse\Rule\Preprocessor\Variable as VariableRule;
use Preprasor\Preprocess\Parse\Parser;



/**
 * Description of MixinFunction
 */
class MixinDefinition extends Magic {
	/**
	 * list of variables that might be call within mixin
	 * 
	 * @var array 
	 */
	public $variables;

	/**
	 * NamedRule constructor.
	 *
	 * @param Token $name
	 * @param array $variables
	 */
	public function __construct(Token $name, array $variables, Parser $parser) {
		parent::__construct($name, null, $parser);
		$this->setVariables($variables);
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * @codeCoverageIgnore
	 */
	public function prepras() {
		foreach ($this->list as $child) {
			$child->prepras();
		}
		$this->parent->parent->alterListItemForArray($this->parent, $this->getFirstChild()->getChildren());
/*
		if(count($this->parent->parent->getChildren()) == 1) {
			$this->parent->parent->parent->alterListItemForArray($this->parent->parent, $this->getChildren());
		} else {
			$this->parent->parent->alterListItemForArray($this->parent, $this->getFirstChild()->getChildren());
		}*/
	}
	
	
	/**
	 * sets inner variables
	 *
	 * @param array $variables
	 * @throws VariableAlreadySet
	 */
	private function setVariables(array $variables) {
		foreach($variables as $variable) {
			$name = $variable->value();
			if(isset($this->variables[$name])) {
				throw new VariableAlreadySet("Proměnná '" . $name . "' je již v mixinu definována.");
			}
			$this->variables[$name] = $variable;
		}

	}

	/**
	 * prepare mixin to be called with params
	 *
	 * @param array $params
	 * @param Rule  $parent
	 * @return MixinDefinition
	 * 
	 * @codeCoverageIgnore
	 */
	public function callWithParams(array $params, Rule $parent) {
		$clone = clone $this;
		$clone->parent = $parent;
		$i = 0;
		if(count($params) != count($clone->variables)) {
			throw new WrongParamNumber(sprintf("Špatný počet argumentů mixinu. Požadováno %d, obdrženo %d.", count($clone->variables), count($params)));
		}
		foreach($clone->variables as $key => $variable) {
			$clone->variables[$key] = $params[$i];
			$i++;
		}
		$clone->callIt();
		return $clone;
	}

	/**
	 * calls mixin
	 * 
	 * @codeCoverageIgnore
	 */
	private function callIt() {
		$first = $this->getFirstChild();
		foreach ($first->list as $child) {
			foreach ($child->list as &$subChild) {	
				if($subChild instanceof VariableToken) {
					$subChild = $this->variables[$subChild->value()];
				} elseif($subChild instanceof VariableRule) {
					$subChild->setRealParent($child);
					$subChild->prepras();
				}
			}
			unset($subChild);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function containsLocalVariable() {
		return true;
	}
	
	/**
	 * checks whenever mixin has inner variable
	 *
	 * @param string $variableName
	 * @return bool
	 */
	public function hasVariable($variableName) {
		return isset($this->variables[$variableName]);
	}
}

class MixinFunctionErrorException extends PreprasorErrorException {

}

class VariableAlreadySet extends MixinFunctionErrorException {

}

class ParamsNotSet extends MixinFunctionErrorException {

}

class VariableNotSet extends MixinFunctionErrorException {

}

class WrongParamNumber extends MixinFunctionErrorException {

}

<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Parse\Rule\Preprocessor;

use Preprasor\Preprocess\Parse\Rule\Base\Rule;
use Preprasor\Preprocess\Parse\Rule\Base\NamedRule;
use Preprasor\Preprocess\Tokenize\Token\Base\Token;
use Preprasor\PreprasorErrorException;
use Preprasor\Preprocess\Parse\Parser;

/**
 * abstract class serving as base for "magic" rules i.e. Variable, Mixin
 *
 * @package Preprasor\Preprocess\Parse\Rule\Preprocessor
 */
abstract class Magic extends NamedRule {
	/**
	 * @var Parser
	 */
	protected $parser;

	/**
	 * Magic constructor.
	 *
	 * @param Token $token
	 * @param Rule  $parent
	 * @param Parser $parser
	 */
	public function __construct(Token $token, Rule $parent = null, Parser $parser = null) {
		parent::__construct($token, $parent);
		$this->parser = $parser;
	}
}

class MagicTokenErrorException extends PreprasorErrorException {

}

class ParserNotDefined extends MagicTokenErrorException {

}

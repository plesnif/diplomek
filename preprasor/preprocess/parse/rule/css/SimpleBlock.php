<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Parse\Rule\Css;

use Preprasor\Preprocess\Parse\Rule\Base\Rule;
use Preprasor\Preprocess\Tokenize\Token\Base\BlockStarting;
use Preprasor\Constant;
use Preprasor\Preprocess\Tokenize\Token\Base\Token;

/**
 * Class representing simple blocks [], {}, ()
 *
 * @package Preprasor\Preprocess\Parse\Rule\Css
 */
class SimpleBlock extends Rule {
	/**
	 * @var Token
	 */
	private $startingToken;

	/**
	 * SimpleBlock constructor.
	 *
	 * @param BlockStarting $startingToken
	 * @param Rule|null     $parent
	 */
	public function  __construct(BlockStarting $startingToken, Rule $parent = null) {
		parent::__construct($parent);
		$this->startingToken = $startingToken;
	}

	/**
	 * {@inheritDoc}
	 */
	public function printOut($depth = 0) {
		parent::printOut();
		$this->output .= $this->startingToken->value();
		if($this->startingToken->printOutAsBlock()) {
			$this->output .= Constant::NEW_LINE;
		}
		foreach ($this->list as $child) {
			$this->output .= $child->printOut($depth+1);
		}		
		if($this->startingToken->printOutAsBlock()) {
			$this->output .= $this->getTabs($depth) . $this->endingToken()->value();
			if($depth>0) {
				$this->output .= Constant::NEW_LINE;
			}

		} else {
			$this->output .= $this->endingToken()->value();
		}		
		return $this->output;
	}

	/**
	 * returns ending token of block
	 * 
	 * @return BlockStarting
	 */
	public function endingToken() {
		return $this->startingToken->mirrorToken();
	}

	/**
	 * returns starting token for block
	 *
	 * @return Token
	 */
	public function startingToken() {
		return $this->startingToken;
	}

	/**
	 * merges two simple block
	 * 
	 * @param SimpleBlock $baseSimpleBlock
	 * @param SimpleBlock $subSimpleBlock
	 * @return SimpleBlock
	 * 
	 * @codeCoverageIgnore
	 */
	public static function merge(SimpleBlock $baseSimpleBlock = null, SimpleBlock $subSimpleBlock = null) {
		$block = new SimpleBlock($baseSimpleBlock->startingToken(), $baseSimpleBlock->parent);
		$declarationLists = array();
		foreach($baseSimpleBlock->list as $child) {
			if($child instanceof DeclarationList) {
				$declarationLists[] = $child;
			}
		}
		foreach($subSimpleBlock->list as $child) {
			if ($child instanceof DeclarationList) {
				$declarationLists[] = $child;
			}
		}
		$declarationList = DeclarationList::merge($declarationLists);
		$block->add($declarationList);
		return $block;
	}
	
	/**
	 * extracts one block from another
	 * 
	 * @param SimpleBlock $base
	 * @param SimpleBlock $exclude
	 * @return DeclarationList
	 * 
	 * @codeCoverageIgnore
	 */
	public static function exclude(SimpleBlock $base, SimpleBlock $exclude) {
		return DeclarationList::exclude($base->getFirstChild(), $exclude->getFirstChild());
	}
}

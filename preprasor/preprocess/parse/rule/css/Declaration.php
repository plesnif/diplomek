<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Parse\Rule\Css;

use Preprasor\Preprocess\Parse\Rule\Base\Rule;
use Preprasor\Preprocess\Parse\Rule\Base\NamedRule;
use Preprasor\Preprocess\Tokenize\Token\Base\Token;
use Preprasor\Constant;

/**
 * Class Declaration
 *
 * @package Preprasor\Preprocess\Parse\Rule\Css
 */
class Declaration extends NamedRule {
	/**
	 * Declaration constructor.
	 *
	 * @param Token $token
	 * @param Rule  $parent
	 */
	public function __construct(Token $token, Rule $parent) {
		parent::__construct($token, $parent);
	}

	/**
	 * {@inheritDoc}
	 */
	public function printOut($depth = 0) {
		parent::printOut();
		if ($this->list) {
			$this->output .= $this->getTabs($depth);
			$this->output .= $this->name() . Constant::COLON;
			foreach ($this->list as $child) {
				$this->output .= $child->printOut($depth);
			}
		}
		return $this->output;
	}

	/**
	 * {@inheritDoc}
	 */

	public function prepras() {
		foreach ($this->list as $child) {
			$child->prepras();
		}
	}
}




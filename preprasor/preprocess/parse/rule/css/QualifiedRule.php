<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Parse\Rule\Css;

use Preprasor\PreprasorErrorException;
use Preprasor\Preprocess\I\Compositable;
use Preprasor\Preprocess\Parse\Rule\Base\Rule;
use Preprasor\Preprocess\Tokenize\Token\Base\Token;
use Preprasor\Preprocess\Tokenize\Token\Preprocessor\ClassToken;
use Preprasor\Preprocess\Tokenize\Token\Css\Whitespace;
use Preprasor\Preprocess\Tokenize\Token\Css\Comma;
use Preprasor\Constant;
use Preprasor\Preprocess\Parse\Parser;

/**
 * Class QualifiedRule
 *
 * @package Preprasor\Preprocess\Parse\Rule\Css
 */
class QualifiedRule extends Rule {	
	private $parser;
	/**
	 * @var array
	 */
	private $prelude;

	/**
	 * @var Token
	 */
	private $isExtendedBy;
	
	/**
	 * @var Token 
	 */
	private $extends;

	/**
	 * @var SimpleBlock
	 */
	private $excludedFrom;

	/**
	 * QualifiedRule constructor.
	 *
	 * @param null|Rule $parent
	 */
	public function __construct(Parser $parser, Rule $parent = null) {
		$this->parser = $parser;
		parent::__construct($parent);
	}

	/**
	 * adds prelude
	 *
	 * @param Compositable $prelude
	 */
	public function addPrelude(Compositable $prelude) {
		$this->prelude[] = $prelude;
	}

	/**
	 * gets prelude
	 *
	 * @return array
	 */
	public function getPrelude() {
		return $this->prelude;
	}

	/**
	 * {@inheritDoc}
	 */
	public function printOut($depth = 0) {
		parent::printOut();
		if(!empty($this->prelude)) {
			$this->getWhitespaceAwarePositionForExtendingClass();
			$this->output .= $this->getTabs($depth);
			foreach ($this->prelude as $prelude) {
				$this->output .= $prelude->printOut();
			}
		}
		foreach ($this->list as $child) {
			$this->output .= $child->printOut($depth);
		}
		return $this->output;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @codeCoverageIgnore
	 */
	public function prepras() {
		if($this->isExtendedBy) {
			$this->addExtendingClassToPrelude($this->isExtendedBy);
		}
		
		//\Tracy\Debugger::dump($this);


		if($this->excludedFrom) {
			$simpleBlock = $this->getFirstChild();
			$block = SimpleBlock::exclude($this->excludedFrom, $simpleBlock);
			$simpleBlock->setFirstChild($block);
		}
		
		if(!$this->extends) {
		foreach ($this->list as $child) {
			$child->prepras();
			if($child instanceof SimpleBlock) {
				$removeThisItemFromParent = false;
				foreach($child->list as $key => $subBlock) {
					if ($subBlock instanceof QualifiedRule) {
						$qualifiedRule = self::merge($this, $subBlock);
						$this->parent->addBefore($qualifiedRule, $this);
						unset($child->list[$key]);
						$qualifiedRule->add(New Whitespace(Constant::NEW_LINE));
						$removeThisItemFromParent = true;
					}
				}
				if($removeThisItemFromParent) {
					$this->parent->removeFromList($this);
				}
			}
		}
			//\Tracy\Debugger::dump($this);
			//die;
			//$this->parent->add($this);
			
			//$this->addExtendingClassToPrelude($this->isExtendedBy);
		}
		
	}

	/**
	 * merges Qualified rules
	 *
	 * @param QualifiedRule $baseQualifiedRule
	 * @param QualifiedRule $subQualifiedRule
	 * @return QualifiedRule
	 * 
	 * @codeCoverageIgnore
	 */
	public static function merge(QualifiedRule $baseQualifiedRule, QualifiedRule $subQualifiedRule) {
		$qualifiedRule = new QualifiedRule($baseQualifiedRule->parser, $baseQualifiedRule->parent);
		foreach($baseQualifiedRule->getPrelude() as $prelude) {
			$qualifiedRule->addPrelude($prelude);
		}
		if(!$qualifiedRule->endsWithWhiteSpace()) {
			$qualifiedRule->addPrelude(new Whitespace);
		}
		foreach($subQualifiedRule->getPrelude() as $prelude) {
			$qualifiedRule->addPrelude($prelude);
		}
		if(!$qualifiedRule->endsWithWhiteSpace()) {
			$qualifiedRule->addPrelude(new Whitespace());
		}
		$qualifiedRule->list[] = SimpleBlock::merge($baseQualifiedRule->getFirstChild(), $subQualifiedRule->getFirstChild());
		return $qualifiedRule;
	}

	/**
	 * note that this rule is extended by $extendedBy
	 *
	 * @param ClassToken $extendedBy
	 * 
	 * @codeCoverageIgnore
	 */
	public function isExtendedBy (ClassToken $extendedBy) {
		$this->isExtendedBy = $extendedBy;
	}
	
	public function extendsClass(ClassToken $extends) {
		$this->extends = $extends;
	}

	/**
	 * note that this rule is excluded from $excludedFrom
	 *
	 * @param SimpleBlock $excludedFrom
	 * 
	 * @codeCoverageIgnore
	 */
	public function excludedFrom(SimpleBlock $excludedFrom) {
		$this->excludedFrom = $excludedFrom;
	}

	/**
	 * add extending class to right place in prelude
	 *
	 * @param Token $extendingClass
	 */
	public function addExtendingClassToPrelude(Token $extendingClass) {	
		$addingPosition = $this->getWhitespaceAwarePositionForExtendingClass();
		$extendingPrelude = [new Comma, new Whitespace, $extendingClass];
		$this->prelude = $this->insertArrayAtPosition($this->prelude, $extendingPrelude, $addingPosition);	
		if($this->extends) {			
			$this->parser->getClassByName($this->extends->value())->addExtendingClassToPrelude($extendingClass);
		}
	}
	
	private function getWhitespaceAwarePositionForExtendingClass() {
		$whitespace = new Whitespace;
		$position = count($this->prelude);
		$lastIndex = $position - 1;
		for($i = $lastIndex; $i>=1; $i--) {
			$token = $this->prelude[$i];
			if($token->isType($whitespace)) {
				$position = $i;
				$tokenBefore = $this->prelude[$i-1];
				if($tokenBefore->isType($whitespace)) {
					unset($this->prelude[$i]);
					$position--;
				}
				break;
			}
		}
		return $position;
	}


	/**
	 * checks whenever prelude ends with whitespace
	 * 
	 * @return boolean
	 */
	public function endsWithWhiteSpace() {
		return (bool) end($this->prelude)->isType(new Whitespace);
	}

	/**
	 * {@inheritDoc}
	 */
	public function isEmpty() {
		return parent::isEmpty() && empty($this->prelude);
	}
	
}
class QualifiedRuleErrorException extends PreprasorErrorException {

}

class CSSClassNotFound extends QualifiedRuleErrorException {

}

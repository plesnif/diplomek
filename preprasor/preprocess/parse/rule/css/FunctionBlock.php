<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Parse\Rule\Css;

use Preprasor\PreprasorErrorException;
use Preprasor\Preprocess\Color\Model\HSV;
use Preprasor\Preprocess\Color\Model\HSL;
use Preprasor\Preprocess\Color\Model\RGB;
use Preprasor\Preprocess\Color\Model\CMYK;
use Preprasor\Preprocess\Color\Model\HSVA;
use Preprasor\Preprocess\Color\Model\HSLA;
use Preprasor\Preprocess\Color\Model\RGBA;
use Preprasor\Preprocess\Color\Model\CMYKA;
use Preprasor\Preprocess\Parse\Rule\Base\NamedRule;
use Preprasor\Preprocess\Parse\Rule\Base\Rule;
use Preprasor\Preprocess\Tokenize\Token\Base\Token;
use Preprasor\Preprocess\Tokenize\Token\Css\Dimension;
use Preprasor\Preprocess\Tokenize\Token\Preprocessor\Variable;
use Preprasor\Preprocess\Parse\Rule\Preprocessor\Variable as VariableRule;
use Preprasor\Preprocess\Image\Transformator as ImageTransformator;
use Preprasor\Preprocess\Tokenize\Token\Css\URL;
use Preprasor\Constant;
use Preprasor\Preprocess\Helper\DirectoryDiver;
use Preprasor\Config\Config;

/**
 * Class FunctionBlock
 *
 * @package Preprasor\Preprocess\Parse\Rule\Css
 */
class FunctionBlock extends NamedRule {	
	/**
	 * @var Constant 
	 */
	private $directoryDiver;

	/**
	 * FunctionBlock constructor.
	 *
	 * @param Token  $token
	 * @param Rule   $parent
	 * @param DirectoryDiver $directoryDiver
	 */
	public function __construct(Token $token, Rule $parent, DirectoryDiver $directoryDiver) {
		parent::__construct($token, $parent);
		$this->directoryDiver = $directoryDiver;
	}

	/**
	 * {@inheritDoc}
	 */
	public function printOut($depth = 0) {
		parent::printOut();
		$this->output = $this->name() . Constant::LEFT_PARENTHESIS;
		if ($this->list) {
			foreach ($this->list as $child) {
				$this->output .= $child->printOut($depth);
			}
		}
		$this->output .= Constant::RIGHT_PARENTHESIS;
		return $this->output;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @codeCoverageIgnore
	 */
	public function prepras() {
		$this->callVariableFunctions();
		if ($this->list) {
			foreach ($this->list as $child) {
				$child->prepras();
			}
		}
		$this->getRidOfRedundantItems();	
		$functionName = strtolower($this->name());
		switch ($functionName) {
			case Constant::KEYWORD_RGB:
				$this->checkIndexes([0, 1, 2], $functionName);
				$params = array($this->list[0], $this->list[1], $this->list[2]);
				$color = new RGB($params);
				$this->parent->alterListItem($this, $color);
				break;
				return;
			case Constant::KEYWORD_RGBA:
				$this->checkIndexes([0, 1, 2], $functionName);
				$params = array($this->list[0], $this->list[1], $this->list[2], $this->list[3]);
				$color = new RGBA($params);
				$this->parent->alterListItem($this, $color);
				break;
				return;
			case  Constant::KEYWORD_HSV:
				$this->checkIndexes([0, 1, 2], $functionName);
				$params = array($this->list[0], $this->list[1], $this->list[2]);
				$color = new HSV($params);
				$this->parent->alterListItem($this, $color);
				return;
				break;
			case  Constant::KEYWORD_HSVA:
				$this->checkIndexes([0, 1, 2, 3], $functionName);
				$params = array($this->list[0], $this->list[1], $this->list[2], $this->list[3]);
				$color = new HSVA($params);
				$this->parent->alterListItem($this, $color);
				return;
				break;
			case  Constant::KEYWORD_HSL:
				$this->checkIndexes([0, 1, 2], $functionName);
				$params = array($this->list[0], $this->list[1], $this->list[2]);
				$color = new HSL($params);
				$this->parent->alterListItem($this, $color);
				return;
				break;
			case  Constant::KEYWORD_HSLA:
				$this->checkIndexes([0, 1, 2, 3], $functionName);
				$params = array($this->list[0], $this->list[1], $this->list[2], $this->list[3]);
				$color = new HSLA($params);
				$this->parent->alterListItem($this, $color);
				return;
				break;
			case  Constant::KEYWORD_CMYK:
				$this->checkIndexes([0, 1, 2, 3], $functionName);
				$params = array($this->list[0], $this->list[1], $this->list[2], $this->list[3]);
				$color = new CMYK($params);
				$this->parent->alterListItem($this, $color);
				return;
				break;
			case  Constant::KEYWORD_CMYKA:
				$this->checkIndexes([0, 1, 2, 3, 4], $functionName);
				$params = array($this->list[0], $this->list[1], $this->list[2], $this->list[3], $this->list[4]);
				$color = new CMYKA($params);
				$this->parent->alterListItem($this, $color);
				return;
				break;
			case  Constant::KEYWORD_SATURATE:
				$this->checkIndexes([0, 1], $functionName);
				$color = $this->list[0];
				$amount = $this->list[1];
				$color->saturate($amount);
				$this->parent->alterListItem($this, $color);
				return;
				break;			
			case  Constant::KEYWORD_DESATURATE:
				$this->checkIndexes([0, 1], $functionName);
				$color = $this->list[0];
				$amount = $this->list[1];
				$color->desaturate($amount);
				$this->parent->alterListItem($this, $color);
				return;
				break;
			case  Constant::KEYWORD_LIGHTEN:
				$this->checkIndexes([0, 1], $functionName);
				$color = $this->list[0];
				$amount = $this->list[1];
				$color->lighten($amount);
				$this->parent->alterListItem($this, $color);
				return;
				break;
			case  Constant::KEYWORD_DARKEN:
				$this->checkIndexes([0, 1], $functionName);
				$color = $this->list[0];
				$amount = $this->list[1];
				$color->darken($amount);
				$this->parent->alterListItem($this, $color);
				return;
				break;
			case  Constant::KEYWORD_CHANGE_HUE:
				$this->checkIndexes([0, 1], $functionName);
				$color = $this->list[0];
				$hue = $this->list[1];
				$color->changeHue($hue);
				$this->parent->alterListItem($this, $color);
				return;
				break;
			default:
				$exploded = explode(Constant::UNDERSCORE, $this->name());
				if(count($exploded)>1) {
					if(Config::prohibitFileManipulation()) {
						throw new FileManipulationProhibited("Manipulování se soubory je v této konfiguraci zakázáno. Funkce " . $this->name() . " není dostupná.");
					}
					list($functionName, $imageSuffix) = $exploded;
					$isImageFunction = $imageSuffix === Constant::SUFFIX_IMAGE;
					if($isImageFunction) {
						array_unshift($this->list, $functionName);
						$imageTrasformator = new ImageTransformator($this->list, $this->directoryDiver);
						$newImage = new URL(Constant::APOSTROPHE, $imageTrasformator->transformate());
						$this->parent->alterListItem($this, $newImage);
					}
				}
				break;
		}
	}
	
	private function checkIndexes(array $indexes, $functionName) {
		if (!isset($this->list)) {
			throw new ArgumentNotSet("Argument funkce \"$functionName\" není zadán.");
		}
		foreach ($indexes as $key => $index) {
			if (!isset($this->list[$index])) {
				throw new ArgumentNotSet("Argument ".($key+1)." funkce \"$functionName\" není zadán.");
			}
		}
	}

	/**
	 * calls special functions on this function block
	 *
	 * @return null|Dimension
	 * 
	 * @codeCoverageIgnore
	 */
	public function callVariableFunctions() {
		$return = null;
		switch ($this->name()) {
			case Constant::KEYWORD_IMAGE_WIDTH:
				$return = $this->readImageDimension(true);
				break;
			case Constant::KEYWORD_IMAGE_HEIGHT:
				$return = $this->readImageDimension(false);
				break;
		}
		if($return) {
			$this->parent->alterListItem($this, $return);
		}
		return $return;
	}

	/**
	 * reads image dimension
	 *
	 * @param bool $getWidth
	 * @return Dimension
	 * @throws FileNotFoundFunctionBlockErrorException
	 * 
	 * @codeCoverageIgnore
	 */
	private function readImageDimension($getWidth = true) {
		$file = $this->directoryDiver->getPath() . DIRECTORY_SEPARATOR . $this->getFirstChild()->value();
		if (!is_readable($file)) {
			throw new FileNotFoundFunctionBlockErrorException("Soubor '$file' nenalezen.");
		}
		list($width, $height) = getimagesize($file);
		if($getWidth) {
			$dimension = $width;
		} else {
			$dimension = $height;
		}
		return new Dimension($dimension, Constant::NUMBER_TYPE_INTEGER, Constant::KEYWORD_PIXELS);
	}

	/**
	 * strips redundant items(whitespace, comma) from list
	 */
	private function getRidOfRedundantItems() {
		$newList = [];
		foreach ($this->list as $item) {
			if(!$item->isSupportive()) {
				$newList[] = $item;
			}
		}
		$this->list = $newList;
	}
}
class FunctionBlockErrorException extends PreprasorErrorException {

}
class FileNotFoundFunctionBlockErrorException extends FunctionBlockErrorException {

}
class FileManipulationProhibited extends FunctionBlockErrorException {

}
class ArgumentNotSet extends FunctionBlockErrorException {

}
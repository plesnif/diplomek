<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Parse\Rule\Css;

use Preprasor\Preprocess\Parse\Rule\Base\Rule;

/**
 * Class ListOfRules
 *
 * @package Preprasor\Preprocess\Parse\Rule\Css
 */
class ListOfRules extends Rule {
	/**
	 * @var bool
	 */
	private $topLevelFlag = false;

	/**
	 * ListOfRules constructor.
	 *
	 * @param Rule $parent
	 * @param bool $topLevelFlag
	 */
	public function __construct(Rule $parent, $topLevelFlag = false) {
		parent::__construct($parent);
		$this->topLevelFlag = $topLevelFlag;
	}

	/**
	 * {@inheritDoc}
	 */
	public function printOut($depth = 0) {
		parent::printOut();
		if ($this->list) {
			foreach ($this->list as $child) {
				$this->output .= $child->printOut($depth);
			}
		}
		return $this->output;
	}

}

<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Parse\Rule\Css;

use Preprasor\Preprocess\Parse\Rule\Base\Rule;
use Preprasor\Constant;
use Preprasor\Preprocess\Tokenize\Token\Base\Token;

/**
 * Class representing list of declarations
 *
 * @package Preprasor\Preprocess\Parse\Rule\Css
 */
class DeclarationList extends Rule {
	/**
	 * {@inheritDoc}
	 * 
	 * @codeCoverageIgnore
	 */
	public function printOut($depth = 0) {
		parent::printOut();
		$lastElement = end($this->list);

		foreach ($this->list as $child) {			
			$this->output .= $child->printOut($depth);
			if($child === $lastElement) {
				if(!($child instanceof Token)) {
					$lastElementOfChild = end($child->list);
					if(!$lastElementOfChild->isNewline()) {
						$this->output .= Constant::NEW_LINE;
					}
				}
			} else {
				$this->output .= Constant::NEW_LINE;
			}
		}
		return $this->output;
	}

	/**
	 * merges declarations
	 * 
	 * @param array $declarationLists
	 * @return DeclarationList
	 * 
	 * @codeCoverageIgnore
	 */
	public static function merge(array $declarationLists) {
		$list = array();
		foreach($declarationLists as $declarationList) {
			foreach($declarationList->list as $declaration) {
				$list[$declaration->name()] = $declaration;
			}
		}
		$declarationList = new DeclarationList();
		foreach($list as $declaration) {
			$declarationList->add($declaration);
		}
		return $declarationList;
	}

	/**
	 * excludes declarations lists $exclude from $base
	 * 
	 * @param DeclarationList $base
	 * @param DeclarationList $exclude
	 * @return DeclarationList
	 * 
	 * @codeCoverageIgnore
	 */
	public static function exclude(DeclarationList $base, DeclarationList $exclude) {
		$declarationList = new DeclarationList(null);
		$tempDeclarations = [];	
		foreach($base->list as $baseChild) {
			$tempDeclarations[$baseChild->name()] = $baseChild;
		}
		foreach($exclude->list as $excludeChild) {		
			if(key_exists($excludeChild->name(), $tempDeclarations)) {			
				unset($tempDeclarations[$excludeChild->name()]);
			}
		}
		foreach($tempDeclarations as $baseChild) {
			$declarationList->add($baseChild);
		}
		
		return $declarationList;
	}
}

<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Parse\Rule\Css;

use Preprasor\Preprocess\Parse\Rule\Base\Rule;

/**
 * Class Stylesheet
 *
 * @package Preprasor\Preprocess\Parse\Rule\Css
 */
class Stylesheet extends Rule {
	/**
	 * Stylesheet constructor.
	 *
	 * @param null|Rule $parent
	 */
	public function __construct(Rule $parent = null) {
		parent::__construct($parent);
	}

	/**
	 * {@inheritDoc}
	 */
	public function printOut($depth = 0) {
		parent::printOut();
		if ($this->list) {
			foreach ($this->list as $child) {
				$this->output .= $child->printOut(0);
			}
		}
		return $this->output;
	}
}

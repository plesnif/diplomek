<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Parse\Rule\Css;

use Preprasor\PreprasorErrorException;
use Preprasor\Preprocess\I\Compositable;
use Preprasor\Preprocess\Parse\Rule\Base\Rule;
use Preprasor\Preprocess\Parse\Rule\Base\NamedRule;
use Preprasor\Preprocess\Tokenize\Token\Base\Token;
use Preprasor\Constant;

/**
 * Class representing CSS At rules
 *
 * @package Preprasor\Preprocess\Parse\Rule\Css
 */
class AtRule extends NamedRule {
	/**
	 * @var array 
	 */
	private $prelude = [];

	/**
	 * AtRule constructor.
	 *
	 * @param Token $token
	 * @param Rule  $parent
	 */
	public function __construct(Token $token, Rule $parent) {
		parent::__construct($token, $parent);
		$this->prelude = array();
	}

	/**
	 * sets prelude
	 * 
	 * @param Compositable $prelude
	 */
	public function setPrelude(Compositable $prelude) {
		$this->prelude[] = $prelude;
	}

	/**
	 * {@inheritDoc}
	 */
	public function printOut($depth = 0) {
		parent::printOut();
		$this->output = $this->name->printOut($depth);
		foreach ($this->prelude as $value) {
			$this->output .= $value->printOut($depth);
		}
		if ($this->list) {
			foreach ($this->list as $value) {
				$this->output .= $value->printOut($depth);
			}
		}
		return $this->output;
	}
	
	/**
	 * gets prelude, presumes only one child in prelude list
	 * 
	 * @return Token
	 * @throws PreludeNotFound
	 */
	public function getPrelude() {
		if(!count($this->prelude)==1) {
			throw new PreludeNotFound("Prelude č. 1 nenalezena, počet prelude ".count($this->prelude));
		}
		return $this->prelude[0];
	}

	/**
	 * checks if rule is responsible for require external file
	 * 
	 * @return boolean
	 */
	public function requiresExternalFile() {
		return $this->compareNameWithKeyword(Constant::KEYWORD_REQUIRE);
	}
	
	/**
	 * checks if rule is charset rule
	 * 
	 * @return boolean
	 */
	public function isCharsetRule() {
		return $this->compareNameWithKeyword(Constant::KEYWORD_CHARSET);
	}
	
	/**
	 * compares name with $keyword
	 *
	 * @param string $keyword
	 * @return boolean
	 */
	private function compareNameWithKeyword($keyword) {
		return strtolower($this->name()) === $keyword;
	}

	/**
	 * {@inheritDoc}
	 */
	public function isEmpty() {
		return parent::isEmpty() && empty($this->prelude);
	}
}
class AtRuleErrorException extends PreprasorErrorException {

}
class FileNotFoundAtRuleErrorException extends AtRuleErrorException {

}

class PreludeNotFound extends AtRuleErrorException {

}

<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Parse\Rule\Base;

use Preprasor\PreprasorErrorException;
use Preprasor\Preprocess\I\Compositable;
use Preprasor\Preprocess\Parse\Rule\Base\I\Listable;
use Preprasor\Constant;
use Preprasor\Preprocess\Parse\Rule\Css\QualifiedRule;
use Tracy\Debugger;

/**
 * Basic class for representing CSS rules
 */
abstract class Rule implements Compositable, Listable {
	/**
	 * @var array 
	 */
	protected $list;
	
	/**
	 * @var string 
	 */
	protected $output;
	
	/**
	 * @var Rule
	 */
	public $parent;

	/**
	 * Rule constructor.
	 *
	 * @param Rule|null $parent
	 */
	public function __construct(Rule $parent = null) {
		$this->parent = $parent;
		$this->output = "";
		$this->list = array();
	}

	/**
	 * {@inheritDoc}
	 */
	public function add(Compositable $value) {
		$this->list[] = $value;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function addBefore(Compositable $item, Compositable $insertBefore) {
		$offset = 0;
		foreach ($this->list as $key => $child) {
			if ($child === $insertBefore) {
				break;
			}
			$offset++;
		}
		$this->list = array_merge(
			array_slice($this->list, 0, $offset, true),
			array($item),
			array_slice($this->list, $offset, count($this->list) - 1, true));
	}

	/**
	 * {@inheritDoc}
	 */
	public function addAfter(Compositable $item, Compositable $insertAfter) {
		$offset = 0;
		foreach ($this->list as $key => $child) {
			$offset++;
			if ($child === $insertAfter) {
				break;
			}
		}
		$this->list = array_merge(
			array_slice($this->list, 0, $offset, true),
			array($item),
			array_slice($this->list, $offset, count($this->list) - 1, true));
	}

	/**
	 * inserts $insertingArray into $baseArray on specific $position
	 *
	 * @param array $baseArray
	 * @param array $insertingArray
	 * @param int $position
	 * @return array
	 */
	protected function insertArrayAtPosition($baseArray, $insertingArray, $position) {
		if($position==0) {
			$baseArray = array_merge($insertingArray, $baseArray);
		} else {
			if($position > (count($baseArray)-1)) {
				$baseArray = array_merge($baseArray, $insertingArray);
			} else {
				$firstPart = array_slice($baseArray, 0, $position);
				$secondPart = array_slice($baseArray, $position);
				$baseArray = array_merge($firstPart, $insertingArray, $secondPart);
			}
		}
		return $baseArray;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function printOut($depth = 0) {
		$this->output = "";
	}

	/**
	 * checks if rule is empty
	 * 
	 * @return boolean
	 */
    public function isEmpty() {
		return empty($this->list);
    }

	/**
	 * returns adequate tabs
	 * 
	 * @param int $depth
	 * @return string
	 */
	protected function getTabs($depth) {
		return str_repeat(Constant::TAB, $depth);
	}

	/**
	 * {@inheritDoc}
	 */
	public function prepras() {
		if ($this->list) {
			foreach ($this->list as $child) {
				$child->prepras();
			}
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getChildren() {
		return $this->list;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function removeFromList(Compositable $rule) {
		foreach($this->list as $key => $child) {
			if($child === $rule) {
				unset($this->list[$key]);
				return;
			}
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function alterListItem(Compositable $item, Compositable $alteredItem) {
		foreach($this->list as $key => $child) {
			if($child === $item) {
				$this->list[$key] = $alteredItem;
				break;
			}
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function getFirstChild() {
		if(!count($this->list) == 1) {
			throw new ChildNotFound("Potomek č. 1 nenalezen, počet potomků ".count($this->list));
		}
		return $this->list[0];
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function setFirstChild(Compositable $child) {
		$this->list[0] = $child;
	}

	/**
	 * {@inheritDoc}
	 */
	public function alterListItemForArray(Compositable $item, array $array) {
		foreach($this->list as $key => $child) {
			if($child === $item) {
				unset($this->list[$key]);
				$this->list = $this->insertArrayAtPosition($this->list, $array, $key);
			}
		}
	}
	
	public function alterListItemForArrayByName(Compositable $item, array $array) {
		foreach($this->list as $key => $child) {
			if($child->name() == $item->name()) {
				unset($this->list[$key]);	
				$this->list = $this->insertArrayAtPosition($this->list, $array, $key);	
				break;
			}
		}
	}
	
	
	/**
	 * overrides standard clone method for Mixin mechanisms pourposes
	 * 
	 * @codeCoverageIgnore
	 */
	public function __clone() {
		foreach($this->list as &$child) {
			$child = clone $child;
		}
		unset($child);
	}

	/**
	 * {@inheritDoc}
	 */
	public function getParent() {
		return $this->parent;
	}
	
	/**
	 * {@inheritDoc}
	 */
	/*
	public function name() {
		return false;
	}
	*/
	/**
	 * check whenever Rule contails local variables
	 * 
	 * @return boolean
	 */
	public function containsLocalVariable() {
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function isNewline() {
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function isSupportive() {
		return false;
	}
}
class RuleErrorException extends PreprasorErrorException {

}

class ChildNotFound extends RuleErrorException {

}

<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Parse\Rule\Base;

use Preprasor\Preprocess\Tokenize\Token\Base\Token;
use Preprasor\Preprocess\I\Compositable;

/**
 * Class representing named rules
 *
 * @package Preprasor\Preprocess\Parse\Rule\Base
 */
abstract class NamedRule extends Rule {
	/**
	 * @var Token
	 */
	protected $name;

	/**
	 * NamedRule constructor.
	 *
	 * @param Token $name
	 * @param Rule  $parent

	 */
	public function __construct(Token $name, Rule $parent = null) {
		parent::__construct($parent);
		$this->name = $name;
	}

	/**
	 * @return Compositable|string
	 */
	public function name() {
		return $this->name->value();
	}
}

<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Parse\Rule\Base\I;

use Preprasor\Preprocess\I\Compositable;
use Preprasor\Preprocess\Parse\Rule\Base\ChildNotFound;
use Preprasor\Preprocess\Parse\Rule\Base\Rule;

/**
 * Interface denoting class able to list itself
 */
interface Listable {
	/**
	 * returns array of children represented as Compositables
	 * 
	 * @return array
	 */
	public function getChildren();
	/**
	 * adds item to list
	 * 
	 * @param Compositable $item
	 */
	public function add(Compositable $item);
	
	/**
	 * insterts $item in list before $insertBefore
	 * 
	 * @param Compositable $item
	 * @param Compositable $insertBefore
	 */
	public function addBefore(Compositable $item, Compositable $insertBefore);
	
	/**
	 * insterts $item in list after $insertAfter
	 * 
	 * @param Compositable $item
	 * @param Compositable $insertAfter
	 */
	public function addAfter(Compositable $item, Compositable $insertAfter);
	
	/**
	 * checks if rule is empty
	 * 
	 * @return boolean
	 */
    public function isEmpty();
	
	/**
	 * removes $rule from list
	 * 
	 * @param Compositable $rule
	 */
	public function removeFromList(Compositable $rule);
	
	/**
	 * switches $item in list for $alteredItem
	 * 
	 * @param Compositable $item
	 * @param Compositable $alteredItem
	 */
	public function alterListItem(Compositable $item, Compositable $alteredItem);
	
	/**
	 * gets first child from list, presumes only one child in list
	 * 
	 * @return Compositable
	 * @throws ChildNotFound
	 */
	public function getFirstChild();
	
	/**
	 * sets first child in list, presumes only one child in list
	 *
	 * @param Compositable $child
	 * @return Compositable
	 * @throws ChildNotFound
	 */
	public function setFirstChild(Compositable $child);
	
	/**
	 * replaces item in rule's list by given array
	 *
	 * @param Compositable $item
	 * @param array $array
	 */
	public function alterListItemForArray(Compositable $item, array $array);
	
	/**
	 * returns parent rule
	 * 
	 * @return Rule
	 */
	public function getParent();
	
	
}

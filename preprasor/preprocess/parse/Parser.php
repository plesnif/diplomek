<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Parse;

use Preprasor\PreprasorErrorException;
use Preprasor\Preprocess\Parse\Rule\Base\Rule;
use Preprasor\Preprocess\Tokenize\Token\Css\LeftCurlyBracket;
use Preprasor\Preprocess\Tokenize\Token\Css\Semicolon;
use Preprasor\Preprocess\Tokenize\Token\TokenTypes;
use Preprasor\Preprocess\Tokenize\Tokenizer\I\Tokenizerable;
use Preprasor\Preprocess\Tokenize\Token\Base\BlockStarting;
use Preprasor\Preprocess\Tokenize\Token\Css\Whitespace;
use Preprasor\Preprocess\Tokenize\Token\Css\FunctionToken;
use Preprasor\Preprocess\Parse\Rule\Css\Stylesheet;
use Preprasor\Preprocess\Parse\Rule\Css\ListOfRules;
use Preprasor\Preprocess\Parse\Rule\Css\AtRule;
use Preprasor\Preprocess\Parse\Rule\Css\SimpleBlock;
use Preprasor\Preprocess\Parse\Rule\Css\DeclarationList;
use Preprasor\Preprocess\Parse\Rule\Css\Declaration;
use Preprasor\Preprocess\Parse\Rule\Css\QualifiedRule;
use Preprasor\Preprocess\Parse\Rule\Css\FunctionBlock;
use Preprasor\Preprocess\Parse\Rule\Preprocessor\Variable as VariableRule;
use Preprasor\Preprocess\Tokenize\Token\Preprocessor\ClassToken;
use Preprasor\Preprocess\Tokenize\Token\Preprocessor\Variable as VariableToken;
use Preprasor\Preprocess\Parse\Rule\Preprocessor\MixinDefinition;
use Preprasor\Preprocess\Parse\Rule\Preprocessor\Mixin;
use Preprasor\Preprocess\I\Initiable;
use Preprasor\Preprocess\Helper\DirectoryDiver;
use Preprasor\Config\Config;
use Preprasor\Constant;
use Preprasor\Preprocess\Color\Model\HSV;
use Preprasor\Preprocess\Color\Model\HSL;
use Preprasor\Preprocess\Color\Model\RGB;
use Preprasor\Preprocess\Color\Model\CMYK;
use Preprasor\Preprocess\Color\Model\HSVA;
use Preprasor\Preprocess\Color\Model\HSLA;
use Preprasor\Preprocess\Color\Model\RGBA;
use Preprasor\Preprocess\Color\Model\CMYKA;
use Tracy\Debugger;

/**
 * Class Parser
 *
 * @package Preprasor\Preprocess\Parse
 */
class Parser implements Initiable {
	/**
	 * @var Tokenizerable
	 */
	private $tokenizer;
	/**
	 * @var DirectoryDiver
	 */
	protected $directoryDiver;
	/**
	 * @var array
	 */
	private $variables;
	/**
	 * @var array
	 */
	private $classes;
	/**
	 * @var Stylesheet
	 */
	private $styleSheet;
	/**
	 * @var array
	 */
	private $mixinFunctions;


	/**
	 * Parser constructor.
	 *
	 * @param Tokenizerable  $tokenizer
	 * @param DirectoryDiver $directoryDiver
	 */
	public function __construct(Tokenizerable $tokenizer, DirectoryDiver $directoryDiver) {
		$this->variables = array();
		$this->tokenizer = $tokenizer;
		$this->directoryDiver = $directoryDiver;
	}

	/**
	 * initialize parser
	 */
	public function init() {
		$this->tokenizer->init();
		$this->styleSheet = $this->parseStylesheet();
	}

	/**
	 * dumps CSS tree
	 */
	public function dumpCSSTree() {
		$this->checkIfParserRunned();
		\Tracy\Debugger::dump($this->styleSheet);
	}

	/**
	 * prints CSS output
	 *
	 * @return string|void
	 */
	public function printCSSOutput() {
		$this->checkIfParserRunned();
		return $this->styleSheet->printOut();
	}

	/**
	 * prints tokens
	 */
	public function printTokens() {
		$this->tokenizer->printTokens();
	}

	/**
	 * checks whenever is parser set
	 *
	 * @throws ParserDidNotRun
	 */
	private function checkIfParserRunned() {
		if(is_null($this->styleSheet)) {
			throw new ParserDidNotRun("Neproběhla fáze parsování.");
		}
	}

	/**
	 * gets object containing tokens for comparation
	 *
	 * @return TokenTypes
	 */
	private function getTokenType() {
		return $this->tokenizer->getTokenType();
	}

	/**
	 * parses stylesheet
	 *
	 * @return Stylesheet
	 */
	private function parseStylesheet() {
		$styleSheet = new Stylesheet(null);
		$this->consumeListOfRules($styleSheet, true);
		$this->callVariableFunction();
		$styleSheet->prepras();
		return $styleSheet;
	}

	/**
	 * call variable functions
	 */
	private function callVariableFunction() {
		foreach ($this->variables as $variable) {
			$variable->getFirstChild()->prepras();
		}
	}

	/**
	 * consumes fist of rules
	 *
	 * @param Stylesheet $parent
	 * @param bool       $topLevelFlag
	 */
	private function consumeListOfRules(Stylesheet $parent, $topLevelFlag = false) {
		$listOfRules = new ListOfRules($parent, $topLevelFlag);
		while ($token = $this->tokenizer->nextToken()) {
			if($token->isType($this->getTokenType()->whitespace)) {
				//do nothing
			}
			if ($token->isType($this->getTokenType()->EOF)) {
				$parent->add($listOfRules);
				return;
			}
			
			// @codeCoverageIgnoreStart		
			//this is part of recomended algorithm, but in this context is not reachable 
			if ($topLevelFlag === false) {
				if ($token->belongsInTypes([$this->getTokenType()->CDC, $this->getTokenType()->CDO])) {
					$this->reconsumeToken();
					$qualifiedRule = $this->consumeQualifiedRule($listOfRules);
					if($qualifiedRule) {
						$listOfRules->add($qualifiedRule);
					}
				}
			}
			// @codeCoverageIgnoreEnd			
			if ($token->isType($this->getTokenType()->atKeyword)) {
				$this->reconsumeToken();
				$atRule = $this->consumeAtRule($listOfRules);
				if ($atRule) {
					if(!$atRule->isCharsetRule()) {
						$listOfRules->add($atRule);
					}
					continue;
				}
			}
			
			$this->reconsumeToken();
			$qualifiedRule = $this->consumeQualifiedRule($listOfRules);
			if($qualifiedRule) {
				$listOfRules->add($qualifiedRule);
			}
		}
	}

	/**
	 * consumes at rule
	 *
	 * @param Rule $parent
	 */
	private function consumeAtRule(Rule $parent) {
		$atRule = new AtRule($this->tokenizer->nextToken(), $parent);
		while ($token = $this->tokenizer->nextToken()) {
			if ($token->belongsInTypes([$this->getTokenType()->EOF, $this->getTokenType()->semicolon])) {
				$parent->add($atRule);
				return;
			}
			if ($token->belongsInTypes([$this->getTokenType()->leftCurlyBracket])) {
				$simpleBlock = $this->consumeStandardBlock($parent, $token);
				if ($simpleBlock) {
					$atRule->add($simpleBlock);
					$parent->add($atRule);
					return;
				}
			}
			$this->reconsumeToken();
			$atRule->setPrelude($this->consumeComponentValue($parent));
		}
	}

	/**
	 * consumes qulified rule
	 *
	 * @param ListOfRules $parent
	 */
	private function consumeQualifiedRule(ListOfRules $parent) {
		$qualifiedRule = new QualifiedRule($this, $parent);
		while ($token = $this->tokenizer->nextToken()) {
			if ($token->isType($this->getTokenType()->comment)) {
				$qualifiedRule->add($token);
				return $qualifiedRule;
			}

			if($token->isType($this->getTokenType()->mixin)) {
				$parent->add(new Whitespace("\n"));
				$this->consumeMixinDefinition();
				return;
			}

			if($token->isType($this->getTokenType()->variable)) {
				$variable = $this->consumeVariableDefinition($qualifiedRule);
				if($variable) {		
					$this->addVariable($variable);
					return;
				}
			}

			if($token->isType($this->getTokenType()->class)) {
				$this->addClass($token, $qualifiedRule);
			}


			if($token->isType($this->getTokenType()->excludeFrom)) {
				$previousToken = $this->tokenizer->getPreviousNonWhitespaceToken();
				$excludedFrom = $this->tokenizer->getNextNonWhitespaceToken();
				if(!$excludedFrom->isType($this->getTokenType()->class) || !$excludedFrom->isType($this->getTokenType()->class)) {
					$previousTokenR = new \ReflectionClass($previousToken);
					$excludedFromR = new \ReflectionClass($excludedFrom);
					throw new TokenClassRequired("Pro využití konstruktu exclude musí syntaxe splňovat formát ClassToken excludeFrom ClassToken. Zadáno " . $previousTokenR->getShortName() . " excludeFrom " . $excludedFromR->getShortName() . ".");
				}
				$class = $this->getClass($previousToken);
				$excludeClassContent = $this->getClass($excludedFrom)->getFirstChild();
				$class->excludedFrom($excludeClassContent);
				continue;
			}

			if($token->isType($this->getTokenType()->extends)) {
				$extendingToken = $this->tokenizer->getPreviousNonWhitespaceToken();
				$extendedToken = $this->tokenizer->getNextNonWhitespaceToken();
				if(!$extendingToken->isType($this->getTokenType()->class) || !$extendedToken->isType($this->getTokenType()->class)) {
					$extendingTokenR = new \ReflectionClass($extendingToken);
					$extendedTokenR = new \ReflectionClass($extendedToken);
					throw new TokenClassRequired("Pro využití konstruktu extends musí syntaxe splňovat formát ClassToken extends ClassToken. Zadáno " . $extendingTokenR->getShortName() . " excludeFrom " . $extendedTokenR->getShortName() . ".");
				}

				$extendingClass = $this->getClass($extendedToken);
				$extendingClass->isExtendedBy($extendingToken);
				$extendedClass = $this->getClass($extendingToken);
				$extendedClass->extendsClass($extendedToken);
				continue;
			}

			if ($token->isType($this->getTokenType()->EOF)) {
				if($qualifiedRule->isEmpty()) {
					$this->reconsumeToken();
					return false;
				} else {
					return $qualifiedRule;
				}

			}
			if ($token->isType($this->getTokenType()->leftCurlyBracket)) {
				$simpleBlock = $this->consumeStandardBlock($qualifiedRule, $token);
				if ($simpleBlock) {
					$qualifiedRule->add($simpleBlock);
					return $qualifiedRule;
				}
			}
			$this->reconsumeToken();
			$componentValue = $this->consumeComponentValue($qualifiedRule);

			if ($componentValue) {
				$qualifiedRule->addPrelude($componentValue);
			}
		}
	}

	/**
	 * consumes declaration list
	 *
	 * @param Rule          $parent
	 * @param BlockStarting $startingToken
	 * @return DeclarationList
	 */
	private function consumeDeclarationList(Rule $parent, BlockStarting $startingToken) {
		$declarationList = new DeclarationList($parent);
		$this->reconsumeToken();	
		while ($token = $this->tokenizer->nextToken()) {
			if ($token->belongsInTypes([$this->getTokenType()->whitespace, $this->getTokenType()->semicolon])) {
				continue;				
			}
			if($token->belongsInTypes([$startingToken->mirrorToken(), $this->getTokenType()->EOF])) {
				return $declarationList;
			}
			if ($token->isType($this->getTokenType()->atKeyword)) {
				if($token->isMixin()) {
					$rule = $this->consumeMixinCall($declarationList, $this);
				} else {
					$this->reconsumeToken();
					$rule = $this->consumeAtRule($declarationList);
				}
				if ($rule) {
					$declarationList->add($rule);
					continue;
				}
			}
			if ($token->isType($this->getTokenType()->ident)) {
				$declaration = $this->consumeDeclaration($declarationList);
				if ($declaration) {
					$declarationList->add($declaration);
				}
			}
			//in case of last declaration without ending semicolon
			$token = $this->tokenizer->getActualToken();
			if($token->isType($this->getTokenType()->rightCurlyBracket)) {
				return $declarationList;
			}
			
			$this->reconsumeToken();
			//difference from recomended specification, dont throw away - comments might be in declaration
			if(!$this->tokenizer->getActualToken()->isType($this->getTokenType()->semicolon)) {
				$componentValue = $this->consumeComponentValue($declarationList);
				if($componentValue) {
					$declarationList->add($componentValue);
				}
			}
		}
	}

	/**
	 * consumes declaration
	 *
	 * @param DeclarationList $parent
	 * @return Declaration
	 * @throws ParseErrorException
	 */
	private function consumeDeclaration(DeclarationList $parent) {
		$declaration = new Declaration($this->tokenizer->getActualToken(), $parent);
		$token = $this->tokenizer->nextToken();
		// for exclude purposes
		if ($token->isType($this->getTokenType()->semicolon)) {
			$declaration->add($token);
			return $declaration;
		}
		if (!$token->isType($this->getTokenType()->colon)) {
			throw new ParseErrorException("Token " . get_class($token) . " - " . $token->value() . " Chybí dvojtečka, nesprávný tvar deklarace.");
		}
		while ($token = $this->tokenizer->nextToken()) {
			if($token->isType($this->getTokenType()->whitespace)) {
				$declaration->add($token);
				continue;
			}
			if ($token->isType($this->getTokenType()->EOF)) {
				return $declaration;
			}
			if ($token->isType($this->getTokenType()->semicolon)) {
				$declaration->add($token);
				return $declaration;
			}
			if ($token->isType($this->getTokenType()->rightCurlyBracket)) {
				$this->reconsumeToken();
				return $declaration;
			}
			
			//adds seminicolon at end of the last declaration to create proper rule
			if(Config::addLastSemicolon()) {
				$lookAhead = $this->tokenizer->getNextNonWhitespaceToken();	
				if($lookAhead->isType($this->getTokenType()->rightCurlyBracket)) {
					$children = $declaration->getChildren();
					$child = end($children);
					$declaration->addBefore(new Semicolon, $child);
					return $declaration;
				} else {
					$this->tokenizer->getPreviousNonWhitespaceToken();
				}
			}
			if($token->isType($this->getTokenType()->function)) {
				$isColor = in_array(strtolower($token->value()), [Constant::KEYWORD_RGB, Constant::KEYWORD_RGBA, Constant::KEYWORD_HSL, Constant::KEYWORD_HSLA, Constant::KEYWORD_HSV, Constant::KEYWORD_HSVA, Constant::KEYWORD_CMYK, Constant::KEYWORD_CMYKA]);
				if($isColor) {
					$colorToken = $this->consumeColor($token);
					$declaration->add($colorToken);
					continue;
				}
			}
			$this->reconsumeToken();
			$componentValue = $this->consumeComponentValue($declaration);
			if ($componentValue) {
				$declaration->add($componentValue);
			}
		}
	}

	/**
	 * consumes component value
	 *
	 * @param Rule $parent
	 * @return FunctionBlock|SimpleBlock|VariableRule
	 */
	private function consumeComponentValue(Rule $parent) {
		$token = $this->tokenizer->nextToken();
		if ($token->isType($this->getTokenType()->variable)) {
			if($parent->parent->parent->containsLocalVariable() && $parent->parent->parent->hasVariable($token->value())) {
				return $token;
			} else {
				$variableRule = new VariableRule($token, $parent, $this);
				return $variableRule;
			}			
		}
		$blocksAllowed = [$this->getTokenType()->leftCurlyBracket, $this->getTokenType()->leftSquareBracket, $this->getTokenType()->leftParenthesis];
		if ($token->belongsInTypes($blocksAllowed)) {
			$simpleBlock = $this->consumeSimpleBlock($parent, $token);
			if ($simpleBlock) {
				return $simpleBlock;
			}
		}
		if ($token->isType($this->getTokenType()->function)) {
			$function = $this->consumeFunction($parent);
			if ($function) {
				return $function;
			}
		}
		return $token;
	}

	private function consumeStandardBlock(Rule $parent, LeftCurlyBracket $startingToken, $mixin = null) {
		$simpleBlock = new SimpleBlock($startingToken, $parent);
		while ($token = $this->tokenizer->nextToken()) {

			if ($token->isType($this->getTokenType()->whitespace)) {
				continue;
			}			
			if ($token->isType($this->getTokenType()->EOF)) {
				return $simpleBlock;
			}
			if ($token->isType($startingToken->mirrorToken())) {

				return $simpleBlock;
			}			
			if($token->belongsInTypes([$this->getTokenType()->ident, $this->getTokenType()->class])) {
				$lookAheadToken = $this->tokenizer->getNextNonWhitespaceToken();
				if($lookAheadToken->isType($this->getTokenType()->leftCurlyBracket)) {
					$qualifiedRule = new QualifiedRule($this, $simpleBlock);
					$qualifiedRule->addPrelude($token);
					$subSimpleBlock = $this->consumeStandardBlock($qualifiedRule, $lookAheadToken);
					if ($subSimpleBlock) {

						$qualifiedRule->add($subSimpleBlock);
					}

					$simpleBlock->add($qualifiedRule);
					if($token->isType($this->getTokenType()->class)) {
						$this->addClass($token, $qualifiedRule);
					}
					continue;
				} else {
					$this->tokenizer->revertLookAhead();
				}
			}
			$declarationList = $this->consumeDeclarationList($simpleBlock, $startingToken);
			if ($declarationList) {

				$simpleBlock->add($declarationList);
				$actualToken = $this->tokenizer->getActualToken();

				if ($actualToken->isType($startingToken->mirrorToken())) {
					return $simpleBlock;
				}
			}
			$componentValue = $this->consumeComponentValue($simpleBlock);

			if ($componentValue) {
				$simpleBlock->add($componentValue);
			}
		}
	}

	private function consumeSimpleBlock(Rule $parent, BlockStarting $startingToken) {
		$simpleBlock = new SimpleBlock($startingToken, $parent);
		while ($token = $this->tokenizer->nextToken()) {
			if ($token->isType($this->getTokenType()->EOF)) {
				return $simpleBlock;
			}
			if ($token->isType($startingToken->mirrorToken())) {
				return $simpleBlock;
			}			
			$this->reconsumeToken();
			$componentValue = $this->consumeComponentValue($simpleBlock);

			if ($componentValue) {
				$simpleBlock->add($componentValue);
			}
		}
	}
	
	private function consumeFunction(Rule $parent) {
		$function = new FunctionBlock($this->tokenizer->getActualToken(), $parent, $this->directoryDiver);
		while ($token = $this->tokenizer->nextToken()) {
			if ($token->belongsInTypes(array($this->getTokenType()->EOF, $this->getTokenType()->rightParenthesis))) {
				return $function;
			}
			$this->reconsumeToken();
			$componentValue = $this->consumeComponentValue($function);
			if ($componentValue) {
				$function->add($componentValue);
			}
		}
	}
	/**
	 * this part is particulary tricky, MixinFunction = general mixin definition, MixinCall = specific mixin call with parameters
	 * 
	 * @param Rule $parent
	 * @param \Preprasor\Preprocess\Parse\Parser $parser
	 * @return Mixin
	 */
	private function consumeMixinCall(Rule $parent, Parser $parser) {
		$mixinContainer = new Mixin($this->tokenizer->getNextNonWhitespaceToken(), $parent, $parser);		
		while ($token = $this->tokenizer->nextToken()) {
			if($token->isType($this->getTokenType()->whitespace)) {
				continue;
			}
			if ($token->belongsInTypes([$this->getTokenType()->EOF, $this->getTokenType()->semicolon])) {
				return $mixinContainer;
			}
			$this->reconsumeToken();
			$params = array();
			while ($token = $this->tokenizer->nextToken()) {
				if($token->isType($this->getTokenType()->whitespace)) {
					continue;
				}
				if($token->isType($this->getTokenType()->comma)) {
					continue;
				}
				if($token->isType($this->getTokenType()->rightParenthesis)) {
					break;
				}
				if($token->isType($this->getTokenType()->function)) {
					$isColor = in_array(strtolower($token->value()), [Constant::KEYWORD_RGB, Constant::KEYWORD_RGBA, Constant::KEYWORD_HSL, Constant::KEYWORD_HSLA, Constant::KEYWORD_HSV, Constant::KEYWORD_HSVA, Constant::KEYWORD_CMYK, Constant::KEYWORD_CMYKA]);
					if($isColor) {
						$token = $this->consumeColor($token);
					}
				}
				$params[] = $token;
			}
			$mixinDefinition = $this->getMixinByName($mixinContainer->name());
			$mixinCall = $mixinDefinition->callWithParams($params, $mixinContainer);
			$mixinContainer->add($mixinCall);
			return $mixinContainer;
		}
	}

	private function consumeVariableDefinition(Rule $parent) {
		$variable = new VariableRule($this->tokenizer->getActualToken(), $parent, $this);
		$nextTokenIsWhitespace = $this->tokenizer->nextToken()->isType($this->getTokenType()->whitespace);
		if(!$nextTokenIsWhitespace) {
			throw new ParseErrorException("Po proměnné musí následovat mezera.");
		}
		while ($token = $this->tokenizer->nextToken()) {
			if ($token->belongsInTypes([$this->getTokenType()->EOF, $this->getTokenType()->semicolon])) {
				//consume additional space after
				$this->tokenizer->getNextNonWhitespaceToken();
				$this->reconsumeToken();
				return $variable;
			}
			$this->reconsumeToken();
			$componentValue = $this->consumeComponentValue($variable);
			$variable->add($componentValue);
		}
	}
	
	private function consumeMixinDefinition() {
		$mixinName = $this->tokenizer->getActualToken();
		$variables = $this->getMixinInternalVariables();
		$mixinFunction = new MixinDefinition($mixinName, $variables, $this);
		while ($token = $this->tokenizer->nextToken()) {
			if ($token->isType($this->getTokenType()->whitespace)) {
				continue;
			}
			if ($token->isType($this->getTokenType()->EOF)) {
				$this->addMixinFunction($mixinFunction);
				return;
			}
			if ($token->isType($this->getTokenType()->leftCurlyBracket)) {
				$this->tokenizer->getNextNonWhitespaceToken();
				$declarationList = $this->consumeDeclarationList($mixinFunction, $token);

				if ($declarationList) {
					$mixinFunction->add($declarationList);
					$actualToken = $this->tokenizer->getActualToken();

					if ($actualToken->isType($token->mirrorToken())) {
						if ($token->isType($this->getTokenType()->EOF)) {
							$this->addMixinFunction($mixinFunction);
							return;
						}
						//consume additional space after
						$this->tokenizer->getNextNonWhitespaceToken();
						$this->reconsumeToken();
						$this->addMixinFunction($mixinFunction);
						return;
					}
				}
			}
		}
	}
	
	private function addMixinFunction(MixinDefinition $mixinFunction) {
		// will override mixin with same name
		$this->mixinFunctions[$mixinFunction->name()] = $mixinFunction;
	}

	private function consumeColor(FunctionToken $colorModel) {
		$list = [];

		while ($token = $this->tokenizer->nextToken()) {
			if($token->isType($this->getTokenType()->whitespace)) {
				continue;
			}
			if($token->isType($this->getTokenType()->comma)) {
				continue;
			}
			if($token->isType($this->getTokenType()->rightParenthesis)) {
				break;
			}
			$list[] = $token;
		}
		$functionName = strtolower($colorModel->value());
		switch ($functionName) {
			case Constant::KEYWORD_RGB:
				$this->checkIndexes([0, 1, 2], $functionName, $list);
				$params = array($list[0], $list[1], $list[2]);
				$color = new RGB($params);
				break;
			case Constant::KEYWORD_RGBA:
				$this->checkIndexes([0, 1, 2, 3], $functionName, $list);
				$params = array($list[0], $list[1], $list[2], $list[3]);
				$color = new RGBA($params);
				break;
			case  Constant::KEYWORD_HSV:
				$this->checkIndexes([0, 1, 2], $functionName, $list);
				$params = array($list[0], $list[1], $list[2]);
				$color = new HSV($params);
				break;
			case  Constant::KEYWORD_HSVA:
				$this->checkIndexes([0, 1, 2, 3], $functionName, $list);
				$params = array($list[0], $list[1], $list[2], $list[3]);
				$color = new HSVA($params);
				break;
			case  Constant::KEYWORD_HSL:
				$this->checkIndexes([0, 1, 2], $functionName, $list);
				$params = array($list[0], $list[1], $list[2]);
				$color = new HSL($params);
				break;
			case  Constant::KEYWORD_HSLA:
				$this->checkIndexes([0, 1, 2, 3], $functionName, $list);
				$params = array($list[0], $list[1], $list[2], $list[3]);
				$color = new HSLA($params);
				break;
			case  Constant::KEYWORD_CMYK:
				$this->checkIndexes([0, 1, 2, 3], $functionName, $list);
				$params = array($list[0], $list[1], $list[2], $list[3]);
				$color = new CMYK($params);
				break;
			case  Constant::KEYWORD_CMYKA:
				$this->checkIndexes([0, 1, 2, 3, 4], $functionName, $list);
				$params = array($list[0], $list[1], $list[2], $list[3], $list[4]);
				$color = new CMYKA($params);
				break;
			default :
				throw new UnknownColorFormat("Neznámý formát barvy.");
				break;		
		}
		return $color;
	}

	private function reconsumeToken() {
		$this->tokenizer->reconsumeToken();
	}

	private function checkIndexes(array $indexes, $functionName, $list = null) {
		if (!isset($list)) {
			throw new ArgumentNotSet("Argument funkce \"$functionName\" není zadán.");
		}
		foreach ($indexes as $key => $index) {
			if (!isset($list[$index])) {
				throw new ArgumentNotSet("Argument ".($key+1)." funkce \"$functionName\" není zadán.");
			}
		}
	}
	
	private function getMixinInternalVariables() {
		$variables = array();
		while ($token = $this->tokenizer->nextToken()) {
			if ($token->belongsInTypes([$this->getTokenType()->whitespace, $this->getTokenType()->semicolon])) {
				continue;				
			}
			if($token->isType($this->getTokenType()->variable)) {
				$variables[] = $token;
			}
			if($token->isType($this->getTokenType()->rightParenthesis)) {
				break;
			}
		}
		return $variables;
	}
	private function addVariable(VariableRule $variable) {
		// will override variable with same name
		$this->variables[$variable->name()] = $variable;
	}
	public function getVariableByName($variableName) {
		if(!isset($this->variables[$variableName])) {
			throw new UndefinedVariables("Proměnná '$variableName' není definována.");
		}
		return $this->variables[$variableName];
	}
	
	public function tryGetVariableByName($variableName) {
		if(isset($this->variables[$variableName])) {
			return $this->variables[$variableName];
		}
		return false;
	}

	/**
	 * gets "global" variables defined by input stream
	 *
	 * @return array
	 */
	public function getVariables() {
		return $this->variables;
	}

	/**
	 * gets mixin name
	 *
	 * @param $mixinName
	 * @return mixed
	 * @throws UndefinedVariables
	 */
	public function getMixinByName($mixinName) {
		if(!isset($this->mixinFunctions[$mixinName])) {
			throw new UndefinedVariables("Mixin '$mixinName' není definován.");
		}
		$mixin = $this->mixinFunctions[$mixinName];
		return $mixin;

	}

	/**
	 * adds class
	 *
	 * @param ClassToken    $class
	 * @param QualifiedRule $qualifiedRule
	 */
	private function addClass(ClassToken $class, QualifiedRule $qualifiedRule) {
		$this->classes[$class->value()] = $qualifiedRule;
	}

	/**
	 * gets class object by its name
	 *
	 * @param $className
	 * @return mixed
	 * @throws UndefinedVariables
	 */
	public function getClassByName($className) {
		if(!isset($this->classes[$className])) {
			throw new UndefinedVariables("CSS třída '$className' není definována.");
		}
		return $this->classes[$className];
	}

	/**
	 * gets class object
	 *
	 * @param ClassToken $class
	 * @return mixed
	 */
	public function getClass(ClassToken $class) {
		return $this->getClassByName($class->value());
	}
}

class ParseErrorException extends PreprasorErrorException {
	
}

class UnknownAssociatedBlockToken  extends ParseErrorException {

}

class UndefinedVariables  extends ParseErrorException {

}

class ParserDidNotRun  extends ParseErrorException {

}

class FileNotFound  extends ParseErrorException {

}
class TokenClassRequired extends ParseErrorException {

}

class UnknownColorFormat extends ParseErrorException {

}
class ArgumentNotSet extends ParseErrorException {

}
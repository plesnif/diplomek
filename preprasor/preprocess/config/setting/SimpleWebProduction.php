<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */
namespace Preprasor\Config\Setting;

/**
 * Class SimpleWebProduction Setting
 *
 * @package Preprasor\Config\Setting
 */
class SimpleWebProduction extends Setting {
	/**
	 * {@inheritDoc}
	 */
	public $printAllColorAsRGBHash = true;

	/**
	 * {@inheritDoc}
	 */
	public $prohibitFileManipulation = true;
	
	/**
	 * {@inheritDoc}
	 */
	public $useDefaultQuotes = false;
	
	/**
	 * {@inheritDoc}
	 */
	public $addLastSemicolon = false;
}
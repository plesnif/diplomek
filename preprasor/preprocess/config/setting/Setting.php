<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */
namespace Preprasor\Config\Setting;

/**
 * Class Setting
 *
 * @package Preprasor\Config\Setting
 */
class Setting {
	/**
	 * defines wherever all color should be printed as RGB hash in order to be compatible with all browsers, this option ignores alpha channel
	 *
	 * @var bool
	 */
	public $printAllColorAsRGBHash = true;

	/**
	 * defines wherever file manipulation should be prohibited. i.e. on web production server
	 *
	 * @var bool
	 */
	public $prohibitFileManipulation = false;
	
	/**
	 * defines wherever default quotes should be used for string tokens, i.e. url("someurl") or url('someurl')
	 *
	 * @var bool
	 */
	public $useDefaultQuotes = false;
	
	
	/**
	 * which char to use for qoutes
	 * 
	 * @var string 
	 */
	public $defaultQuote = "\"";
	
	/**
	 * defines wherever semicolon should be added and the end of last declaration
	 */
	public $addLastSemicolon = false;
}
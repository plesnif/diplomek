<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */
namespace Preprasor\Config\Setting;

/**
 * Class StandardWebProduction Setting
 *
 * @package Preprasor\Config\Setting
 */
class StandardWebProduction extends Setting  {
	/**
	 * {@inheritDoc}
	 */
	public $printAllColorAsRGBHash = true;

	/**
	 * {@inheritDoc}
	 */
	public $prohibitFileManipulation = true;
	
	/**
	 * {@inheritDoc}
	 */
	public $useDefaultQuotes = false;
	
	/**
	 * {@inheritDoc}
	 */
	public $addLastSemicolon = false;
}
<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */
namespace Preprasor\Config\Setting;

/**
 * Class StandardStandAlone Setting
 *
 * @package Preprasor\Config\Setting
 */
class StandardStandAlone extends Setting  {
	/**
	 * {@inheritDoc}
	 */
	public $printAllColorAsRGBHash = false;

	/**
	 * {@inheritDoc}
	 */
	public $prohibitFileManipulation = false;
	
	/**
	 * {@inheritDoc}
	 */
	public $useDefaultQuotes = false;
	
	/**
	 * {@inheritDoc}
	 */
	public $addLastSemicolon = false;
}
<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */
namespace Preprasor\Config;

use Preprasor\Config\Setting\Setting;
use Preprasor\Config\Setting\SimpleStandAlone;
use Preprasor\PreprasorErrorException;

/**
 * Class storing application configuration
 *
 * @package Preprasor\Config
 */
abstract class Config {
	/**
	 * @var Setting
	 */
	private static $setting;

	/**
	 * gets setting value
	 *
	 * @param string $key
	 * @return bool
	 * @throws VariableNotSet
	 */
	public static  function getSetting($key) {
		if(is_null(self::$setting)) {
			self::$setting = new SimpleStandAlone;
		}
		if(!isset(self::$setting->$key)) {
			throw new VariableNotSet("V konfiguraci \"" . get_class(self::$setting) . "\" není proměnná \"$key\" definována.");
		}
		return self::$setting->$key;
	}

	/**
	 * sets configuration setting
	 *
	 * @param Setting $setting
	 */
	public static function setSetting(Setting $setting) {
		self::$setting = $setting;
	}

	/**
	 * should be all color print as RGB hash?
	 *
	 * @return bool
	 */
	public static function printAllColorAsRGBHash() {
		return self::getSetting("printAllColorAsRGBHash");
	}

	/**
	 * should be prohibited file manipulation?
	 *
	 * @return bool
	 */
	public static function prohibitFileManipulation() {
		return self::getSetting("prohibitFileManipulation");
	}
	
	/**
	 * should be used default quotes?
	 *
	 * @return bool
	 */
	public static function useDefaultQuotes() {
		return self::getSetting("useDefaultQuotes");
	}
	
	/**
	 * returns default quote
	 *
	 * @return string
	 */
	public static function defaultQuote() {
		return self::getSetting("defaultQuote");
	}
	
	/**
	 * should be semicolon added and the end of last declaration?
	 *
	 * @return bool
	 */
	public static function addLastSemicolon() {
		return self::getSetting("addLastSemicolon");
	}
}

class ConfigErrorException extends PreprasorErrorException {

}

class VariableNotSet extends ConfigErrorException {

}
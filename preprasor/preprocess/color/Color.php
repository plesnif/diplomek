<?php

/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Color;

use Preprasor\Preprocess\Tokenize\Token\Base\Token;
use Preprasor\Preprocess\Color\I\ColorModificable;
use Preprasor\PreprasorErrorException;

/**
 * Template class as base for color objects
 *
 * @package Preprasor\Preprocess\Color
 */
abstract class Color extends Token implements ColorModificable {
	/**
	 * @var int
	 */
	protected $numberOfArgument;
	
	/**
	 * Color constructor.
	 *
	 * @param array $params
	 */
    public function __construct(array $params) {
		$this->checkNumberOfArguments($params);
		$preparedParams = $this->prepareParams($params);
        $this->setFromArray($preparedParams);
    }
	
	/**
	 * sets color inner value from array
	 * 
	 * @param array $params
	 */
	protected abstract function setFromArray(array $params);

	/**
	 * prepares array of params for creating color
	 * 
	 * @param array $params
	 * @return array
	 */
	protected function prepareParams(array $params) {
		$newParams = [];
		foreach ($params as $param) {
			$newParams[] = $this->prepareParam($param);
		}
		return $newParams;
	}

	/**
	 * prepares param for color manipulation
	 *
	 * @param $param
	 * @return int
	 */
	protected function prepareParam($param) {
		if(!is_numeric($param)) {
			if($param instanceof Token) {
				$value = (int) $param->value();
			} else {
				throw new WrongArgument("Neplatný parametr. Paramert může být ve formě Tokenu nebo čísla.");
			}
		} else {
			$value = (int) $param;
		}
		if($value < 1 && $value !== 0) {
			$value *= 100;
		}
		return $value;
	}
	
	/**
	 * checks if $param lies in $range, otherwise throw exception with modified message
	 * 
	 * @param int $param
	 * @param array $range
	 * @param \Preprasor\Preprocess\Color\ColorErrorException $colorException
	 * @throws WrongRangeParamNumber
	 * @throws \Preprasor\Preprocess\Color\ColorErrorException
	 */
	protected function checkIfParamIsInRange($param, array $range, ColorErrorException $colorException) {
		if(count($range) != 2) {
			throw new WrongRangeParamNumber("Špatný počet argumentů pro rozmezí parametru. Vyžadováno 2, obdrženo " . count($range) . ".");
		}
		$param = (int) $param;
		$colorException->setRange($range);
		if(($param < $range[0] || $param > $range[1])) {
			throw $colorException;
		}
	}
	
	/**
	 * modifies $param to fit in low $limit if needed
	 * 
	 * @param int $param
	 * @param int $limit
	 * @return int
	 */
	protected function fitInLowLimit($param, $limit) {
        if($param < $limit) {
            $param = $limit;
        }
		return $param;
	}
	
	/**
	 * modifies $param to fit in high $limit if needed
	 * 
	 * @param int $param
	 * @param int $limit
	 * @return int
	 */
	protected function fitInHighLimit($param, $limit) {
        if($param > $limit) {
            $param = $limit;
        }
		return $param;
	}
	
	protected function checkNumberOfArguments(array $params) {
		if(is_null($this->numberOfArgument) || $this->numberOfArgument !== count($params)) {
			throw new WrongArgumentNumber("Nulový, nebo nesprávný počet argumentů funkce barvy");
		}
		foreach ($params as $key => $param) {
			if(is_null($param) || !isset($param)) {
				$colorClass = new \ReflectionClass($this);
				throw new WrongArgumentNumber("Parametr " . ($key+1) . " barvy " . $colorClass->getShortName() . " nesmí být nulový.");
			}
		}
	}
}
/**
 * Extends exception and modifies its message by $range
 */
class ColorErrorException extends PreprasorErrorException {
	/**
	 * modifies message
	 * 
	 * @param array $range
	 * @throws WrongRangeParamNumber
	 */
	public function setRange(array $range) {
		if(count($range) != 2) {
			throw new WrongRangeParamNumber("Špatný počet argumentů pro rozmezí parametru. Vyžadováno 2, obdrženo " . count($range) . ".");
		}
		$this->message = sprintf($this->message, $range[0], $range[1]);
	}
}

class WrongRangeParamNumber extends ColorErrorException {

}

class WrongArgumentNumber extends ColorErrorException {

}

class WrongArgument extends ColorErrorException {

}
<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Color;

use Preprasor\Preprocess\Color\Model\HSV;
use Preprasor\Preprocess\Color\Model\HSL;
use Preprasor\Preprocess\Color\Model\RGB;
use Preprasor\Preprocess\Color\Model\CMYK;


/**
 * Class managing transition between Colors - HSV, HSL, RBG, CMYK
 *
 * @package Preprasor\Preprocess\Color
 *
 */
class Transition {
	/**
	 * transforms HSL to RGB
	 * 
	 * @param HSL $hsl
	 * @return RGB
	 */
    public static function HSL2RGB(HSL $hsl) {
        $H = $hsl->H;
        $S = $hsl->S/100;
        $L = $hsl->L/100;
        $c = (1-abs(2*$L-1))*$S;
        $x = $c*(1-abs(fmod(($H/60), 2)-1));
        $m = $L-($c/2);
        if ($H<60) {
            $R = $c;
            $G = $x;
            $B = 0;
        } else if ($H<120) {
            $R = $x;
            $G = $c;
            $B = 0;
        } else if ($H<180) {
            $R = 0;
            $G = $c;
            $B = $x;
        } else if ($H<240) {
            $R = 0;
            $G = $x;
            $B = $c;
        } else if ($H<300) {
            $R = $x;
            $G = 0;
            $B = $c;
        } else {
            $R = $c;
            $G = 0;
            $B = $x;
        }
        $R = max(0, floor(($R+$m)*255));
        $G = max(0, floor(($G+$m)*255));
        $B = max(0, floor(($B+$m)*255));
        return new RGB([$R, $G, $B]);
    }
	
	/**
	 * transforms RGB to HSL
	 * 
	 * @param RGB $rgb
	 * @return HSL
	 */
    public static function RGB2HSL(RGB $rgb) {
        $R = $rgb->R/255;
        $G = $rgb->G/255;
        $B = $rgb->B/255;

        $Cmax = max($R, $G, $B);
        $Cmin = min($R, $G, $B);
        $chroma = $Cmax-$Cmin;
		$L = ($Cmax+$Cmin)/2;
        if ($chroma == 0) {
            $H = 0;
            $S = 0;
        } else {
            $S = $chroma/(1-abs(2*$L-1));
            switch ($Cmax) {
                case $R:
                    $H = 60*fmod((($G-$B)/$chroma), 6);
                    if ($B>$G) {
                        $H += 360;
                    }
                    break;
                case $G:
                    $H = 60*(($B-$R)/$chroma+2);
                    break;
                case $B:
                    $H = 60*(($R-$G)/$chroma+4);
                    break;
            }
        }
        return new HSL([round($H, 2), round($S, 4)*100, round($L, 4)*100]);
    }
	
	/**
	 * transforms HSV to HSL
	 * 
	 * @param HSV $hsv
	 * @return HSL
	 */
    public static function HSV2HSL(HSV $hsv){
        $H = $hsv->H;
        $S = $hsv->S/100;
        $V = $hsv->V/100;

		$L = 0.5*$V*(2-$S);
		if((1-abs(2*$L-1)) == 0) {
			$S = $V*$S;
		} else {
			$S = $V*$S/(1-abs(2*$L-1));
		}
        return new HSL([$H, $S*100, $L*100]);
    }
	
	/**
	 * transforms HSL to HSV
	 * 
	 * @param HSL $hsl
	 * @return HSV
	 */
    public static function HSL2HSV(HSL $hsl){
        $H = $hsl->H;
        $S = $hsl->S/100;
        $L = $hsl->L/100;

		$V = (2*$L+$S*(1-abs(2*$L-1)))/2;
		if($V == 0) {
			$S = 2*($V-$L);
		} else {
			$S = 2*($V-$L)/($V);
		}

		return new HSV([$H, $S*100, $V*100]);
    }
	
	/**
	 * transforms RGB to HSV
	 * 
	 * @param RGB $rgb
	 * @return HSV
	 */
    public static function RGB2HSV(RGB $rgb) {
        $R = $rgb->R/255;
        $G = $rgb->G/255;
        $B = $rgb->B/255;

        $max = max($R, $G, $B);
        $min = min($R, $G, $B);
        $chroma = $max - $min;
        $computedV = 100 * $max;
        if ($chroma == 0) {
            $computedH = 0;
            $computedS = 0;
        } else {
            $computedS = 100*($chroma/$max);
            if ($R == $min) {
                $h = 3-(($G-$B)/$chroma);
            } elseif ($B == $min) {
                $h = 1-(($R-$G)/$chroma);
            } else {
                $h = 5-(($B-$R)/$chroma);
            }
            $computedH = 60*$h;
        }
        return new HSV([$computedH, $computedS, $computedV]);
    }
	
	/**
	 * transforms HSV to RGB
	 * 
	 * @param HSV $hsv
	 * @return RGB
	 */
    public static function HSV2RGB(HSV $hsv) {
        $H = $hsv->H/360;
        $S = $hsv->S/100;
        $V = $hsv->V/100;
        if ($S == 0) {
            $computedR = $computedG = $computedB = $V*255;
        }
        else {
            $computedH = $H*6;
            $var_i = floor($computedH);
            $var_1 = $V*(1-$S);
            $var_2 = $V*(1-$S*($computedH-$var_i));
            $var_3 = $V*(1-$S*(1-($computedH-$var_i)));
            if ($var_i == 0) {
                $computedR = $V;
                $computedG = $var_3;
                $computedB = $var_1;
            }
            else if ($var_i == 1) {
                $computedR = $var_2;
                $computedG = $V;
                $computedB = $var_1;
            }
            else if ($var_i == 2) {
                $computedR = $var_1;
                $computedG = $V;
                $computedB = $var_3;
            }
            else if ($var_i == 3) {
                $computedR = $var_1;
                $computedG = $var_2;
                $computedB = $V;
            }
            else if ($var_i == 4) {
                $computedR = $var_3;
                $computedG = $var_1;
                $computedB = $V;
            }
            else {
                $computedR = $V;
                $computedG = $var_1;
                $computedB = $var_2;
            }
            $computedR *= 255;
            $computedG *= 255;
            $computedB *= 255;
        }
        $color = new RGB([$computedR, $computedG, $computedB]);
        return $color;
    }
	
	/**
	 * transforms RGB to CMYK
	 * 
	 * @param RGB $rgb
	 * @return CMYK
	 */
    public static function RGB2CMYK(RGB $rgb) {
        $r = $rgb->R/255;
        $g = $rgb->G/255;
        $b = $rgb->B/255;

		$K = 1-max($r, $g, $b);
		if($K == 1) {
			$C = (1-$r-$K);
			$M = (1-$g-$K);
			$Y = (1-$b-$K);
		} else {
			$C = (1-$r-$K)/(1-$K);
			$M = (1-$g-$K)/(1-$K);
			$Y = (1-$b-$K)/(1-$K);
		}


		return new CMYK([$C*100, $M*100, $Y*100, $K*100]);
	}
	
	/**
	 * transforms CMYK to RGB
	 * 
	 * @param CMYK $cmyk
	 * @return RGB
	 */
    public static function CMYK2RGB(CMYK $cmyk) {
		$R = 255*(100-$cmyk->C)/100*(100-$cmyk->K)/100;
		$G = 255*(100-$cmyk->M)/100*(100-$cmyk->K)/100;
		$B = 255*(100-$cmyk->Y)/100*(100-$cmyk->K)/100;

		return new RGB([(int) round($R), (int) round($G), (int) round($B)]);
	}
}
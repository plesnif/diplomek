<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Color\I;

use Preprasor\Preprocess\I\Compositable;

/**
 * Interface denoting classes capable of color modification
 *
 * @package Preprasor\Preprocess\Color\I
 */
interface ColorModificable extends Compositable {
	/**
	 * lighten Color by $amount
	 *
	 * @param $amount
	 * @return ColorModificable
	 */
    public function lighten($amount);

	/**
	 * darken Color by $amount
	 *
	 * @param $amount
	 * @return ColorModificable
	 */
    public function darken($amount);

	/**
	 * saturate Color by $amount
	 *
	 * @param $amount
	 * @return ColorModificable
	 */
    public function saturate($amount);

	/**
	 * desaturate Color by $amount
	 *
	 * @param $amount
	 * @return ColorModificable
	 */
    public function desaturate($amount);
	
	/**
	 * sets hue on $hueValue
	 *
	 * @param $hueValue
	 * @return ColorModificable
	 */
	public function changeHue($hueValue);
}
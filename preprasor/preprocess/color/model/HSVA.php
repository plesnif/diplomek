<?php

/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Color\Model;

use Preprasor\Constant;
use Preprasor\Config\Config;
use Preprasor\Preprocess\Color\Transition;

/**
 * HSV color with aplha
 *
 * @package Preprasor\Preprocess\Color\Model
 */
class HSVA extends HSV {
	/*
	 * @var int
	 */
	protected $numberOfArgument = 4;
	/**
	 * @var int 
	 */
	protected $alpha;

	/**
	 * HSVA constructor.
	 *
	 * @param array $params
	 */
	public function __construct(array $params) {
		$this->checkNumberOfArguments($params);
		$preparedParams = $this->prepareParams($params);
		$this->alpha = array_pop($preparedParams);
		parent::__construct($params);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function printOut($depth = 0) {
		if(Config::printAllColorAsRGBHash()) {
			$color = Transition::HSV2RGB($this);
			$output = $color->printOut($depth);
		} else {
			$hsl = Transition::HSV2HSL($this);
			$output = Constant::KEYWORD_HSLA 
						. Constant::LEFT_PARENTHESIS 
							. "%d" . Constant::COMMA
							. " %d" . Constant::PERCENTAGE_SIGN . Constant::PERCENTAGE_SIGN . Constant::COMMA
							. " %d" . Constant::PERCENTAGE_SIGN . Constant::PERCENTAGE_SIGN . Constant::COMMA
							. " %s"
						. Constant::RIGHT_PARENTHESIS;
			$output = sprintf($output, $hsl->H, $hsl->S, $hsl->L, $this->alpha/100);
		}
        return $output;

	}
}

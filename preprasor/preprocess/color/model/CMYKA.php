<?php

/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Color\Model;

use Preprasor\Constant;
use Preprasor\Preprocess\Color\Transition;

/**
 * CMYK color with aplha
 *
 * @package Preprasor\Preprocess\Color\Model
 */
class CMYKA extends CMYK {
	/*
	 * @var int
	 */
	protected $numberOfArgument = 5;
	/**
	 * @var int 
	 */
	protected $alpha;

	/**
	 * CMYKA constructor.
	 *
	 * @param array $params
	 */
	public function __construct(array $params) {
		$this->checkNumberOfArguments($params);
		$preparedParams = $this->prepareParams($params);
		$this->alpha = array_pop($preparedParams);
		parent::__construct($params);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function printOut($depth = 0) {
		$rgb = Transition::CMYK2RGB($this);
		if(Config::printAllColorAsRGBHash()) {
			$output = '#'.$rgb->getInHex();
		} else {
			$rgbaFormat = Constant::KEYWORD_RGBA
					. Constant::LEFT_PARENTHESIS 
						. "%d" . Constant::COMMA
						. " %d" . Constant::COMMA
						. " %d" . Constant::COMMA
						. " %s"
					. Constant::RIGHT_PARENTHESIS;
			$output = sprintf($rgbaFormat, $rgb->R, $rgb->G, $rgb->B, $this->alpha/100);
		}
		return $output;	
		
	}
}

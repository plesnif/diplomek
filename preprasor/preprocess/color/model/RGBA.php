<?php

/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Color\Model;

use Preprasor\Constant;
use Preprasor\Config\Config;

/**
 * RGB color with aplha
 *
 * @package Preprasor\Preprocess\Color\Model
 */
class RGBA extends RGB {
	/*
	 * @var int
	 */
	protected $numberOfArgument = 4;
	/**
	 * @var int 
	 */
	protected $alpha;

	/**
	 * RGBA constructor.
	 *
	 * @param array $params
	 */
	public function __construct(array $params) {
		$this->checkNumberOfArguments($params);
		$preparedParams = $this->prepareParams($params);
		$this->alpha = array_pop($preparedParams);
		parent::__construct($params);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public function printOut($depth = 0) {
		if(Config::printAllColorAsRGBHash()) {
			$output = '#'.$this->getInHex();
		} else {
			$rgbaFormat = Constant::KEYWORD_RGBA
				. Constant::LEFT_PARENTHESIS
				. "%d" . Constant::COMMA
				. " %d" . Constant::COMMA
				. " %d" . Constant::COMMA
				. " %s"
				. Constant::RIGHT_PARENTHESIS;
			$output = sprintf($rgbaFormat, $this->R, $this->G, $this->B, $this->alpha/100);
		}
		return $output;
	}
}

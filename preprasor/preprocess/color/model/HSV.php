<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Color\Model;

use Preprasor\Config\Config;
use Preprasor\Preprocess\Color\ColorErrorException;
use Preprasor\Preprocess\Color\Transition;
use Preprasor\Preprocess\Color\Color;

/**
 * Class representing HSV Color
 *
 * @package Preprasor\Preprocess\Color\Model
 */
class HSV extends Color {
	/*
	 * @var int
	 */
	protected $numberOfArgument = 3;
	/**
	 * @var int 
	 */
    public $H;
	
	/**
	 * @var int 
	 */
    public $S;
	
	/**
	 * @var int 
	 */
    public $V;
	
	/**
	 * {@inheritDoc}
	 */
    public function printOut($depth = 0) {
		if(Config::printAllColorAsRGBHash()) {
			$color = Transition::HSV2RGB($this);
			$output = $color->printOut($depth);
		} else {
			$color = Transition::HSV2HSL($this);
			$output = $color->printOut($depth);
		}
        return $output;
    }
	
	/**
	 * {@inheritDoc}
	 */
    protected function setFromArray(array $color) {
        $H = (int) $color[0];
        $S = (int) $color[1];
        $V = (int) $color[2];
        $hasFormat = ($H>=0 && $H<=360 && $S>=0 && $S<=100 && $V>=0 && $V <=100);
        if(!$hasFormat) {
            throw new HSVWrongFormat("Špatný formát HSV barvy - H(0-360) - $H S(0-100) - $S V(0-100) - $V");
        }
        $this->H = $H;
        $this->S = $S;
        $this->V = $V;
    }
	
	/**
	 * {@inheritDoc}
	 */
    public function lighten($amount) {
        $transformedColor = Transition::HSV2HSL($this);
        $transformedColor->lighten($amount);
        $newColor = Transition::HSL2HSV($transformedColor);
        $this->H = $newColor->H;
        $this->S = $newColor->S;
        $this->V = $newColor->V;
		return $this;
    }
	
	/**
	 * {@inheritDoc}
	 */
    public function darken($amount) {
        $transformedColor = Transition::HSV2HSL($this);
        $transformedColor->darken($amount);
        $newColor = Transition::HSL2HSV($transformedColor);
        $this->H = $newColor->H;
        $this->S = $newColor->S;
        $this->V = $newColor->V;
		return $this;
    }
	
	/**
	 * {@inheritDoc}
	 */
    public function saturate($amount) {
		$amount = $this->prepareParam($amount);
        $this->checkIfParamIsInRange($amount, [0, 100], new HSVWrongFormat("Úroveň úpravy saturace musí být číslo v rozmezí %d až %d, nikoliv $amount."));
        $this->S += $amount;
        $this->S = $this->fitInHighLimit($this->S, 100);
		return $this;
    }
	
	/**
	 * {@inheritDoc}
	 */
    public function desaturate($amount) {
        $amount = $this->prepareParam($amount);
        $this->checkIfParamIsInRange($amount, [0, 100], new HSVWrongFormat("Úroveň úpravy saturace musí být číslo v rozmezí %d až %d, nikoliv $amount."));
        $this->S -= $amount;
        $this->S = $this->fitInLowLimit($this->S, 0);
		return $this;
    }
	
	/**
	 * {@inheritDoc}
	 */
    public function changeHue($hueValue) {
        $hueValue = $this->prepareParam($hueValue);
        $this->checkIfParamIsInRange($hueValue, [0, 255], new HSLWrongFormat("Odstínu musí být číslo v rozmezí %d až %d, nikoliv $hueValue."));
        $this->H = $hueValue;
		return $this;
    }
	
}
class HSVErrorException extends ColorErrorException {

}

class HSVWrongFormat extends HSVErrorException {

}

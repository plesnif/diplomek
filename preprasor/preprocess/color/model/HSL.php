<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Color\Model;

use Preprasor\Preprocess\Color\ColorErrorException;
use Preprasor\Constant;
use Preprasor\Preprocess\Color\Color;
use Preprasor\Config\Config;
use Preprasor\Preprocess\Color\Transition;

/**
 * Class representing HSL Color
 *
 * @package Preprasor\Preprocess\Color\Model
 */
class HSL extends Color {
	/*
	 * @var int
	 */
	protected $numberOfArgument = 3;
	/**
	 * @var int 
	 */
    public $H;
	
	/**
	 * @var int 
	 */
    public $S;
	
	/**
	 * @var int 
	 */
    public $L;
	
	/**
	 * returns HSL representation in format hsl(10, 50%, 50%)
	 * 
	 * @param int $depth
	 * @return string
	 */
    public function printOut($depth = 0) {
		if(Config::printAllColorAsRGBHash()) {
			$color = Transition::HSL2RGB($this);
			$output = $color->printOut($depth);
		} else {
			$output = Constant::KEYWORD_HSL 
						. Constant::LEFT_PARENTHESIS 
							. "%d" . Constant::COMMA
							. " %d" . Constant::PERCENTAGE_SIGN . Constant::PERCENTAGE_SIGN . Constant::COMMA
							. " %d" . Constant::PERCENTAGE_SIGN . Constant::PERCENTAGE_SIGN
						. Constant::RIGHT_PARENTHESIS;
			$output = sprintf($output, $this->H, $this->S, $this->L);
		}
        return $output;
    }
	
	/**
	 * {@inheritDoc}
	 */
    protected function setFromArray(array $color) {
        $H = (int) $color[0];
        $S = (int) $color[1];
        $L = (int) $color[2];
        $hasFormat = ($H>=0 && $H<=360 && $S>=0 && $S<=100 && $L>=0 && $L <=100);
        if(!$hasFormat) {
            throw new HSLWrongFormat("Špatný formát HSL barvy - H(0-360) - $H S(0-100) - $S L(0-100) - $L");
        }
        $this->H = $H;
        $this->S = $S;
        $this->L = $L;
    }
	
	/**
	 * {@inheritDoc}
	 */
    public function lighten($amount) {
		$amount = $this->prepareParam($amount);
		$this->checkIfParamIsInRange($amount, [0, 100], new HSVWrongFormat("Úroveň úpravy světelnosti musí být číslo v rozmezí %d až %d, nikoliv $amount."));
        $this->L += $amount;
        $this->L = $this->fitInHighLimit($this->L, 100);
		return $this;
    }
	
	/**
	 * {@inheritDoc}
	 */
    public function darken($amount) {
		$amount = $this->prepareParam($amount);
        $this->checkIfParamIsInRange($amount, [0, 100], new HSVWrongFormat("Úroveň úpravy světelnosti musí být číslo v rozmezí %d až %d, nikoliv $amount."));
        $this->L -= $amount;
        $this->L = $this->fitInLowLimit($this->L, 0);
		return $this;
    }
	
	/**
	 * {@inheritDoc}
	 */
    public function saturate($amount) {
		$amount = $this->prepareParam($amount);
        $this->checkIfParamIsInRange($amount, [0, 100], new HSLWrongFormat("Úroveň úpravy saturace musí být číslo v rozmezí %d až %d, nikoliv $amount."));
        $this->S += $amount;
        $this->S = $this->fitInHighLimit($this->S, 100);
		return $this;
    }
	
	/**
	 * {@inheritDoc}
	 */
    public function desaturate($amount) {
        $amount = $this->prepareParam($amount);
        $this->checkIfParamIsInRange($amount, [0, 100], new HSLWrongFormat("Úroveň úpravy saturace musí být číslo v rozmezí %d až %d, nikoliv $amount."));
        $this->S -= $amount;
        $this->S = $this->fitInLowLimit($this->S, 0);
		return $this;
    }
	
	/**
	 * {@inheritDoc}
	 */
    public function changeHue($hueValue) {
        $hueValue = $this->prepareParam($hueValue);
        $this->checkIfParamIsInRange($hueValue, [0, 255], new HSLWrongFormat("Odstínu musí být číslo v rozmezí %d až %d, nikoliv $hueValue."));
        $this->H = $hueValue;
		return $this;
    }
}
class HSLErrorException extends ColorErrorException {

}

class HSLWrongFormat extends HSLErrorException {

}
<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Color\Model;

use Preprasor\Preprocess\Color\ColorErrorException;
use Preprasor\Preprocess\Color\Color;
use Preprasor\Preprocess\Color\Transition;

/**
 * Class representing CMYK Color
 *
 * @package Preprasor\Preprocess\Color\Model
 */
class CMYK extends Color {
	/*
	 * @var int
	 */
	protected $numberOfArgument = 4;
	/**
	 * @var int 
	 */
    public $C;
	
	/**
	 * @var int 
	 */
    public $M;
	
	/**
	 * @var int 
	 */
    public $Y;
	
	/**
	 * @var int 
	 */
	public $K;

	
	/**
	 * {@inheritDoc}
	 */
    public function printOut($depth = 0) {
        $rgb = Transition::CMYK2RGB($this);
        return $rgb->printOut();
    }
	
	/**
	 * {@inheritDoc}
	 */
    protected function setFromArray(array $color) {
        $C = (int) $color[0];
        $M = (int) $color[1];
        $Y = (int) $color[2];
		$K = (int) $color[3];
        $hasFormat = ($C>=0 && $C<=100 && $M>=0 && $M <=100 && $Y>=0 && $Y<=100 && $K>=0 && $K<=100);
        if(!$hasFormat) {
            throw new CMYKWrongFormat("Špatný formát CMYK barvy - C(0-100) - $C M(0-100) - $M Y(0-100) - $Y K(0-100) - $K");
        }
        $this->C = $C;
        $this->M = $M;
        $this->Y = $Y;
        $this->K = $K;
    }
	
	/**
	 * {@inheritDoc}
	 */
    public function lighten($amount) {
    	$color = Transition::RGB2HSL(Transition::CMYK2RGB($this));
		$color->lighten($amount);
		$newCMYK = Transition::RGB2CMYK(Transition::HSL2RGB($color));
        $this->C = $newCMYK->C;
		$this->M = $newCMYK->M;
		$this->Y = $newCMYK->Y;
		$this->K = $newCMYK->K;
		return $this;
    }
	
	/**
	 * {@inheritDoc}
	 */
    public function darken($amount) {
		$color = Transition::RGB2HSL(Transition::CMYK2RGB($this));
		$color->darken($amount);
		$newCMYK = Transition::RGB2CMYK(Transition::HSL2RGB($color));
		$this->C = $newCMYK->C;
		$this->M = $newCMYK->M;
		$this->Y = $newCMYK->Y;
		$this->K = $newCMYK->K;
		return $this;
    }
	
	/**
	 * {@inheritDoc}
	 */
    public function saturate($amount) {
		$color = Transition::RGB2HSL(Transition::CMYK2RGB($this));
		$color->saturate($amount);
		$newCMYK = Transition::RGB2CMYK(Transition::HSL2RGB($color));
		$this->C = $newCMYK->C;
		$this->M = $newCMYK->M;
		$this->Y = $newCMYK->Y;
		$this->K = $newCMYK->K;
		return $this;
    }
	
	/**
	 * {@inheritDoc}
	 */
    public function desaturate($amount) {
		$color = Transition::RGB2HSL(Transition::CMYK2RGB($this));
		$color->desaturate($amount);
		$newCMYK = Transition::RGB2CMYK(Transition::HSL2RGB($color));
		$this->C = $newCMYK->C;
		$this->M = $newCMYK->M;
		$this->Y = $newCMYK->Y;
		$this->K = $newCMYK->K;
		return $this;
    }
	
	/**
	 * {@inheritDoc}
	 */
    public function changeHue($hue) {
		$color = Transition::RGB2HSL(Transition::CMYK2RGB($this));
		$color->changeHue($hue);
		$newCMYK = Transition::RGB2CMYK(Transition::HSL2RGB($color));
		$this->C = $newCMYK->C;
		$this->M = $newCMYK->M;
		$this->Y = $newCMYK->Y;
		$this->K = $newCMYK->K;
		return $this;
    }
}
class CMYKErrorException extends ColorErrorException {

}

class CMYKWrongFormat extends CMYKErrorException {

}
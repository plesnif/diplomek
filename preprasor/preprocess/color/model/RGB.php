<?php
/**
 * This file is part of Preprasor CSS Preprocessor
 * Created by Filip Vorel 2016
 */

namespace Preprasor\Preprocess\Color\Model;

use Preprasor\Constant;
use Preprasor\Preprocess\Color\ColorErrorException;
use Preprasor\Preprocess\Color\Color;
use Preprasor\Preprocess\Color\Transition;
use Preprasor\Config\Config;

/**
 * Class representing RGB Color
 *
 * @package Preprasor\Preprocess\Color\Model
 */
class RGB extends Color {
	/*
	 * @var int
	 */
	protected $numberOfArgument = 3;
	/**
	 * @var int 
	 */
    public $R;
	
	/**
	 * @var int 
	 */
    public $G;
	
	/**
	 * @var int 
	 */
    public $B;

	/**
	 * RGB constructor.
	 *
	 * @param array $params
	 */
	public function __construct($params) {
		if(is_array($params)) {
			$this->checkNumberOfArguments($params);
			$newParams = $this->prepareParams($params);
			$this->setFromArray($newParams);
		} else {
			$this->setFromString($params);
		}
	}

	/**
	 * {@inheritDoc}
	 */
    private function setFromString($string) {
        $format = '/^[0-9a-fA-F]{6}$/';
        if(!preg_match($format, $string, $matches)) {
            throw new RGBWrongFormat("Špatný formát hashe RGB barvy - " . $string . ", vyžadováno $format");
        }
        $this->R = hexdec(substr($string, 0, 2));
        $this->G = hexdec(substr($string, 2, 2));
        $this->B = hexdec(substr($string, 4, 2));

    }
	
	/**
	 * {@inheritDoc}
	 */
    public function printOut($depth = 0) {
		if(Config::printAllColorAsRGBHash()) {
			$output = '#'.$this->getInHex();
		} else {
			$output = Constant::KEYWORD_RGB
				. Constant::LEFT_PARENTHESIS
				. "%d" . Constant::COMMA
				. " %d" . Constant::COMMA
				. " %d"
				. Constant::RIGHT_PARENTHESIS;
			$output = sprintf($output, $this->R, $this->G, $this->B);
		}
        return $output;
    }
	
	/**
	 * {@inheritDoc}
	 */
    protected function setFromArray(array $color) {
        $R = (int) $color[0];
        $G = (int) $color[1];
        $B = (int) $color[2];
        $hasFormat = ($R>=0 && $R<=255 && $G>=0 && $G <=255 && $B>=0 && $B<=255);
        if(!$hasFormat) {
            throw new RGBWrongFormat("Špatný formát RGB barvy - R(0-255) - $R G(0-255) - $G B(0-255) - $B");
        }
        $this->R = $R;
        $this->G = $G;
        $this->B = $B;
    }
	
	/**
	 * returns hexadecimal representation of RGB color
	 * 
	 * @return string
	 */
    public function getInHex() {
        $r = dechex($this->R);
        $g = dechex($this->G);
        $b = dechex($this->B);
        if(strlen($r)==1) {
            $r = "0".$r;
        }
        if(strlen($g)==1) {
            $g = "0".$g;
        }
        if(strlen($b)==1) {
            $b = "0".$b;
        }
        return $r.$g.$b;
    }
	
	/**
	 * {@inheritDoc}
	 */
    public function lighten($amount) {
        $transformedColor = Transition::RGB2HSL($this);
        $transformedColor->lighten($amount);
        $newColor = Transition::HSL2RGB($transformedColor);
        $this->R = $newColor->R;
        $this->G = $newColor->G;
        $this->B = $newColor->B;
		return $this;
    }
	
	/**
	 * {@inheritDoc}
	 */
    public function darken($amount) {
        $transformedColor = Transition::RGB2HSL($this);
        $transformedColor->darken($amount);
        $newColor = Transition::HSL2RGB($transformedColor);
        $this->R = $newColor->R;
        $this->G = $newColor->G;
        $this->B = $newColor->B;
		return $this;
    }
	
	/**
	 * {@inheritDoc}
	 */
    public function saturate($amount) {
        $transformedColor = Transition::RGB2HSL($this);
        $transformedColor->saturate($amount);
        $newColor = Transition::HSL2RGB($transformedColor);
        $this->R = $newColor->R;
        $this->G = $newColor->G;
        $this->B = $newColor->B;
		return $this;
    }
	
	/**
	 * {@inheritDoc}
	 */
    public function desaturate($amount) {
        $transformedColor = Transition::RGB2HSL($this);
        $transformedColor->desaturate($amount);
        $newColor = Transition::HSL2RGB($transformedColor);
        $this->R = $newColor->R;
        $this->G = $newColor->G;
        $this->B = $newColor->B;
		return $this;
    }
	
	/**
	 * {@inheritDoc}
	 */
    public function changeHue($hue) {
        $transformedColor = Transition::RGB2HSL($this);
        $transformedColor->changeHue($hue);
        $newColor = Transition::HSL2RGB($transformedColor);
        $this->R = $newColor->R;
        $this->G = $newColor->G;
        $this->B = $newColor->B;
		return $this;
    }
	
}
class RGBErrorException extends ColorErrorException {

}

class RGBWrongFormat extends RGBErrorException {

}
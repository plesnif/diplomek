\select@language {czech}
\select@language {czech}
\contentsline {subsection}{Odkaz na tuto pr{\' a}ci}{viii}{Doc-Start}
\contentsline {chapter}{{\' U}vod}{3}{chapter*.2}
\contentsline {chapter}{\chapternumberline {1}CSS - Cascading Style Sheets}{5}{chapter.1}
\contentsline {section}{\numberline {1.1}Vyu\IeC {\v z}it\IeC {\'\i }}{5}{section.1.1}
\contentsline {section}{\numberline {1.2}Syntaxe}{6}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Napojen\IeC {\'\i } kask\IeC {\'a}dov\IeC {\'y}ch styl\IeC {\r u} na HTML str\IeC {\'a}nku}{6}{subsection.1.2.1}
\contentsline {section}{\numberline {1.3}Verze}{7}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}CSS verze 1}{7}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}CSS verze 2}{7}{subsection.1.3.2}
\contentsline {subsection}{\numberline {1.3.3}CSS verze 2.1}{7}{subsection.1.3.3}
\contentsline {subsection}{\numberline {1.3.4}CSS verze 3}{7}{subsection.1.3.4}
\contentsline {subsection}{\numberline {1.3.5}CSS verze 4}{7}{subsection.1.3.5}
\contentsline {section}{\numberline {1.4}V\IeC {\'y}hody}{8}{section.1.4}
\contentsline {section}{\numberline {1.5}Nev\IeC {\'y}hody}{8}{section.1.5}
\contentsline {chapter}{\chapternumberline {2}Preprocesor}{9}{chapter.2}
\contentsline {section}{\numberline {2.1}Existuj\IeC {\'\i }c\IeC {\'\i } n\IeC {\'a}stroje}{9}{section.2.1}
\contentsline {section}{\numberline {2.2}V\IeC {\'y}hody n\IeC {\'a}stroj\IeC {\r u} SASS a LESS oproti jazyku CSS}{9}{section.2.2}
\contentsline {section}{\numberline {2.3}Nedostatky existuj\IeC {\'\i }c\IeC {\'\i }ch n\IeC {\'a}stroj\IeC {\r u}}{9}{section.2.3}
\contentsline {section}{\numberline {2.4}Po\IeC {\v z}adavky na preprocesor}{10}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Jazyk roz\IeC {\v s}i\IeC {\v r}uj\IeC {\'\i }c\IeC {\'\i } CSS}{10}{subsection.2.4.1}
\contentsline {subsubsection}{\numberline {2.4.1.1}U\IeC {\v z}ivatelsk\IeC {\'e} prom\IeC {\v e}nn\IeC {\'e}}{10}{subsubsection.2.4.1.1}
\contentsline {subsubsection}{\numberline {2.4.1.2}U\IeC {\v z}ivatelsk\IeC {\'e} funkce}{10}{subsubsection.2.4.1.2}
\contentsline {subsubsection}{\numberline {2.4.1.3}D\IeC {\v e}di\IeC {\v c}nost}{10}{subsubsection.2.4.1.3}
\contentsline {subsubsection}{\numberline {2.4.1.4}Vno\IeC {\v r}ov\IeC {\'a}n\IeC {\'\i }}{10}{subsubsection.2.4.1.4}
\contentsline {subsubsection}{\numberline {2.4.1.5}Vyj\IeC {\'\i }m\IeC {\'a}n\IeC {\'\i } vlastnost\IeC {\'\i }}{11}{subsubsection.2.4.1.5}
\contentsline {subsubsection}{\numberline {2.4.1.6}Barevn\IeC {\'e} palety a barevn\IeC {\'e} funkce}{11}{subsubsection.2.4.1.6}
\contentsline {subsubsection}{\numberline {2.4.1.7}Barevn\IeC {\'e} t\IeC {\'o}nov\IeC {\'a}n\IeC {\'\i } fotek a jin\IeC {\'e} manipulace s~obr\IeC {\'a}zky}{11}{subsubsection.2.4.1.7}
\contentsline {chapter}{\chapternumberline {3}N\IeC {\'a}vrh preprocesoru}{13}{chapter.3}
\contentsline {section}{\numberline {3.1}Form\IeC {\'a}ln\IeC {\'\i } definice}{13}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Form\IeC {\'a}ln\IeC {\'\i } jazyk}{13}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Form\IeC {\'a}ln\IeC {\'\i } gramatika}{13}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Syntaktick\IeC {\'y} strom}{14}{subsection.3.1.3}
\contentsline {section}{\numberline {3.2}Mechanismus fungov\IeC {\'a}n\IeC {\'\i } preprocesoru}{14}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Nastaven\IeC {\'\i } preprocesoru}{14}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Tokenizace}{15}{subsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.2.1}Definice pojm\IeC {\r u}}{15}{subsubsection.3.2.2.1}
\contentsline {subsubsection}{\numberline {3.2.2.2}V\IeC {\'y}znamy token\IeC {\r u} a jejich diagramy}{18}{subsubsection.3.2.2.2}
\contentsline {subsubsection}{\numberline {3.2.2.3}Algoritmus tokenizace}{25}{subsubsection.3.2.2.3}
\contentsline {subsubsection}{\numberline {3.2.2.4}Konzumace \IeC {\v c}\IeC {\'\i }sla}{28}{subsubsection.3.2.2.4}
\contentsline {subsection}{\numberline {3.2.3}Parsov\IeC {\'a}n\IeC {\'\i }}{29}{subsection.3.2.3}
\contentsline {subsubsection}{\numberline {3.2.3.1}Definice pojm\IeC {\r u} f\IeC {\'a}ze parsov\IeC {\'a}n\IeC {\'\i }}{30}{subsubsection.3.2.3.1}
\contentsline {subsubsection}{\numberline {3.2.3.2}V\IeC {\'y}znamy a diagramy gramatick\IeC {\'y}ch pravidel}{30}{subsubsection.3.2.3.2}
\contentsline {subsubsection}{\numberline {3.2.3.3}Algoritmus parsov\IeC {\'a}n\IeC {\'\i }}{33}{subsubsection.3.2.3.3}
\contentsline {subsection}{\numberline {3.2.4}Preprocesov\IeC {\'a}n\IeC {\'\i }}{38}{subsection.3.2.4}
\contentsline {subsubsection}{\numberline {3.2.4.1}Import extern\IeC {\'\i }ch soubor\IeC {\r u}}{38}{subsubsection.3.2.4.1}
\contentsline {subsubsection}{\numberline {3.2.4.2}U\IeC {\v z}ivatelsk\IeC {\'e} prom\IeC {\v e}nn\IeC {\'e}}{38}{subsubsection.3.2.4.2}
\contentsline {subsubsection}{\numberline {3.2.4.3}U\IeC {\v z}ivatelsk\IeC {\'e} funkce}{38}{subsubsection.3.2.4.3}
\contentsline {subsubsection}{\numberline {3.2.4.4}D\IeC {\v e}di\IeC {\v c}nost}{38}{subsubsection.3.2.4.4}
\contentsline {subsubsection}{\numberline {3.2.4.5}Vno\IeC {\v r}ov\IeC {\'a}n\IeC {\'\i }}{38}{subsubsection.3.2.4.5}
\contentsline {subsubsection}{\numberline {3.2.4.6}Vyj\IeC {\'\i }m\IeC {\'a}n\IeC {\'\i } vlastnost\IeC {\'\i }}{39}{subsubsection.3.2.4.6}
\contentsline {subsubsection}{\numberline {3.2.4.7}Funkce zji\IeC {\v s}\IeC {\v t}uj\IeC {\'\i }c\IeC {\'\i } vlastnosti obr\IeC {\'a}zku}{39}{subsubsection.3.2.4.7}
\contentsline {subsubsection}{\numberline {3.2.4.8}Barevn\IeC {\'e} t\IeC {\'o}nov\IeC {\'a}n\IeC {\'\i } fotek a funkce s~obr\IeC {\'a}zky}{39}{subsubsection.3.2.4.8}
\contentsline {subsubsection}{\numberline {3.2.4.9}Barevn\IeC {\'e} palety a barevn\IeC {\'e} funkce}{39}{subsubsection.3.2.4.9}
\contentsline {subsection}{\numberline {3.2.5}V\IeC {\'y}pis v\IeC {\'y}stupu}{47}{subsection.3.2.5}
\contentsline {subsection}{\numberline {3.2.6}O\IeC {\v s}et\IeC {\v r}ov\IeC {\'a}n\IeC {\'\i } chyb}{47}{subsection.3.2.6}
\contentsline {subsection}{\numberline {3.2.7}Shrnut\IeC {\'\i } pravidel}{47}{subsection.3.2.7}
\contentsline {chapter}{\chapternumberline {4}Implementace}{49}{chapter.4}
\contentsline {section}{\numberline {4.1}Pou\IeC {\v z}it\IeC {\'e} technologie}{49}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Skriptovac\IeC {\'\i } jazyk PHP}{49}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Serverov\IeC {\'a} \IeC {\v c}\IeC {\'a}st s~vyu\IeC {\v z}it\IeC {\'\i }m Lighttpd}{49}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Verzovac\IeC {\'\i } syst\IeC {\'e}my a vyu\IeC {\v z}it\IeC {\'\i } GITu}{50}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}Spr\IeC {\'a}va z\IeC {\'a}vislost\IeC {\'\i } a utilita Composer}{50}{subsection.4.1.4}
\contentsline {subsection}{\numberline {4.1.5}Unit testov\IeC {\'a}n\IeC {\'\i } a framework PHPUnit}{50}{subsection.4.1.5}
\contentsline {subsection}{\numberline {4.1.6}Debugger a roz\IeC {\v s}\IeC {\'\i }\IeC {\v r}en\IeC {\'\i } Xdebug}{50}{subsection.4.1.6}
\contentsline {subsection}{\numberline {4.1.7}Lad\IeC {\v e}n\IeC {\'\i } a knihovna Tracy}{51}{subsection.4.1.7}
\contentsline {subsection}{\numberline {4.1.8}Dokumentace a n\IeC {\'a}stroj ApiGen}{51}{subsection.4.1.8}
\contentsline {subsection}{\numberline {4.1.9}V\IeC {\'y}vojov\IeC {\'a} prost\IeC {\v r}ed\IeC {\'\i } a vyu\IeC {\v z}it\IeC {\'\i } NetBeans}{51}{subsection.4.1.9}
\contentsline {section}{\numberline {4.2}V\IeC {\'y}choz\IeC {\'\i } principy}{51}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}OOP - Object-Oriented Programming}{52}{subsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.1.1}Object}{52}{subsubsection.4.2.1.1}
\contentsline {subsubsection}{\numberline {4.2.1.2}Abstrakce}{52}{subsubsection.4.2.1.2}
\contentsline {subsubsection}{\numberline {4.2.1.3}Encapsulation}{53}{subsubsection.4.2.1.3}
\contentsline {subsubsection}{\numberline {4.2.1.4}Composition}{53}{subsubsection.4.2.1.4}
\contentsline {subsubsection}{\numberline {4.2.1.5}Delegation}{53}{subsubsection.4.2.1.5}
\contentsline {subsubsection}{\numberline {4.2.1.6}Inheritance}{53}{subsubsection.4.2.1.6}
\contentsline {subsubsection}{\numberline {4.2.1.7}Polymorphism}{53}{subsubsection.4.2.1.7}
\contentsline {subsection}{\numberline {4.2.2}SoC - Separation of Concerns}{53}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}KISS - Keep It Simple Stupid}{54}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}DRY - Don't Repeat Yourself}{54}{subsection.4.2.4}
\contentsline {subsection}{\numberline {4.2.5}S.O.L.I.D}{54}{subsection.4.2.5}
\contentsline {subsubsection}{\numberline {4.2.5.1}SRP - Single Responsibility Principle}{54}{subsubsection.4.2.5.1}
\contentsline {subsubsection}{\numberline {4.2.5.2}OCP - Open/Closed Principle}{54}{subsubsection.4.2.5.2}
\contentsline {subsubsection}{\numberline {4.2.5.3}LSP - Liskov Substitution Principle}{54}{subsubsection.4.2.5.3}
\contentsline {subsubsection}{\numberline {4.2.5.4}ISP - Interface Segregation Principle}{54}{subsubsection.4.2.5.4}
\contentsline {subsubsection}{\numberline {4.2.5.5}DIP - Dependency Inversion Principle}{55}{subsubsection.4.2.5.5}
\contentsline {subsection}{\numberline {4.2.6}DI - Dependency Injection}{55}{subsection.4.2.6}
\contentsline {subsection}{\numberline {4.2.7}TDD - Test-Driven Development}{55}{subsection.4.2.7}
\contentsline {subsubsection}{\numberline {4.2.7.1}Cyklus v\IeC {\'y}voje}{55}{subsubsection.4.2.7.1}
\contentsline {subsubsection}{\numberline {4.2.7.2}Metrika Code Coverage}{56}{subsubsection.4.2.7.2}
\contentsline {subsubsection}{\numberline {4.2.7.3}Kritika a omezen\IeC {\'\i } TDD}{56}{subsubsection.4.2.7.3}
\contentsline {subsection}{\numberline {4.2.8}Design Patterns - n\IeC {\'a}vrhov\IeC {\'e} vzory}{56}{subsection.4.2.8}
\contentsline {subsubsection}{\numberline {4.2.8.1}Vyu\IeC {\v z}it\IeC {\'\i } n\IeC {\'a}vrhov\IeC {\'y}ch vzor\IeC {\r u}}{56}{subsubsection.4.2.8.1}
\contentsline {subsubsection}{\numberline {4.2.8.2}Kritika}{57}{subsubsection.4.2.8.2}
\contentsline {subsection}{\numberline {4.2.9}PSR - PHP Standard Recommendation}{57}{subsection.4.2.9}
\contentsline {subsubsection}{\numberline {4.2.9.1}PSR 1}{57}{subsubsection.4.2.9.1}
\contentsline {subsubsection}{\numberline {4.2.9.2}PSR 2}{57}{subsubsection.4.2.9.2}
\contentsline {subsubsection}{\numberline {4.2.9.3}PSR 4}{57}{subsubsection.4.2.9.3}
\contentsline {subsubsection}{\numberline {4.2.9.4}PSR 5}{57}{subsubsection.4.2.9.4}
\contentsline {section}{\numberline {4.3}Implementace f\IeC {\'a}ze nastaven\IeC {\'\i }}{59}{section.4.3}
\contentsline {section}{\numberline {4.4}Implementace tokenizace}{60}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Cyklus z\IeC {\'\i }sk\IeC {\'a}n\IeC {\'\i } tokenu}{60}{subsection.4.4.1}
\contentsline {section}{\numberline {4.5}Implementace parsov\IeC {\'a}n\IeC {\'\i }}{62}{section.4.5}
\contentsline {section}{\numberline {4.6}Implementace preprocesov\IeC {\'a}n\IeC {\'\i }}{66}{section.4.6}
\contentsline {subsection}{\numberline {4.6.1}Import extern\IeC {\'\i }ch soubor\IeC {\r u}}{66}{subsection.4.6.1}
\contentsline {subsubsection}{\numberline {4.6.1.1}P\IeC {\v r}\IeC {\'\i }klad pou\IeC {\v z}it\IeC {\'\i }}{66}{subsubsection.4.6.1.1}
\contentsline {subsection}{\numberline {4.6.2}U\IeC {\v z}ivatelsk\IeC {\'e} prom\IeC {\v e}nn\IeC {\'e}}{66}{subsection.4.6.2}
\contentsline {subsubsection}{\numberline {4.6.2.1}P\IeC {\v r}\IeC {\'\i }klad pou\IeC {\v z}it\IeC {\'\i }}{66}{subsubsection.4.6.2.1}
\contentsline {subsection}{\numberline {4.6.3}U\IeC {\v z}ivatelsk\IeC {\'e} funkce}{67}{subsection.4.6.3}
\contentsline {subsubsection}{\numberline {4.6.3.1}P\IeC {\v r}\IeC {\'\i }klad pou\IeC {\v z}it\IeC {\'\i }}{67}{subsubsection.4.6.3.1}
\contentsline {subsection}{\numberline {4.6.4}D\IeC {\v e}di\IeC {\v c}nost}{68}{subsection.4.6.4}
\contentsline {subsubsection}{\numberline {4.6.4.1}P\IeC {\v r}\IeC {\'\i }klad pou\IeC {\v z}it\IeC {\'\i }}{68}{subsubsection.4.6.4.1}
\contentsline {subsection}{\numberline {4.6.5}Vno\IeC {\v r}ov\IeC {\'a}n\IeC {\'\i }}{68}{subsection.4.6.5}
\contentsline {subsubsection}{\numberline {4.6.5.1}P\IeC {\v r}\IeC {\'\i }klad pou\IeC {\v z}it\IeC {\'\i }}{68}{subsubsection.4.6.5.1}
\contentsline {subsection}{\numberline {4.6.6}Vyj\IeC {\'\i }m\IeC {\'a}n\IeC {\'\i } vlastnost\IeC {\'\i }}{69}{subsection.4.6.6}
\contentsline {subsubsection}{\numberline {4.6.6.1}P\IeC {\v r}\IeC {\'\i }klad pou\IeC {\v z}it\IeC {\'\i }}{69}{subsubsection.4.6.6.1}
\contentsline {subsection}{\numberline {4.6.7}Funkce zji\IeC {\v s}\IeC {\v t}uj\IeC {\'\i }c\IeC {\'\i } vlastnosti obr\IeC {\'a}zku}{69}{subsection.4.6.7}
\contentsline {subsubsection}{\numberline {4.6.7.1}P\IeC {\v r}\IeC {\'\i }klad pou\IeC {\v z}it\IeC {\'\i }}{70}{subsubsection.4.6.7.1}
\contentsline {subsection}{\numberline {4.6.8}Barevn\IeC {\'e} t\IeC {\'o}nov\IeC {\'a}n\IeC {\'\i } fotek a funkce s~obr\IeC {\'a}zky}{70}{subsection.4.6.8}
\contentsline {subsubsection}{\numberline {4.6.8.1}P\IeC {\v r}\IeC {\'\i }klad pou\IeC {\v z}it\IeC {\'\i }}{70}{subsubsection.4.6.8.1}
\contentsline {subsection}{\numberline {4.6.9}Barevn\IeC {\'e} palety a barevn\IeC {\'e} funkce}{71}{subsection.4.6.9}
\contentsline {subsubsection}{\numberline {4.6.9.1}P\IeC {\v r}\IeC {\'\i }klad pou\IeC {\v z}it\IeC {\'\i } 1}{72}{subsubsection.4.6.9.1}
\contentsline {subsubsection}{\numberline {4.6.9.2}P\IeC {\v r}\IeC {\'\i }klad pou\IeC {\v z}it\IeC {\'\i } 2}{73}{subsubsection.4.6.9.2}
\contentsline {section}{\numberline {4.7}Implementace v\IeC {\'y}pisu v\IeC {\'y}stupu}{74}{section.4.7}
\contentsline {subsection}{\numberline {4.7.1}Anal\IeC {\'y}za}{74}{subsection.4.7.1}
\contentsline {section}{\numberline {4.8}Implementace o\IeC {\v s}et\IeC {\v r}ov\IeC {\'a}n\IeC {\'\i } chyb}{74}{section.4.8}
\contentsline {section}{\numberline {4.9}Celkov\IeC {\'y} p\IeC {\v r}ehled implementace}{74}{section.4.9}
\contentsline {chapter}{\chapternumberline {5}V\IeC {\'y}hody pou\IeC {\v z}it\IeC {\'\i } preprocesoru oproti jazyku CSS}{77}{chapter.5}
\contentsline {section}{\numberline {5.1}Znovupou\IeC {\v z}itelnost k\IeC {\'o}du}{77}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}P\IeC {\v r}\IeC {\'\i }klad pou\IeC {\v z}it\IeC {\'\i }}{77}{subsection.5.1.1}
\contentsline {section}{\numberline {5.2}Udr\IeC {\v z}itelnost}{78}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}P\IeC {\v r}\IeC {\'\i }klad pou\IeC {\v z}it\IeC {\'\i }}{78}{subsection.5.2.1}
\contentsline {section}{\numberline {5.3}Zkr\IeC {\'a}cen\IeC {\'\i } d\IeC {\'e}lky k\IeC {\'o}du}{79}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}P\IeC {\v r}\IeC {\'\i }klad pou\IeC {\v z}it\IeC {\'\i }}{79}{subsection.5.3.1}
\contentsline {section}{\numberline {5.4}Odstran\IeC {\v e}n\IeC {\'\i } duplicit}{81}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}P\IeC {\v r}\IeC {\'\i }klad pou\IeC {\v z}it\IeC {\'\i }}{81}{subsection.5.4.1}
\contentsline {section}{\numberline {5.5}Snadn\IeC {\'a} editovatelnost k\IeC {\'o}du}{82}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}P\IeC {\v r}\IeC {\'\i }klad pou\IeC {\v z}it\IeC {\'\i }}{82}{subsection.5.5.1}
\contentsline {chapter}{\chapternumberline {6}Integrace do publika\IeC {\v c}n\IeC {\'\i }ho syst\IeC {\'e}mu Webgarden}{85}{chapter.6}
\contentsline {section}{\numberline {6.1}Architektura MVC}{85}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}\IeC {\v C}\IeC {\'a}st Model (\IeC {\v c}es. model)}{85}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}\IeC {\v C}\IeC {\'a}st View (\IeC {\v c}es. pohled)}{85}{subsection.6.1.2}
\contentsline {subsection}{\numberline {6.1.3}\IeC {\v C}\IeC {\'a}st Controller (\IeC {\v c}es. \IeC {\v r}adi\IeC {\v c})}{85}{subsection.6.1.3}
\contentsline {subsection}{\numberline {6.1.4}Mechanismus MVC}{86}{subsection.6.1.4}
\contentsline {section}{\numberline {6.2}Pou\IeC {\v z}it\IeC {\'\i } preprocesoru s~p\IeC {\v r}ihl\IeC {\'e}dnut\IeC {\'\i }m ke komer\IeC {\v c}n\IeC {\'\i }mu p\IeC {\v r}\IeC {\'\i }stupu}{86}{section.6.2}
\contentsline {section}{\numberline {6.3}Propojen\IeC {\'\i } publika\IeC {\v c}n\IeC {\'\i }ho syst\IeC {\'e}mu a preprocesoru}{86}{section.6.3}
\contentsline {section}{\numberline {6.4}O\IeC {\v s}et\IeC {\v r}ov\IeC {\'a}n\IeC {\'\i } chyb}{88}{section.6.4}
\contentsline {chapter}{Z{\' a}v{\v e}r}{89}{chapter*.3}
\contentsline {chapter}{Literatura}{91}{section*.5}
\contentsline {appendix}{\chapternumberline {A}Seznam pou\IeC {\v z}it\IeC {\'y}ch zkratek}{95}{appendix.A}
\contentsline {appendix}{\chapternumberline {B}Obsah p\IeC {\v r}ilo\IeC {\v z}en\IeC {\'e}ho CD}{97}{appendix.B}
\contentsline {paragraph}{Charakteristika nejpodstatn\IeC {\v e}j\IeC {\v s}\IeC {\'\i }ch soubor\IeC {\r u} na p\IeC {\v r}ilo\IeC {\v z}en\IeC {\'e}m CD}{97}{appendix.B}
